/**
 * 
 */
package forum;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author Arushi
 *
 */
public class ForumThread {
	private long thread_id;
	private long parent_category_id;
	private String thread_name;
	private long author_id;
	private Timestamp timestamp;
	/**
	 * @return the thread_id
	 */
	public long getThread_id() {
		return thread_id;
	}
	/**
	 * @param thread_id the thread_id to set
	 */
	public void setThread_id(long thread_id) {
		this.thread_id = thread_id;
	}
	/**
	 * @return the parent_category_id
	 */
	public long getParent_category_id() {
		return parent_category_id;
	}
	/**
	 * @param parent_category_id the parent_category_id to set
	 */
	public void setParent_category_id(long parent_category_id) {
		this.parent_category_id = parent_category_id;
	}
	/**
	 * @return the thread_name
	 */
	public String getThread_name() {
		return thread_name;
	}
	/**
	 * @param thread_name the thread_name to set
	 */
	public void setThread_name(String thread_name) {
		this.thread_name = thread_name;
	}
	/**
	 * @return the author_id
	 */
	public long getAuthor_id() {
		return author_id;
	}
	/**
	 * @param author_id the author_id to set
	 */
	public void setAuthor_id(long author_id) {
		this.author_id = author_id;
	}
	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
