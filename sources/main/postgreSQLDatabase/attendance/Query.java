/**
 * 
 */
package postgreSQLDatabase.attendance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import settings.database.PostgreSQLConnection;

/**
 * @author Dixit
 *
 */
public class Query {
	static Connection conn;

	public static void main(String[] args) {
		//getAllocationList("EC101");
		getCourseList("EC101", 2, "CSE");
	}

	
	public static ArrayList<Allocation> getCourseList(String faculty_id, int semester, String branch) {
		ArrayList<Allocation> allocation_list = null;
		try {
			PreparedStatement proc =  settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getCourseList\"(?,?,?);");
			proc.setString(1, faculty_id);
			proc.setInt(2, semester);
			proc.setString(3, branch);
			ResultSet rs = proc.executeQuery();
			allocation_list = new ArrayList<Allocation>();
			while (rs.next()) {
				
				Allocation allocation = new Allocation();
				allocation.setCourse_code(rs.getString("alloted_course_code"));
				allocation.setSemester(rs.getInt("semester"));
				allocation.setDate(rs.getString("date"));
				allocation.setBranch_name(rs.getString("branch_name"));

				allocation_list.add(allocation);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allocation_list;

	}
	
	public static ArrayList<Allocation> getBranchList(String faculty_id, int semester) {
		ArrayList<Allocation> allocation_list = null;
		try {
			PreparedStatement proc =  settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getBranchName\"(?,?);");
			proc.setString(1, faculty_id);
			proc.setInt(2, semester);
			ResultSet rs = proc.executeQuery();
			
			allocation_list = new ArrayList<Allocation>();
			while(rs.next()) {
				Allocation allocation = new Allocation();
				allocation.setCourse_code(rs.getString("alloted_course_code"));
				allocation.setSemester(rs.getInt("semester"));
				allocation.setDate(rs.getString("date"));
				allocation.setBranch_name(rs.getString("branch_name"));
				allocation_list.add(allocation);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allocation_list;

	}
	
	public static ArrayList<Attendance> getStudentAttendanceList(String course_code,String branch) {
		ArrayList<Attendance> attendance_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getStudentAttendanceList\"(?,?);");
			proc.setString(1, course_code);
			proc.setString(2, branch);
			ResultSet rs = proc.executeQuery();
			attendance_list = new ArrayList<Attendance>();
			while (rs.next()) {
				Attendance attendance = new Attendance();

				attendance.setStudent_id(rs.getString("student_id"));
				attendance.setStudent_name(rs.getString("student_name"));

				attendance_list.add(attendance);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attendance_list;

	}
	
	public static void addAttendance(String class_id, JSONObject[] attendance_json) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		class_id=dateFormat.format(date).toString();
		//System.out.println(class_id);
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addtAttendance\"(?,?);");
			proc.setString(1,class_id);
			proc.setArray(2,PostgreSQLConnection.getConnection().createArrayOf("json", attendance_json));
			proc.executeQuery();
			
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public static ArrayList<Attendance> getAttendanceList(String course_code) {
		ArrayList<Attendance> attendance_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT* from public.\"getStudentAttendanceList\"(?);");
			proc.setString(1, course_code);
			ResultSet rs = proc.executeQuery();
			
			attendance_list = new ArrayList<Attendance>();
			while (rs.next()) {
				Attendance attendance = new Attendance();

				attendance.setStudent_id(rs.getString("student_id"));
				attendance.setStudent_name(rs.getString("student_name"));

				attendance_list.add(attendance);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attendance_list;

	}

	public static ArrayList<Allocation> getAllocationList(String faculty_id) {
		ArrayList<Allocation> allocation_list = null;
		try {
			PreparedStatement proc =PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getSubjectAllocation\"(?);");
			proc.setString(1, faculty_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray jArray = new JSONArray(rs.getString(1));
			allocation_list = new ArrayList<Allocation>();
			while (rs.next()) {
				Allocation allocation = new Allocation();
				allocation.setCourse_code(rs.getString("alloted_course_code"));
				allocation.setSemester(rs.getInt("semester"));
				allocation.setDate(rs.getString("date"));
				allocation.setBranch_name(rs.getString("branch_name"));
				allocation_list.add(allocation);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allocation_list;

	}
	
	public static ArrayList<Allocation> getFacultyCourseCode(String faculty_id, int semester) {
		ArrayList<Allocation> allocation_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getFacultyCourseCode\"(?,?);");
			proc.setString(1, faculty_id);
			proc.setInt(2, semester);
			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray jArray = new JSONArray(rs.getString(1));
			allocation_list = new ArrayList<Allocation>();
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject current = (JSONObject) jArray.get(i);
				Allocation allocation = new Allocation();
				allocation.setCourse_code(current.getString("alloted_course_code"));
				allocation.setSemester(current.getInt("semester"));
				allocation.setDate(current.getString("date"));
				allocation.setBranch_name(current.getString("branch_name"));

				allocation_list.add(allocation);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allocation_list;

	}

}
