/**
 * 
 */
package postgreSQLDatabase.forum;

import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONObject;

import postgreSQLDatabase.feePayment.Payment;
import postgreSQLDatabase.notifications.Notifications;
import settings.database.PostgreSQLConnection;
import forum.Category;
import forum.Comment;
import forum.ForumThread;
import forum.Post;

/**
 * @author Arushi
 *
 */
public class Query {
	static Connection conn;
	private static PreparedStatement proc;
	
	/**
	 * @return a new connection to postgreSQL
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {

		if (conn == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager.getConnection("jdbc:postgresql://172.16.1.231:5432/iiitk", "developer", "developer");
		}
		return conn;
	}
	
	public static void addCategory(String category_name) {
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"addCategory\"(?);");
			proc.setString(1, category_name);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static Long addThread(Long category_id,String thread_name,Long author_id) {
		Long thread_id=null;
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"addThread\"(?,?,?);");
			
			proc.setLong(1, category_id);
			proc.setString(2, thread_name);
			proc.setLong(3, author_id);
			
			ResultSet rs=proc.executeQuery();
			rs.next();
			thread_id=rs.getLong(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return thread_id;
	}
	
	public static void addPost(Long thread_id,Long author_id,String post_name) {
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"addPost\"(?,?,?);");
			proc.setLong(1, thread_id);
			proc.setLong(2, author_id);
			proc.setString(3, post_name);
			
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void addComment(String data,Long post_id,Long author_id) {
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"addComment\"(?,?,?);");
			proc.setString(1, data);
			proc.setLong(2, post_id);
			proc.setLong(3, author_id);
			
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static ForumThread getSingleThread(Long thread_id){
		ForumThread thread=new ForumThread();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getSingleThread\"(?);");
			proc.setLong(1, thread_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				thread.setAuthor_id(rs.getLong("author_id"));
				thread.setParent_category_id(rs.getLong("category_id"));
				thread.setThread_id(rs.getLong("thread_id"));
				thread.setThread_name(rs.getString("thread_name"));
				thread.setTimestamp(rs.getTimestamp("timestamp"));
				
			}
			return thread;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Post getSinglePost(Long post_id){
		Post post=new Post();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getSinglePost\"(?);");
			proc.setLong(1, post_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				post.setAuthor_id(rs.getLong("author_id"));
				post.setParent_thread_id(rs.getLong("thread_id"));
				post.setPost_id(rs.getLong("post_id"));
				post.setPost_name(rs.getString("post_name"));
				post.setTimestamp(rs.getTimestamp("timestamp"));
				
			}
			return post;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Comment getSingleComment(Long comment_id){
		Comment comment=new Comment();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getSingleComment\"(?);");
			proc.setLong(1, comment_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				comment.setAuthor_id(rs.getLong("author_id"));
				comment.setParent_post_id(rs.getLong("post_id"));
				comment.setComment_id(rs.getLong("comment_id"));
				comment.setData(rs.getString("data"));
				comment.setTimestamp(rs.getTimestamp("timestamp"));
				
			}
			return comment;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<Category> getAllCategories(){
		ArrayList<Category> category_info = new ArrayList<Category>();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getAllCategories\"();");
			
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				Category category=new Category();
				category.setCategory_id(rs.getLong("category_id"));
				category.setCategory_name(rs.getString("category_name"));
				category_info.add(category);
				
			}
			return category_info;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<ForumThread> getMultipleThreads(Long category_id){
		ArrayList<ForumThread> thread_info = new ArrayList<ForumThread>();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getMultipleThreads\"(?);");
			proc.setLong(1, category_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				ForumThread thread=new ForumThread();
				thread.setAuthor_id(rs.getLong("author_id"));
				thread.setParent_category_id(rs.getLong("category_id"));
				thread.setThread_id(rs.getLong("thread_id"));
				thread.setThread_name(rs.getString("thread_name"));
				thread.setTimestamp(rs.getTimestamp("timestamp"));
				thread_info.add(thread);
				
			}
			return thread_info;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<Post> getMultiplePosts(Long thread_id){
		ArrayList<Post> post_info = new ArrayList<Post>();
		
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getMultiplePosts\"(?);");
			proc.setLong(1, thread_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				Post post=new Post();
				post.setAuthor_id(rs.getLong("author_id"));
				post.setParent_thread_id(rs.getLong("thread_id"));
				post.setPost_id(rs.getLong("post_id"));
				post.setPost_name(rs.getString("post_name"));
				post.setTimestamp(rs.getTimestamp("timestamp"));
				//System.out.println(a);
				ArrayList<Long> likes_info = new ArrayList<Long>();
				ResultSet as=rs.getArray("likes").getResultSet();
				
				while(as.next()){
					likes_info.add(Long.parseLong(as.getString(2)));
				}
				//System.out.println(likes_info.size()+" likes");
				post.setLikes(likes_info);
				
				post_info.add(post);
				
			}
			return post_info;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<Comment> getMultipleComments(Long post_id){
		ArrayList<Comment> comment_info = new ArrayList<Comment>();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getMultipleComments\"(?);");
			proc.setLong(1, post_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				Comment comment=new Comment();
				comment.setAuthor_id(rs.getLong("author_id"));
				comment.setParent_post_id(rs.getLong("post_id"));
				comment.setComment_id(rs.getLong("comment_id"));
				comment.setData(rs.getString("data"));
				comment.setTimestamp(rs.getTimestamp("timestamp"));
				comment_info.add(comment);
				
			}
			return comment_info;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static String getAuthorName(Long author_id){
		String username=null;
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"getUserName\"(?);");
			proc.setLong(1, author_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				username=rs.getString("getUserName");
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return username;
	}
	
	public static String getCategoryName(Long category_id){
		String categoryname=null;
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"getCategoryName\"(?);");
			proc.setLong(1, category_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				categoryname=rs.getString("getCategoryName");
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categoryname;
	}
	
	public static String getThreadName(Long thread_id){
		String threadname=null;
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"getThreadName\"(?);");
			proc.setLong(1, thread_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				threadname=rs.getString("getThreadName");
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return threadname;
	}
	
	public static String getPostName(Long post_id){
		String postname=null;
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"getPostName\"(?);");
			proc.setLong(1, post_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				postname=rs.getString("getPostName");
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return postname;
	}
	
	public static void deletePost(Long post_id){
		try {
			PreparedStatement proc1 = getConnection()
					.prepareStatement("SELECT public.\"deleteAllComments\"(?);");
			proc1.setLong(1, post_id);
			proc1.executeQuery();
			//proc1.executeQuery();
			
			
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"deletePost\"(?);");
			proc.setLong(1, post_id);
			proc.executeQuery();
			//proc.executeQuery();
			System.out.println(proc);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void deleteComment(Long comment_id){
		try {
			
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"deleteComment\"(?);");
			proc.setLong(1, comment_id);
			proc.executeQuery();
			//proc.executeQuery();
			System.out.println(proc);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public static void addPostSubscriber(Long post_id,Long subscriber_id){
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"addPostSubscriber\"(?,?);");
		proc.setLong(1, post_id);
		proc.setLong(2, subscriber_id);
		proc.executeQuery();
		
		System.out.println(proc);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	public static void addThreadSubscriber(Long thread_id,Long subscriber_id){
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"addThreadSubscriber\"(?,?);");
		proc.setLong(1, thread_id);
		proc.setLong(2, subscriber_id);
		proc.executeQuery();
		
		System.out.println(proc);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	public static void removeThreadSubscriber(Long thread_id,Long subscriber_id){
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"removeThreadSubscriber\"(?,?);");
		proc.setLong(1, thread_id);
		proc.setLong(2, subscriber_id);
		proc.executeQuery();
		
		System.out.println(proc);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	/*public static void sendGroupNotificationForThread(Long thread_id){
		ResultSet rs=null;
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"addGroupNotification\"(?,?,?,?,?,?,?);");
		proc.setString(1, "arushi");
		proc.setString(2, "new msg");
		proc.setString(3, "www.facebook.com");
		proc.setString(4, "gupta");
		Timestamp stamp = new Timestamp(System.currentTimeMillis());
		Date date = new Date(stamp.getTime());
		//System.out.println(date);
		proc.setDate(5,date );
		proc.setDate(6, null);
		try{
			
		PreparedStatement proc1 = getConnection().prepareStatement("SELECT public.\"getThreadNotifGroup\"(?);");
		proc1.setLong(1, thread_id);
		rs=proc1.executeQuery();
		
		System.out.println(proc1);
		rs.next();
		
		}
		
		catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
		proc.setArray(7, rs.getArray(1));
		proc.executeQuery();
		System.out.println(proc);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	public static void sendGroupNotificationForPost(Long post_id){
		ResultSet rs=null;
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"addGroupNotification\"(?,?,?,?,?,?,?);");
		proc.setString(1, "arushi");
		proc.setString(2, "new msg");
		proc.setString(3, "www.facebook.com");
		proc.setString(4, "gupta");
		Timestamp stamp = new Timestamp(System.currentTimeMillis());
		Date date = new Date(stamp.getTime());
		//System.out.println(date);
		proc.setDate(5,date );
		proc.setDate(6, null);
		try{
			
		PreparedStatement proc1 = getConnection().prepareStatement("SELECT public.\"getPostNotifGroup\"(?);");
		proc1.setLong(1, post_id);
		rs=proc1.executeQuery();
		
		System.out.println(proc1);
		rs.next();
		
		}
		
		catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
		proc.setArray(7, rs.getArray(1));
		proc.executeQuery();
		System.out.println(proc);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}*/
	
	public static ArrayList<Long> getAllSubscribersOfThread(Long thread_id){
		ArrayList<Long> subscribers=new ArrayList<Long>();
		try{
			
			PreparedStatement proc1 = getConnection().prepareStatement("SELECT public.\"getThreadNotifGroup\"(?);");
			proc1.setLong(1, thread_id);
			ResultSet rs=proc1.executeQuery();
			
			//System.out.println(proc1);
			rs.next();
			Array a=rs.getArray(1);
			//System.out.println(a);
			ResultSet as=a.getResultSet();
			while(as.next()){
				subscribers.add(Long.parseLong(as.getString(2)));
			}
			/*Iterator<Long> it=subscribers.iterator();
			while(it.hasNext()){
				System.out.println(it.next());
			}*/
			
			
			}
			
			catch (SQLException e) {
			     // TODO Auto-generated catch block
			     e.printStackTrace();
		   }
		return subscribers;
	}
	
	public static ArrayList<Long> getAllSubscribersOfPost(Long post_id){
		ArrayList<Long> subscribers=new ArrayList<Long>();
		try{
			
			PreparedStatement proc1 = getConnection().prepareStatement("SELECT public.\"getPostNotifGroup\"(?);");
			proc1.setLong(1, post_id);
			ResultSet rs=proc1.executeQuery();
			
			//System.out.println(proc1);
			rs.next();
			Array a=rs.getArray(1);
			//System.out.println(a);
			ResultSet as=a.getResultSet();
			while(as.next()){
				subscribers.add(Long.parseLong(as.getString(2)));
			}
			/*Iterator<Long> it=subscribers.iterator();
			while(it.hasNext()){
				System.out.println(it.next());
			}*/
			
			
			}
			
			catch (SQLException e) {
			     // TODO Auto-generated catch block
			     e.printStackTrace();
		   }
		return subscribers;
	}

	/**
	 * @param post_id
	 * @return
	 */
	public static Long getThreadId(Long post_id) {
		// TODO Auto-generated method stub
		Long thread_id=null;
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"getThreadId\"(?);");
		proc.setLong(1, post_id);
		System.out.println(proc);
		ResultSet rs=proc.executeQuery();
		rs.next();
		thread_id=rs.getLong(1);
		System.out.println(thread_id);
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
		return thread_id;
		
	}
	
	public static boolean getThreadSubscriberStatus(Long thread_id,Long subscriber_id){
		boolean status=false;
		ArrayList<Long> subscribers=new ArrayList<Long>();
		subscribers=getAllSubscribersOfThread(thread_id);
		Iterator<Long> it=subscribers.iterator();
		while(it.hasNext()){
			if(it.next().equals(subscriber_id)){
				status=true;
			}
		}
		return status;
	}
	
	public static boolean getPostSubscriberStatus(Long post_id,Long subscriber_id){
		boolean status=false;
		ArrayList<Long> subscribers=new ArrayList<Long>();
		subscribers=getAllSubscribersOfPost(post_id);
		Iterator<Long> it=subscribers.iterator();
		while(it.hasNext()){
			if(it.next().equals(subscriber_id)){
				status=true;
			}
		}
		return status;
	}
	
	public static void editPost(String updated_data,Long post_id){
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"editPost\"(?,?);");
		
		proc.setString(1, updated_data);
		proc.setLong(2, post_id);
		System.out.println(proc);
		ResultSet rs=proc.executeQuery();
		rs.next();
		
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	public static void editComment(String updated_data,Long comment_id){
		try{
			PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"editComment\"(?,?);");
		proc.setLong(1, comment_id);
		proc.setString(2, updated_data);
		System.out.println(proc);
		ResultSet rs=proc.executeQuery();
		rs.next();
		
		
		
	   } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
	   }
	}
	
	public static ArrayList<ForumThread> getMyFeeds(Long erp_id){
		ArrayList<ForumThread> thread_info = new ArrayList<ForumThread>();
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT * from public.\"getMyFeeds\"(?);");
			proc.setLong(1, erp_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				ForumThread thread=new ForumThread();
				thread.setAuthor_id(rs.getLong("author_id"));
				thread.setParent_category_id(rs.getLong("category_id"));
				thread.setThread_id(rs.getLong("thread_id"));
				thread.setThread_name(rs.getString("thread_name"));
				thread.setTimestamp(rs.getTimestamp("timestamp"));
				thread_info.add(thread);
				
			}
			return thread_info;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param post_id
	 */
	public static void likePost(Long post_id,Long erp_id) {
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"likePost\"(?,?);");
			proc.setLong(1, post_id);
			proc.setLong(2, erp_id);
			proc.executeQuery();
			
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void unlikePost(Long post_id,Long erp_id) {
		try {
			PreparedStatement proc = getConnection()
					.prepareStatement("SELECT public.\"unlikePost\"(?,?);");
			proc.setLong(1, post_id);
			proc.setLong(2, erp_id);
			proc.executeQuery();
			
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String args[]){
		//getThreadId(Long.parseLong("26"));
		
		System.out.println();
		
	}

	
}
