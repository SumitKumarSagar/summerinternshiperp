--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.11
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-06-05 12:58:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2495 (class 1262 OID 16398)
-- Name: iiitk; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE iiitk WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE iiitk OWNER TO postgres;

\connect iiitk

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 11787)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 838 (class 1247 OID 17412)
-- Name: csab_type; Type: TYPE; Schema: public; Owner: developer
--

CREATE TYPE csab_type AS (
	name text,
	first_name text,
	middle_name text,
	last_name text,
	category text,
	jee_main_rollno bigint,
	jee_adv_rollno bigint,
	state text,
	phone_number text,
	email text,
	date_of_birth date,
	program_allocated text,
	allocated_category text,
	allocated_rank text,
	status text,
	choice_no integer,
	physically_disabled boolean,
	gender text,
	quota text,
	round integer,
	willingness text,
	address text,
	rc_name text,
	nationality text,
	reported boolean
);


ALTER TYPE csab_type OWNER TO developer;

--
-- TOC entry 900 (class 1247 OID 34233)
-- Name: timetable_type; Type: TYPE; Schema: public; Owner: developer
--

CREATE TYPE timetable_type AS (
	dept integer,
	sem integer,
	batch integer,
	time_slot integer,
	day text,
	subject_name text,
	faculty_name text,
	venue_name text
);


ALTER TYPE timetable_type OWNER TO developer;

--
-- TOC entry 278 (class 1255 OID 16455)
-- Name: addAnswer(text[], integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO public.test_answer(
             answer,question_id)
    VALUES (answer,question_id);

update public."test_answerSheet" set answers=answers ||

(select max(id) from public.test_answer)
where id=answer_sheet_id;$$;


ALTER FUNCTION public."addAnswer"(answer text[], answer_sheet_id integer, question_id integer) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 42499)
-- Name: addAttendance(text, json[]); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addAttendance"(class text, attendance_json json[]) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO attendance_register  select student_id,attendance_status,class AS class_id 
 from json_populate_recordset(null::attendance_register, array_to_json(attendance_json));
  $$;


ALTER FUNCTION public."addAttendance"(class text, attendance_json json[]) OWNER TO developer;

--
-- TOC entry 287 (class 1255 OID 17424)
-- Name: addCSABData(json[]); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addCSABData"(data json[]) RETURNS void
    LANGUAGE sql
    AS $$
  

INSERT INTO csab_student_list (name, first_name, middle_name, last_name, category, jee_main_rollno, 
       jee_adv_rollno, state, phone_number, email, date_of_birth, program_allocated, 
       allocated_category, allocated_rank, status, choice_no, physically_disabled, 
       gender, quota, round, willingness, address, rc_name, nationality,reported)  select name, first_name, middle_name, last_name, category, jee_main_rollno, 
       jee_adv_rollno, state, phone_number, email, date_of_birth, program_allocated, 
       allocated_category, allocated_rank, status, choice_no, physically_disabled, 
       gender, quota, round, willingness, address, rc_name, nationality,reported  from json_populate_recordset(null::"csab_type", array_to_json(data));
  $$;


ALTER FUNCTION public."addCSABData"(data json[]) OWNER TO developer;

--
-- TOC entry 336 (class 1255 OID 42744)
-- Name: addCSABData(text, text, text, text, text, bigint, bigint, text, text, text, date, text, text, text, text, integer, boolean, text, text, integer, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addCSABData"(name text, first_name text, middle_name text, last_name text, category text, jee_main_rollno bigint, jee_adv_rollno bigint, state text, phone_number text, email text, date_of_birth date, program_allocated text, allocated_category text, allocated_rank text, status text, choice_no integer, physically_disabled boolean, gender text, quota text, round integer, willingness text, address text, rc_name text, nationality text) RETURNS void
    LANGUAGE plpgsql
    AS $$
  
begin
INSERT INTO csab_student_list (name, first_name, middle_name, last_name, category, jee_main_rollno, 
       jee_adv_rollno, state, phone_number, email, date_of_birth, program_allocated, 
       allocated_category, allocated_rank, status, choice_no, physically_disabled, 
       gender, quota, round, willingness, address, rc_name, nationality)  values (name, first_name, middle_name, last_name, category, jee_main_rollno, 
       jee_adv_rollno, state, phone_number, email, date_of_birth, program_allocated, 
       allocated_category, allocated_rank, status, choice_no, physically_disabled, 
       gender, quota, round, willingness, address, rc_name, nationality ) ;
 end $$;


ALTER FUNCTION public."addCSABData"(name text, first_name text, middle_name text, last_name text, category text, jee_main_rollno bigint, jee_adv_rollno bigint, state text, phone_number text, email text, date_of_birth date, program_allocated text, allocated_category text, allocated_rank text, status text, choice_no integer, physically_disabled boolean, gender text, quota text, round integer, willingness text, address text, rc_name text, nationality text) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 42484)
-- Name: addCategory(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addCategory"(cat_name text) RETURNS void
    LANGUAGE sql
    AS $$insert into forum_category(category_name) values (cat_name);$$;


ALTER FUNCTION public."addCategory"(cat_name text) OWNER TO developer;

--
-- TOC entry 261 (class 1255 OID 42501)
-- Name: addComment(text, bigint, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addComment"(data text, post_id bigint, erp_id bigint) RETURNS void
    LANGUAGE sql
    AS $$insert into comments
(data,post_id,author_id)
values					
(data,post_id,erp_id);$$;


ALTER FUNCTION public."addComment"(data text, post_id bigint, erp_id bigint) OWNER TO developer;

--
-- TOC entry 286 (class 1255 OID 42882)
-- Name: addDocument(bigint, text, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addDocument"(file_id bigint, document_name text, owner_reg_id bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
declare documentId bigint;
begin  
insert into documents
(file_id,title,owner)
values(file_id,document_name,owner_reg_id) returning document_id into documentId; 
return documentId;
end
$$;


ALTER FUNCTION public."addDocument"(file_id bigint, document_name text, owner_reg_id bigint) OWNER TO developer;

--
-- TOC entry 254 (class 1255 OID 42698)
-- Name: addFeeBreakup(integer, integer, text, json[], integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) RETURNS void
    LANGUAGE sql
    AS $$insert into fee_breakup
(category,total_amt,semester,year,breakup)
values(category,total,sem,batch,breakup);$$;


ALTER FUNCTION public."addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) OWNER TO developer;

--
-- TOC entry 397 (class 1255 OID 34308)
-- Name: addFeeBreakup(integer, integer, text, json, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) RETURNS void
    LANGUAGE sql
    AS $$insert into fee_breakup
values(category,breakup,total,sem,batch);$$;


ALTER FUNCTION public."addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) OWNER TO developer;

--
-- TOC entry 350 (class 1255 OID 33860)
-- Name: addFeePayment(text, bigint, json, bigint, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$declare
refid bigint;
sem integer;
BEGIN
insert into fee_payment
(comment,payment_method,details,amount)
values(fcomment,pay_method,details,amt)
returning ref_no into refid;

update registration_student_list
set payment=array_append(payment,refid),verification_status=3
where id=regid;

select semester into sem from registration_student_list where id=regid;

update fee_transaction set ref_no=array_append(ref_no,refid)
where fee_transaction.reg_id=regid and fee_transaction.semester=sem;
return refid;
END

$$;


ALTER FUNCTION public."addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) OWNER TO developer;

--
-- TOC entry 358 (class 1255 OID 34110)
-- Name: addFeeTransaction(bigint[], integer, text, date, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) RETURNS void
    LANGUAGE sql
    AS $$insert into fee_transaction
(ref_no,reg_id,semester,category,date)
values(ref_no,reg_id,semester,category,date);$$;


ALTER FUNCTION public."addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) OWNER TO developer;

--
-- TOC entry 390 (class 1255 OID 42278)
-- Name: addFile(text, text, text, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addFile"(directory text, name text, extension text, author bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE file_id bigint;
BEGIN
INSERT INTO files (directory,file_name,extension,author)
values(directory,name,extension,author) returning id into file_id;
return file_id;
END;
$$;


ALTER FUNCTION public."addFile"(directory text, name text, extension text, author bigint) OWNER TO developer;

--
-- TOC entry 283 (class 1255 OID 17134)
-- Name: addForm(text, text, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addForm"(formname text, req_fields text, form_format text) RETURNS void
    LANGUAGE sql
    AS $$insert into public.forms(form_name,fields,format)
values(formname,req_fields,form_format);$$;


ALTER FUNCTION public."addForm"(formname text, req_fields text, form_format text) OWNER TO developer;

--
-- TOC entry 269 (class 1255 OID 42859)
-- Name: addGroup(bigint[], text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addGroup"(users_id bigint[], name text) RETURNS void
    LANGUAGE sql
    AS $$
insert into groups(users_id,name)
values(users_id,name);
$$;


ALTER FUNCTION public."addGroup"(users_id bigint[], name text) OWNER TO developer;

--
-- TOC entry 300 (class 1255 OID 33624)
-- Name: addGroupNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
nid bigint;
u_id bigint;
BEGIN
insert into notifications
(notif_type,message,link,notif_timestamp,expiry)
values(ntype,msg,link,notif_timestamp,expiry)
returning notif_id into nid;

FOREACH u_id IN ARRAY userid
LOOP
update users
set unread_notifications=array_append(unread_notifications,nid)
where id=u_id;
END LOOP;
END

$$;


ALTER FUNCTION public."addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) OWNER TO postgres;

--
-- TOC entry 355 (class 1255 OID 33625)
-- Name: addGroupNotification(text, text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
nid bigint;
u_id bigint;
BEGIN
insert into notifications
(notif_type,message,link,notif_author,notif_timestamp,expiry)
values(ntype,msg,link,notif_author,notif_timestamp,expiry)
returning notif_id into nid;

FOREACH u_id IN ARRAY userid
LOOP
update users
set unread_notifications=array_append(unread_notifications,nid)
where id=u_id;
END LOOP;
END

$$;


ALTER FUNCTION public."addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) OWNER TO postgres;

--
-- TOC entry 295 (class 1255 OID 17148)
-- Name: addNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
nid bigint;
BEGIN
insert into notifications
(notif_type,message,link,notif_timestamp,expiry)
values(ntype,msg,link,notif_timestamp,expiry)
returning notif_id into nid;

update users
set unread_notifications=array_append(unread_notifications,nid)
where id=userid;
END

$$;


ALTER FUNCTION public."addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 42498)
-- Name: addPost(bigint, bigint, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addPost"(thread_id bigint, erp_id bigint, post_name text) RETURNS void
    LANGUAGE sql
    AS $$insert into posts(thread_id,author_id,post_name) 
values
(thread_id,erp_id,post_name);$$;


ALTER FUNCTION public."addPost"(thread_id bigint, erp_id bigint, post_name text) OWNER TO developer;

--
-- TOC entry 275 (class 1255 OID 16456)
-- Name: addQuestion(text, text, text[], text[], integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO public.test_question(
             marks, question, type, options, answer)
    VALUES (marks, question, type, options, answer);

update public."test_testPaper" set questions=questions ||

(select max(id) from public.test_question)
where id=testpaper;$$;


ALTER FUNCTION public."addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) OWNER TO postgres;

--
-- TOC entry 386 (class 1255 OID 42679)
-- Name: addRegistrationFeeBreakup(integer, integer, text, json[], integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) RETURNS void
    LANGUAGE sql
    AS $$insert into registration_fee_breakup
(category,total_amt,round,year,breakup)
values(category,total,rnd,year,breakup);$$;


ALTER FUNCTION public."addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) OWNER TO developer;

--
-- TOC entry 391 (class 1255 OID 42681)
-- Name: addRegistrationFeeTransaction(bigint[], bigint, integer, text, date); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) RETURNS void
    LANGUAGE sql
    AS $$insert into registration_fee_Transaction
(ref_no,reg_id,round,category,date)
values(reference_no,reference_id,rnd,cat,dt);$$;


ALTER FUNCTION public."addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) OWNER TO developer;

--
-- TOC entry 277 (class 1255 OID 16457)
-- Name: addRegistrationInformation(text, text, text, text, text, text, date, text, boolean, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO public.registration_student_list(
            name, category, state, address, 
            phone_number, email, date_of_birth, program_allocated,  
            physically_disabled, gender, nationality)
    VALUES (name, category, state, address, 
            phone_number, email, date_of_birth, program_allocated,  
            false, gender, nationality
           ) returning id;$$;


ALTER FUNCTION public."addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) OWNER TO postgres;

--
-- TOC entry 385 (class 1255 OID 34267)
-- Name: addStudent(bigint, text, integer, text, text, text, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO student(
             erp_id, student_id, semester, course, branch, batch,reg_id)
    VALUES (erp_id, sid, sem, course, branch, batch,reg_id);
update registration_Student_list set verification_status=5 where id =reg_id;$$;


ALTER FUNCTION public."addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) OWNER TO developer;

--
-- TOC entry 347 (class 1255 OID 42630)
-- Name: addTags(text, text, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addTags"(t_name text, users text, att text) RETURNS void
    LANGUAGE sql
    AS $$insert into tags
(tagname,users,attributes)
values(t_name,users,att);$$;


ALTER FUNCTION public."addTags"(t_name text, users text, att text) OWNER TO developer;

--
-- TOC entry 365 (class 1255 OID 42792)
-- Name: addTemplates(text, bigint, bigint, text[]); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addTemplates"(title text, file bigint, author bigint, tags text[]) RETURNS void
    LANGUAGE sql
    AS $$insert into templates(title,file,author,tags)
values(title,file,author,tags);
$$;


ALTER FUNCTION public."addTemplates"(title text, file bigint, author bigint, tags text[]) OWNER TO developer;

--
-- TOC entry 294 (class 1255 OID 42731)
-- Name: addTemplates(text, text, bigint, text[]); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addTemplates"(title text, file text, author bigint, tags text[]) RETURNS void
    LANGUAGE sql
    AS $$insert into templates(title,file,author,tags)
values(title,file,author,tags);
$$;


ALTER FUNCTION public."addTemplates"(title text, file text, author bigint, tags text[]) OWNER TO developer;

--
-- TOC entry 306 (class 1255 OID 42734)
-- Name: addThread(bigint, text, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addThread"(cat_id bigint, thread_name text, erp_id bigint) RETURNS bigint
    LANGUAGE sql
    AS $$insert into threads(thread_name,category_id,author_id)
 values 
(thread_name,cat_id,erp_id) returning thread_id;$$;


ALTER FUNCTION public."addThread"(cat_id bigint, thread_name text, erp_id bigint) OWNER TO developer;

--
-- TOC entry 359 (class 1255 OID 34296)
-- Name: addUpdateRegistrationStudentDetails(text, text, text, text, text, bigint, text, text, text, text, text, text, text, text, boolean, json, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN

IF (Select count(*) from update_registration_student_list where registration_id=param_registration_id)>0
THEN 

UPDATE update_registration_student_list SET 
 first_name=param_first_name,  middle_name=param_middle_name,  last_name=param_last_name,   phone_number=param_phone_number, 
             email=param_email, 
           registration_id=param_registration_id,  guardian_name=param_guardian_name,  guardian_contact=param_guardian_contact, 
             guardian_email=param_guardian_email,  guardian_address=param_guardian_address,  father_name=param_father_name,  mother_name=param_mother_name,  father_contact=param_father_contact, 
             mother_contact=param_mother_contact,  permanent_address=param_permanent_address,  local_address=param_local_address,  hosteller=param_hosteller, 
             hostel_address=param_hostel_address where registration_id=param_registration_id;
ELSE
INSERT INTO update_registration_student_list(
             first_name, middle_name, last_name,  phone_number, 
            email,
          registration_id, guardian_name, guardian_contact, 
            guardian_email, guardian_address, father_name, mother_name, father_contact, 
            mother_contact, permanent_address, local_address, hosteller, 
            hostel_address)
    VALUES (param_first_name, param_middle_name, param_last_name, param_phone_number, param_email, param_registration_id, param_guardian_name, param_guardian_contact, param_guardian_email, param_guardian_address, param_father_name, param_mother_name, param_father_contact, param_mother_contact, param_permanent_address, param_local_address, param_hosteller, param_hostel_address);
            update registration_student_list set verification_status=1 where id = param_registration_id;
END IF;
END
$$;


ALTER FUNCTION public."addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 17468)
-- Name: addUser(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addUser"(user_name text) RETURNS bigint
    LANGUAGE sql
    AS $$INSERT INTO users(username) VALUES (user_name) returning id;$$;


ALTER FUNCTION public."addUser"(user_name text) OWNER TO developer;

--
-- TOC entry 288 (class 1255 OID 17473)
-- Name: addUser(text, text, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addUser"(val_user_name text, val_name text, val_user_type text) RETURNS bigint
    LANGUAGE sql
    AS $$INSERT INTO users(username,name,user_type) VALUES (val_user_name,val_name,val_user_type) returning id;$$;


ALTER FUNCTION public."addUser"(val_user_name text, val_name text, val_user_type text) OWNER TO developer;

--
-- TOC entry 380 (class 1255 OID 34188)
-- Name: addUsernameGeneration(text[]); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "addUsernameGeneration"(usernames text[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
name_user text;
BEGIN
FOREACH name_user IN ARRAY usernames
LOOP
insert into user_name_generation
(username,used)values(name_user,false);
END LOOP;

END

$$;


ALTER FUNCTION public."addUsernameGeneration"(usernames text[]) OWNER TO developer;

--
-- TOC entry 375 (class 1255 OID 34124)
-- Name: applyUpdate(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "applyUpdate"(reg_id bigint) RETURNS void
    LANGUAGE sql
    AS $$update registration_student_list
set  first_name=update_registration_student_list.first_name, 
middle_name=update_registration_student_list.middle_name, 
last_name=update_registration_student_list.last_name, 
phone_number=update_registration_student_list.phone_number, 
email=update_registration_student_list.email, 
date_of_birth=update_registration_student_list.date_of_birth,
 guardian_name=update_registration_student_list.guardian_name, 
guardian_contact=update_registration_student_list.guardian_contact,
guardian_email=update_registration_student_list.guardian_email, 
guardian_address=update_registration_student_list.guardian_address, 
father_name=update_registration_student_list.father_name, 
mother_name=update_registration_student_list.mother_name, 
father_contact=update_registration_student_list.father_contact, 
mother_contact=update_registration_student_list.mother_contact, 
permanent_address=update_registration_student_list.permanent_address, 
local_address=update_registration_student_list.local_address, 
hosteller=update_registration_student_list.hosteller, 
hostel_address=update_registration_student_list.hostel_address,  
entry_time=update_registration_student_list.entry_time

from update_registration_student_list
where registration_student_list.id =update_registration_student_list.registration_id
and update_registration_student_list.registration_id = reg_id;$$;


ALTER FUNCTION public."applyUpdate"(reg_id bigint) OWNER TO developer;

--
-- TOC entry 284 (class 1255 OID 17236)
-- Name: broadcastNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
nid bigint;
user_id bigint;
BEGIN
insert into notifications
(notif_type,message,link,notif_timestamp,expiry)
values(ntype,msg,link,notif_timestamp,expiry)
returning notif_id into nid;

FOREACH user_id IN ARRAY userid
    LOOP
    update users
    set unread_notifications=array_append(unread_notifications,nid)
where id=user_id;    
  
END LOOP;     

END

$$;


ALTER FUNCTION public."broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 26288)
-- Name: calculateTotal(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "calculateTotal"(answer_sheet_id integer) RETURNS void
    LANGUAGE sql
    AS $$
update public."test_answerSheet" set total_marks =( select sum(marks)  from public."test_answer" where id in
(select unnest (answers)
from public."test_answerSheet" where id=answer_sheet_id)) where id=answer_sheet_id;


$$;


ALTER FUNCTION public."calculateTotal"(answer_sheet_id integer) OWNER TO postgres;

--
-- TOC entry 293 (class 1255 OID 16765)
-- Name: createNotification(text, text, text, timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) RETURNS bigint
    LANGUAGE sql
    AS $$insert into notifications
(notif_type, message, link, notif_timestamp, expiry)
values (notif_type, message, link, notif_timestamp, expiry)
returning notif_id;
$$;


ALTER FUNCTION public."createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) OWNER TO postgres;

--
-- TOC entry 338 (class 1255 OID 42750)
-- Name: deleteAllComments(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "deleteAllComments"(p_id bigint) RETURNS void
    LANGUAGE sql
    AS $$delete from comments where post_id=p_id;$$;


ALTER FUNCTION public."deleteAllComments"(p_id bigint) OWNER TO developer;

--
-- TOC entry 339 (class 1255 OID 42751)
-- Name: deleteComment(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "deleteComment"(c_id bigint) RETURNS void
    LANGUAGE sql
    AS $$delete from comments where comment_id=c_id;$$;


ALTER FUNCTION public."deleteComment"(c_id bigint) OWNER TO developer;

--
-- TOC entry 393 (class 1255 OID 42682)
-- Name: deleteFalseUsername(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "deleteFalseUsername"() RETURNS void
    LANGUAGE sql
    AS $$delete from user_name_generation where used=false;$$;


ALTER FUNCTION public."deleteFalseUsername"() OWNER TO developer;

--
-- TOC entry 374 (class 1255 OID 34047)
-- Name: deleteFile(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "deleteFile"(dir text) RETURNS void
    LANGUAGE sql
    AS $$delete from files where directory = dir$$;


ALTER FUNCTION public."deleteFile"(dir text) OWNER TO developer;

--
-- TOC entry 337 (class 1255 OID 42749)
-- Name: deletePost(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "deletePost"(p_id bigint) RETURNS void
    LANGUAGE sql
    AS $$delete from posts where post_id=p_id;$$;


ALTER FUNCTION public."deletePost"(p_id bigint) OWNER TO developer;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 203 (class 1259 OID 17432)
-- Name: csab_student_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE csab_student_list (
    name text NOT NULL,
    first_name text,
    middle_name text,
    last_name text,
    category text,
    jee_main_rollno bigint,
    jee_adv_rollno bigint,
    state text,
    phone_number text,
    email text,
    date_of_birth date,
    program_allocated text,
    allocated_category text,
    allocated_rank text,
    status text,
    choice_no integer,
    physically_disabled boolean,
    gender text,
    quota text,
    round integer,
    willingness text,
    address text,
    rc_name text,
    nationality text,
    csab_id bigint NOT NULL,
    entry_date timestamp without time zone DEFAULT timezone('utc'::text, now()),
    reported boolean DEFAULT false,
    registration_id bigint
);


ALTER TABLE csab_student_list OWNER TO postgres;

--
-- TOC entry 360 (class 1255 OID 42373)
-- Name: displayCsabList(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "displayCsabList"() RETURNS SETOF csab_student_list
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select * from csab_student_list order by registration_id DESC);
END;
$$;


ALTER FUNCTION public."displayCsabList"() OWNER TO postgres;

--
-- TOC entry 373 (class 1255 OID 42654)
-- Name: displayCsabProfile(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "displayCsabProfile"(id bigint) RETURNS SETOF csab_student_list
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select * from csab_student_list 
where csab_student_list.csab_id=id);
END
$$;


ALTER FUNCTION public."displayCsabProfile"(id bigint) OWNER TO postgres;

--
-- TOC entry 332 (class 1255 OID 34245)
-- Name: displayFaculty(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "displayFaculty"(faculty text) RETURNS json
    LANGUAGE plpgsql
    AS $$
declare
d text;
js json;
tim integer;
venue text;
sub text;

BEGIN
select day into d
from tt_save_timetable
where faculty_name=faculty;

select venue_name into venue 
from tt_save_timetable
where faculty_name=faculty;

select time_slot into tim 
from tt_save_timetable
where faculty_name=faculty;

select subject_name into sub 
from tt_save_timetable
where faculty_name=faculty;


select "displayFaculty"(d,venue,tim,sub) into js;
return js;
END
$$;


ALTER FUNCTION public."displayFaculty"(faculty text) OWNER TO developer;

--
-- TOC entry 252 (class 1255 OID 17565)
-- Name: existsRegId(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "existsRegId"(reg_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
count bigint;
BEGIN
select count(id)from registration_student_list where id=reg_id into count;
if count=0
then
return false;
else
return true;
end if;
END

$$;


ALTER FUNCTION public."existsRegId"(reg_id bigint) OWNER TO developer;

--
-- TOC entry 383 (class 1255 OID 34263)
-- Name: facultyAvail(text, integer, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "facultyAvail"(day2 text, time1 integer, faculty text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
var integer;
BEGIN
select count(*) from tt_save_timetable where tt_save_timetable.day=day2 and time_slot=time1 and faculty_name=faculty into var;
return var;
END

$$;


ALTER FUNCTION public."facultyAvail"(day2 text, time1 integer, faculty text) OWNER TO developer;

--
-- TOC entry 382 (class 1255 OID 34262)
-- Name: facultyAvailability(text, integer, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "facultyAvailability"(day text, time1 integer, faculty text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
var integer;
BEGIN
select count(*) from tt_save_timetable where tt_save_timetable.day=day and time_slot=time1 and faculty_name=faculty into var;
return var;
END

$$;


ALTER FUNCTION public."facultyAvailability"(day text, time1 integer, faculty text) OWNER TO developer;

--
-- TOC entry 285 (class 1255 OID 17374)
-- Name: feeVerify(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "feeVerify"(ref bigint) RETURNS void
    LANGUAGE sql
    AS $$update fee_payment
set verified = true
where ref_no=ref;

$$;


ALTER FUNCTION public."feeVerify"(ref bigint) OWNER TO developer;

--
-- TOC entry 226 (class 1259 OID 42475)
-- Name: forum_category; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE forum_category (
    category_id bigint NOT NULL,
    category_name text
);


ALTER TABLE forum_category OWNER TO developer;

--
-- TOC entry 335 (class 1255 OID 42583)
-- Name: getAllCategories(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getAllCategories"() RETURNS SETOF forum_category
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from forum_category);
END
$$;


ALTER FUNCTION public."getAllCategories"() OWNER TO developer;

--
-- TOC entry 313 (class 1255 OID 16808)
-- Name: getAllConversationMessages(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAllConversationMessages"(convo_id integer) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
   messages_id integer[]; 
   message integer;
   msg_text json;
   message_text json[];
BEGIN
   messages_id="getMessagesId"(convo_id);
   
  FOREACH message IN ARRAY messages_id
    LOOP
      select json_agg(r) from( select *,"getUserName"(messages.author) as username FROM messages where messages.id=message into msg_text)r;
     
  message_text = array_append(message_text,msg_text);
  
END LOOP;     
 RETURN message_text;
END

$$;


ALTER FUNCTION public."getAllConversationMessages"(convo_id integer) OWNER TO postgres;

--
-- TOC entry 312 (class 1255 OID 42976)
-- Name: getAllFileData(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAllFileData"(fid bigint) RETURNS TABLE(author bigint, directory text, subdirectory text, file_name text, extension text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN  QUERY(select files.author,files.directory,files.subdirectory,files.file_name,files.extension from files where files.id=fid
);
END;
$$;


ALTER FUNCTION public."getAllFileData"(fid bigint) OWNER TO postgres;

--
-- TOC entry 319 (class 1255 OID 17295)
-- Name: getAllFoo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAllFoo"() RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
    
    member text;
    member_id bigint;
    id_list bigint[];
    convo_id integer;
    convo_id_list integer[];
    convo bigint;
    chat integer;
    con integer[];
BEGIN

   FOREACH member IN ARRAY members
    LOOP
       member_id="getUserId"(member);
id_list = array_append(id_list,member_id);    
END LOOP;
 
 FOREACH member IN ARRAY members
    LOOP
       member_id="getUserId"(member);
INSERT INTO conversations(
            messages, unread, members_id)
    VALUES ('{}', '{}', id_list) returning id INTO convo_id;
    convo_id_list=array_append(convo_id_list,convo_id);
    UPDATE users
   SET  conversations=array_append(conversations,convo_id)
 WHERE id=member_id;    
END LOOP;     
 INSERT INTO chat(
           conversation)
    VALUES (convo_id_list) returning id into chat ;
    
con= "getConversationId"(chat);
FOREACH convo IN ARRAY con
    LOOP
       UPDATE conversations
   SET  chat_id=chat
 WHERE id=convo;
END LOOP;
  

END

$$;


ALTER FUNCTION public."getAllFoo"() OWNER TO postgres;

--
-- TOC entry 263 (class 1255 OID 42844)
-- Name: getAllTemplates(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAllTemplates"() RETURNS TABLE(title text, file bigint, author bigint, tags text[])
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN  QUERY(select templates.title,templates.file,templates.author,templates.tags from public."templates" 
);
END;
$$;


ALTER FUNCTION public."getAllTemplates"() OWNER TO postgres;

--
-- TOC entry 368 (class 1255 OID 25433)
-- Name: getAllUnreadMessages(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAllUnreadMessages"(user_id bigint) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
   convo_array integer[]; 
   convo_id integer;
   message_text json;
   messages_text json[];
   msg_info json;
   chat_name text;
BEGIN
   select unread_conversations from users where id=user_id into convo_array;
   
  FOREACH convo_id IN ARRAY convo_array
    
    LOOP
       msg_info="retrieveUnreadMessages"(convo_id);
       
   select "conversations".chat_name from "conversations" where id=convo_id into chat_name;

  select json_agg(r) from (select convo_id , chat_name,msg_info)r into message_text;
		 messages_text =messages_text ||message_text; 
 END LOOP;  
        return messages_text;          
                   
END

$$;


ALTER FUNCTION public."getAllUnreadMessages"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16408)
-- Name: test_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test_answer (
    answer text[],
    question_id integer,
    id integer NOT NULL,
    marks integer DEFAULT 0,
    correct boolean
);


ALTER TABLE test_answer OWNER TO postgres;

--
-- TOC entry 401 (class 1255 OID 42292)
-- Name: getAnswer(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getAnswer"(answer_sheet_id integer) RETURNS SETOF test_answer
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN  QUERY(select * from public."test_answer" where id in
(select unnest (answers)
from public."test_answerSheet" where id=answer_sheet_id));
END;
$$;


ALTER FUNCTION public."getAnswer"(answer_sheet_id integer) OWNER TO postgres;

--
-- TOC entry 400 (class 1255 OID 42294)
-- Name: getAnswerSheet(integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getAnswerSheet"(test_id integer) RETURNS TABLE(status text, id integer, submission_time timestamp without time zone, total_marks integer, author text)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY(SELECT "test_answerSheet".status, "test_answerSheet".id,  "test_answerSheet".submission_time,"test_answerSheet".total_marks,public."getUserName" ("test_answerSheet".author) as author
  FROM public."test_answerSheet"  where test_paper_id=test_id);
  END
$$;


ALTER FUNCTION public."getAnswerSheet"(test_id integer) OWNER TO developer;

--
-- TOC entry 270 (class 1255 OID 42326)
-- Name: getBranchName(text, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getBranchName"(faculty text, sem_no integer) RETURNS TABLE(branch_name text, semester integer, faculty_id text, alloted_course_code text, date timestamp without time zone, batch_id text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY( SELECT DISTINCT on (branch_name)faculty_alloted_sub.branch_name ,faculty_alloted_sub.semester,faculty_alloted_sub.faculty_id,faculty_alloted_sub.alloted_course_code,faculty_alloted_sub.date, faculty_alloted_sub.batch_id
  FROM faculty_alloted_sub where faculty_alloted_sub.faculty_id=faculty and faculty_alloted_sub.semester=sem_no);
  END
  $$;


ALTER FUNCTION public."getBranchName"(faculty text, sem_no integer) OWNER TO developer;

--
-- TOC entry 257 (class 1255 OID 42490)
-- Name: getCategory(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCategory"(cat_id bigint) RETURNS SETOF forum_category
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from forum_category where category_id = cat_id);
END
$$;


ALTER FUNCTION public."getCategory"(cat_id bigint) OWNER TO developer;

--
-- TOC entry 326 (class 1255 OID 42581)
-- Name: getCategoryName(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCategoryName"(cat_id bigint) RETURNS text
    LANGUAGE sql
    AS $$select category_name from forum_category where category_id=cat_id;$$;


ALTER FUNCTION public."getCategoryName"(cat_id bigint) OWNER TO developer;

--
-- TOC entry 328 (class 1255 OID 16819)
-- Name: getChatId(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getChatId"(conversation_id bigint) RETURNS bigint
    LANGUAGE sql
    AS $$Select chat_id from conversations where id=conversation_id$$;


ALTER FUNCTION public."getChatId"(conversation_id bigint) OWNER TO postgres;

--
-- TOC entry 304 (class 1255 OID 42342)
-- Name: getChatMessages(bigint, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) RETURNS TABLE("timestamp" timestamp without time zone, text text, id bigint, author bigint, username text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY (select *,"getUserName"(messages.author) as username from messages where messages.id in 
( SELECT * FROM (select unnest ( messages) as id from conversations where conversations.id=conversation_id  order by id  desc limit limit_value offset set*limit_value ) as t1  ORDER BY id ASC));
END
$$;


ALTER FUNCTION public."getChatMessages"(conversation_id bigint, set integer, limit_value integer) OWNER TO postgres;

--
-- TOC entry 329 (class 1255 OID 33691)
-- Name: getChatName(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getChatName"(chat bigint) RETURNS text
    LANGUAGE sql
    AS $$select chat_name from public."conversations" where "conversations".chat_id=chat; $$;


ALTER FUNCTION public."getChatName"(chat bigint) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 16659)
-- Name: getConversationId(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getConversationId"(chat_id bigint) RETURNS integer[]
    LANGUAGE sql
    AS $$ SELECT conversation
  FROM chat where id = chat_id ;$$;


ALTER FUNCTION public."getConversationId"(chat_id bigint) OWNER TO postgres;

--
-- TOC entry 330 (class 1255 OID 42718)
-- Name: getConversationsInfo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getConversationsInfo"(user_id bigint) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
   convo_id bigint[] ;
   con integer;
   name text;
   member_names text[];
   name_of_chat text;
   members bigint[];
   chat bigint;
   mem_id bigint;
   conversations_info json[];
   convo json;
   
BEGIN
  select conversations from public."users" where users.id=user_id into convo_id ;
     FOREACH con IN ARRAY convo_id
		    LOOP
		      select members_id from public."conversations" where conversations.id=con into members;
		      select chat_id from public."conversations" where conversations.id = con into chat;
		      name_of_chat="getChatName"(chat);
		      member_names=ARRAY[]::text[];
		      FOREACH mem_id IN ARRAY members
		        LOOP
		             name="getUserName"(mem_id);
		             member_names=array_append(member_names,name);
		            
		       END LOOP;   
		   SELECT json_agg(t) FROM (SELECT member_names AS member_list, name_of_chat AS chatname,con AS conversation_id) t into convo;
		  conversations_info =conversations_info ||convo; 
 END LOOP;  
                  
                  return  conversations_info;
END

$$;


ALTER FUNCTION public."getConversationsInfo"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 362 (class 1255 OID 42642)
-- Name: getCourseBySemYear(integer, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) RETURNS record
    LANGUAGE sql
    AS $$SELECT course_code, credits
  FROM course_allocation where year=input_year and
  semester=input_semester;
$$;


ALTER FUNCTION public."getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) OWNER TO developer;

--
-- TOC entry 253 (class 1255 OID 42489)
-- Name: getCourseGradeList(text, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCourseGradeList"(input_course_code text, registration_year integer) RETURNS TABLE(student_id text, course_code text, student_name text, grade text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(SELECT course_registration.student_id, course_registration.course_code,"getUserName"(student.erp_id)as student_name,course_registration.grade from student join course_registration on
 student.student_id=course_registration.student_id where course_registration.course_code=input_course_code 
 and extract(year from course_registration.registration_date)=registration_year);
END
$$;


ALTER FUNCTION public."getCourseGradeList"(input_course_code text, registration_year integer) OWNER TO developer;

--
-- TOC entry 349 (class 1255 OID 42362)
-- Name: getCourseList(text, integer, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCourseList"(faculty text, sem_no integer, branch text) RETURNS TABLE(alloted_course_code text, semester integer, branch_name text, faculty_id text, date timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY( SELECT DISTINCT on (alloted_course_code)faculty_alloted_sub.alloted_course_code ,faculty_alloted_sub.semester,faculty_alloted_sub.branch_name,faculty_alloted_sub.faculty_id,faculty_alloted_sub.date
  FROM faculty_alloted_sub where faculty_alloted_sub.faculty_id=faculty and faculty_alloted_sub.semester=sem_no and faculty_alloted_sub.branch_name=branch);
  END
  $$;


ALTER FUNCTION public."getCourseList"(faculty text, sem_no integer, branch text) OWNER TO developer;

--
-- TOC entry 356 (class 1255 OID 42544)
-- Name: getCourse_code(integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getCourse_code"(sem integer) RETURNS TABLE(course_code text)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY(select subject_list.course_code 
from subject_list
where subject_list.semester=sem);
END;
$$;


ALTER FUNCTION public."getCourse_code"(sem integer) OWNER TO developer;

--
-- TOC entry 303 (class 1255 OID 17543)
-- Name: getCoversationsInfo(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getCoversationsInfo"(user_id bigint) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
   convo_id bigint[] ;
   con integer;
   name text;
   member_names text[];
   name_of_chat text;
   members bigint[];
   chat bigint;
   mem_id bigint;
   conversations_info json[];
   convo json;
   
BEGIN
  select conversations from public."users" where users.id=user_id into convo_id ;
     FOREACH con IN ARRAY convo_id
		    LOOP
		      select members_id from public."conversations" where conversations.id=con into members;
		      select chat_id from public."conversations" where conversations.id = con into chat;
		      name_of_chat="getChatName"(chat);
		      member_names=ARRAY[]::text[];
		      FOREACH mem_id IN ARRAY members
		        LOOP
		             name="getUserName"(mem_id);
		             member_names=array_append(member_names,name);
		            
		       END LOOP;   
		   SELECT json_agg(t) FROM (SELECT member_names AS member_list, name_of_chat AS chatname,con AS conversation_id) t into convo;
		  conversations_info =conversations_info ||convo; 
 END LOOP;  
                  
                  return  conversations_info;
END

$$;


ALTER FUNCTION public."getCoversationsInfo"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 42506)
-- Name: getCredit(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getCredit"(course_code text) RETURNS integer
    LANGUAGE sql
    AS $$ SELECT credit
  FROM subject_list where subject_list.course_code = course_code ;$$;


ALTER FUNCTION public."getCredit"(course_code text) OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 34032)
-- Name: files; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE files (
    id bigint NOT NULL,
    directory text,
    file_name text,
    extension text,
    author bigint,
    subdirectory text
);


ALTER TABLE files OWNER TO developer;

--
-- TOC entry 348 (class 1255 OID 42360)
-- Name: getDirectory(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getDirectory"(dir text) RETURNS SETOF files
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from files where directory = dir);
END
$$;


ALTER FUNCTION public."getDirectory"(dir text) OWNER TO developer;

--
-- TOC entry 236 (class 1259 OID 42865)
-- Name: documents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE documents (
    file_id bigint,
    title text,
    "timestamp" timestamp without time zone DEFAULT now(),
    owner bigint,
    document_id bigint NOT NULL
);


ALTER TABLE documents OWNER TO postgres;

--
-- TOC entry 296 (class 1255 OID 42888)
-- Name: getDocuments(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getDocuments"(owner_id bigint) RETURNS SETOF documents
    LANGUAGE plpgsql
    AS $$ 
begin
return query (SELECT * 
  FROM documents where owner = owner_id) ;
end
  $$;


ALTER FUNCTION public."getDocuments"(owner_id bigint) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 42328)
-- Name: getFacultyCourseCode(text, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFacultyCourseCode"(faculty text, semester_no integer) RETURNS TABLE(allocated_course_code text, semester integer, faculty_id text, date timestamp without time zone, batch_id text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY( SELECT DISTINCT on (alloted_course_code)faculty_alloted_sub.alloted_course_code ,faculty_alloted_sub.semester,faculty_alloted_sub.faculty_id, faculty_alloted_sub.date, faculty_alloted_sub.batch_id
  FROM faculty_alloted_sub where faculty_alloted_sub.faculty_id=faculty and semester=semester_no);
  END
  $$;


ALTER FUNCTION public."getFacultyCourseCode"(faculty text, semester_no integer) OWNER TO developer;

--
-- TOC entry 396 (class 1255 OID 34317)
-- Name: getFeeAmount(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getFeeAmount"(userid bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
cat text;
amt integer;
BEGIN
select category into cat
from registration_student_list
where id=userid;
select "getFeeAmount"('2',cat,'2014') into amt;
return amt;
END

$$;


ALTER FUNCTION public."getFeeAmount"(userid bigint) OWNER TO postgres;

--
-- TOC entry 323 (class 1255 OID 34316)
-- Name: getFeeAmount(integer, text, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeeAmount"(sem integer, cat text, bat integer) RETURNS integer
    LANGUAGE sql
    AS $$select total_amt
 from fee_breakup
  where year=bat and semester=sem and category=cat $$;


ALTER FUNCTION public."getFeeAmount"(sem integer, cat text, bat integer) OWNER TO developer;

--
-- TOC entry 193 (class 1259 OID 16970)
-- Name: fee_breakup; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE fee_breakup (
    category text NOT NULL,
    total_amt integer,
    semester integer NOT NULL,
    year integer NOT NULL,
    breakup json[]
);


ALTER TABLE fee_breakup OWNER TO developer;

--
-- TOC entry 311 (class 1255 OID 42740)
-- Name: getFeeBreakup(integer, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeeBreakup"(sem integer, acad_year integer) RETURNS SETOF fee_breakup
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY
(select * from public."fee_breakup" where fee_breakup.semester = sem and fee_breakup.year = acad_year
);
END
$$;


ALTER FUNCTION public."getFeeBreakup"(sem integer, acad_year integer) OWNER TO developer;

--
-- TOC entry 309 (class 1255 OID 42343)
-- Name: getFeeBreakup(integer, integer, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeeBreakup"(year integer, sem integer, user_id bigint) RETURNS TABLE(cat text)
    LANGUAGE plpgsql
    AS $$

declare
cat text;

BEGIN
select category into cat
from registration_student_list
where id=user_id;
select "getFeeBreakup"(year,sem,cat) ;

END

$$;


ALTER FUNCTION public."getFeeBreakup"(year integer, sem integer, user_id bigint) OWNER TO developer;

--
-- TOC entry 307 (class 1255 OID 42340)
-- Name: getFeeBreakup(integer, integer, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) RETURNS SETOF fee_breakup
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
(select * from fee_breakup 
where year=bat and semester=sem and category=cat 
);
END
$$;


ALTER FUNCTION public."getFeeBreakup"(bat integer, sem integer, cat text) OWNER TO developer;

--
-- TOC entry 394 (class 1255 OID 34321)
-- Name: getFeeJson(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getFeeJson"(userid bigint) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
cat text;
js json[];
current_year integer;
sem integer;
BEGIN
select category into cat 
from registration_student_list
where id=userid;

select "registration_student_list".semester into sem
from registration_student_list
where id=userid;

select extract (year from now()) into current_year;

select "getFeeJson"(current_year,sem,cat) into js;
return js;
END

$$;


ALTER FUNCTION public."getFeeJson"(userid bigint) OWNER TO postgres;

--
-- TOC entry 324 (class 1255 OID 34320)
-- Name: getFeeJson(integer, integer, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeeJson"(fee_year integer, sem integer, cat text) RETURNS json[]
    LANGUAGE sql
    AS $$select breakup from fee_breakup 
where year=year and semester=sem and category=cat $$;


ALTER FUNCTION public."getFeeJson"(fee_year integer, sem integer, cat text) OWNER TO developer;

--
-- TOC entry 318 (class 1255 OID 42346)
-- Name: getFeePaymentDetails(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeePaymentDetails"(rid bigint) RETURNS TABLE(name text, details json, payment_method bigint, ref_no bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select a.name,b.details,b.payment_method,b.ref_no
from registration_student_list a,fee_payment b
where (select registration_student_list.payment[array_upper(registration_student_list.payment,1)]from registration_student_list where id=rid)=b.ref_no and a.id=rid);
END
$$;


ALTER FUNCTION public."getFeePaymentDetails"(rid bigint) OWNER TO developer;

--
-- TOC entry 199 (class 1259 OID 17361)
-- Name: fee_payment; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE fee_payment (
    ref_no bigint NOT NULL,
    comment text,
    details json,
    amount bigint,
    verified boolean DEFAULT false,
    payment_method bigint
);


ALTER TABLE fee_payment OWNER TO developer;

--
-- TOC entry 316 (class 1255 OID 42345)
-- Name: getFeePaymentHistory(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeePaymentHistory"(userid bigint) RETURNS SETOF fee_payment
    LANGUAGE plpgsql
    AS $$
declare
ref_id bigint[];

BEGIN
select payment into ref_id
from registration_student_list
where id=userid;
 
  
 RETURN QUERY(select * from fee_payment where ref_no=ANY(ref_id));
 
END
$$;


ALTER FUNCTION public."getFeePaymentHistory"(userid bigint) OWNER TO developer;

--
-- TOC entry 322 (class 1255 OID 42349)
-- Name: getFeePaymentList(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFeePaymentList"() RETURNS SETOF fee_breakup
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from fee_breakup);
END
$$;


ALTER FUNCTION public."getFeePaymentList"() OWNER TO developer;

--
-- TOC entry 354 (class 1255 OID 42778)
-- Name: getFileId(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getFileId"(f_name text) RETURNS bigint
    LANGUAGE sql
    AS $$select files.id from files where files.file_name=f_name;$$;


ALTER FUNCTION public."getFileId"(f_name text) OWNER TO postgres;

--
-- TOC entry 334 (class 1255 OID 42356)
-- Name: getFormFormat(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFormFormat"(formname text) RETURNS TABLE(format text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select forms.format from public."forms"where forms.form_name=formname);
END
$$;


ALTER FUNCTION public."getFormFormat"(formname text) OWNER TO developer;

--
-- TOC entry 340 (class 1255 OID 42358)
-- Name: getFormName(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getFormName"() RETURNS TABLE(form_name text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select forms.form_name from public."forms");
END
$$;


ALTER FUNCTION public."getFormName"() OWNER TO developer;

--
-- TOC entry 315 (class 1255 OID 33700)
-- Name: getLDAPStudentStatus(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getLDAPStudentStatus"(rid bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
temp1 boolean;
temp2 boolean;
ab json;
BEGIN
select verified into temp1
from registration_student_list
where id=rid;

SELECT json_agg(r) into ab 
from(select a.name,b.details,b.payment_method,b.ref_no
from registration_student_list a,fee_payment b
where (select registration_student_list.payment[array_upper(registration_student_list.payment,1)]from registration_student_list where id=rid)=b.ref_no and a.id=rid)r;
return json; 
select verified into temp2 
from fee_payment
where ref_no = ab;

if(temp1 and temp2)
then
return true;
else 
return false;
END IF;
end

$$;


ALTER FUNCTION public."getLDAPStudentStatus"(rid bigint) OWNER TO developer;

--
-- TOC entry 292 (class 1255 OID 16735)
-- Name: getMessagesId(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getMessagesId"(convo_id integer) RETURNS integer[]
    LANGUAGE sql
    AS $$
 select messages from public."conversations" where id =convo_id
;$$;


ALTER FUNCTION public."getMessagesId"(convo_id integer) OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 42431)
-- Name: comments; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE comments (
    data text,
    post_id bigint,
    "timestamp" timestamp without time zone DEFAULT now(),
    author_id bigint,
    comment_id bigint NOT NULL
);


ALTER TABLE comments OWNER TO developer;

--
-- TOC entry 321 (class 1255 OID 42546)
-- Name: getMultipleComments(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getMultipleComments"(p_id bigint) RETURNS SETOF comments
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from comments where post_id = p_id);
END
$$;


ALTER FUNCTION public."getMultipleComments"(p_id bigint) OWNER TO developer;

--
-- TOC entry 223 (class 1259 OID 42415)
-- Name: posts; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE posts (
    post_id bigint NOT NULL,
    thread_id bigint,
    author_id bigint,
    post_name text,
    "timestamp" timestamp without time zone DEFAULT now()
);


ALTER TABLE posts OWNER TO developer;

--
-- TOC entry 305 (class 1255 OID 42545)
-- Name: getMultiplePosts(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getMultiplePosts"(t_id bigint) RETURNS SETOF posts
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from posts where thread_id = t_id);
END
$$;


ALTER FUNCTION public."getMultiplePosts"(t_id bigint) OWNER TO developer;

--
-- TOC entry 221 (class 1259 OID 42404)
-- Name: threads; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE threads (
    thread_id bigint NOT NULL,
    thread_name text,
    category_id bigint,
    author_id bigint,
    "timestamp" timestamp without time zone DEFAULT now(),
    first_post_id bigint
);


ALTER TABLE threads OWNER TO developer;

--
-- TOC entry 301 (class 1255 OID 42542)
-- Name: getMultipleThreads(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getMultipleThreads"(cat_id bigint) RETURNS SETOF threads
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from threads where category_id = cat_id);
END
$$;


ALTER FUNCTION public."getMultipleThreads"(cat_id bigint) OWNER TO developer;

--
-- TOC entry 251 (class 1255 OID 42697)
-- Name: getPaymentDetails(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getPaymentDetails"(rid bigint) RETURNS json[]
    LANGUAGE plpgsql
    AS $$
declare
pay integer[];
i integer;
detail_name json;
arr json[];

BEGIN
select payment 
from registration_student_list where id=rid  into pay;

FOREACH i IN ARRAY pay
    LOOP
select  json_agg(r) from( select details  from fee_payment where ref_no=i into detail_name)r;
arr = array_append(arr,detail_name);
    END LOOP;  
 RETURN arr;
END


$$;


ALTER FUNCTION public."getPaymentDetails"(rid bigint) OWNER TO developer;

--
-- TOC entry 302 (class 1255 OID 33653)
-- Name: getProgram(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getProgram"(sid bigint) RETURNS text
    LANGUAGE sql
    AS $$select program_allocated from registration_student_list where id=sid;$$;


ALTER FUNCTION public."getProgram"(sid bigint) OWNER TO developer;

--
-- TOC entry 176 (class 1259 OID 16424)
-- Name: test_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE test_question (
    id integer NOT NULL,
    marks integer,
    question text,
    type text,
    options text[],
    answer text[]
);


ALTER TABLE test_question OWNER TO postgres;

--
-- TOC entry 357 (class 1255 OID 42347)
-- Name: getQuestions(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getQuestions"(testpaper_id integer) RETURNS SETOF test_question
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY(select * from public."test_question" where id in
(select unnest (questions)
from public."test_testPaper" where id=testPaper_id )order by random() );
END
$$;


ALTER FUNCTION public."getQuestions"(testpaper_id integer) OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 42662)
-- Name: registration_fee_breakup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE registration_fee_breakup (
    category text NOT NULL,
    total_amt integer,
    round integer NOT NULL,
    year integer NOT NULL,
    breakup json[]
);


ALTER TABLE registration_fee_breakup OWNER TO postgres;

--
-- TOC entry 384 (class 1255 OID 42678)
-- Name: getRegistrationFeeBreakup(text, integer, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) RETURNS SETOF registration_fee_breakup
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY( Select category,total_amt,round,year,breakup from registration_fee_breakup where category=cat and round=rnd and year=reg_year);
  END
  $$;


ALTER FUNCTION public."getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) OWNER TO developer;

--
-- TOC entry 378 (class 1255 OID 34173)
-- Name: getRegistrationStatus(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getRegistrationStatus"(reg_id bigint) RETURNS integer
    LANGUAGE sql
    AS $$select verification_status from
registration_student_list
where id=reg_id;$$;


ALTER FUNCTION public."getRegistrationStatus"(reg_id bigint) OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16400)
-- Name: registration_student_list; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE registration_student_list (
    name text NOT NULL,
    first_name text,
    middle_name text,
    last_name text,
    category text,
    state text,
    phone_number text,
    email text,
    date_of_birth date,
    program_allocated text,
    status text,
    physically_disabled boolean,
    gender text,
    nationality text,
    guardian_name text,
    guardian_contact text,
    guardian_email text,
    guardian_address text,
    father_name text,
    mother_name text,
    father_contact text,
    mother_contact text,
    permanent_address text,
    local_address text,
    hosteller boolean,
    hostel_address json,
    entry_time timestamp without time zone,
    id bigint NOT NULL,
    payment bigint[],
    verified boolean DEFAULT false,
    semester integer DEFAULT 1,
    verification_status integer DEFAULT 0
);


ALTER TABLE registration_student_list OWNER TO developer;

--
-- TOC entry 267 (class 1255 OID 42325)
-- Name: getRegistrationStudentData(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getRegistrationStudentData"(reg_id bigint) RETURNS SETOF registration_student_list
    LANGUAGE plpgsql
    AS $$
BEGIN 
RETURN QUERY(select * from registration_student_list 
where id=reg_id);END
$$;


ALTER FUNCTION public."getRegistrationStudentData"(reg_id bigint) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 42324)
-- Name: getRequiredFields(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getRequiredFields"(formname text) RETURNS TABLE(fields text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select forms.fields from public."forms"where form_name=formname);
END
$$;


ALTER FUNCTION public."getRequiredFields"(formname text) OWNER TO developer;

--
-- TOC entry 274 (class 1255 OID 42520)
-- Name: getSingleComment(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSingleComment"(c_id bigint) RETURNS SETOF comments
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from comments where comment_id = c_id);
END
$$;


ALTER FUNCTION public."getSingleComment"(c_id bigint) OWNER TO developer;

--
-- TOC entry 256 (class 1255 OID 42492)
-- Name: getSinglePost(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSinglePost"(p_id bigint) RETURNS SETOF posts
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from posts where post_id = p_id);
END
$$;


ALTER FUNCTION public."getSinglePost"(p_id bigint) OWNER TO developer;

--
-- TOC entry 259 (class 1255 OID 42491)
-- Name: getSingleThread(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSingleThread"(t_id bigint) RETURNS SETOF threads
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(select * from threads where thread_id = t_id);
END
$$;


ALTER FUNCTION public."getSingleThread"(t_id bigint) OWNER TO developer;

--
-- TOC entry 258 (class 1255 OID 42313)
-- Name: getSolutionSheet(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getSolutionSheet"(answer_sheet_id integer) RETURNS TABLE(id integer, marks integer, question text, type text, options text[], correct_answer text[], given_answer text[])
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select "test_question".id as question_id,"test_question".marks,"test_question".question,"test_question".type,"test_question".options,"test_question".answer as correct_answer,"test_answer".answer as given_answer,"test_answer".id as answer_id   from public."test_question" join public."test_answer" on  "test_question".id="test_answer".question_id where "test_question".id in
(select unnest ("test_testPaper".questions)
from public."test_testPaper" where id=(select test_paper_id from public."test_answerSheet" where id=answer_sheet_id) ) and "test_answer".id in
(select unnest ("test_answerSheet".answers)
from public."test_answerSheet" where id=answer_sheet_id ));
END
$$;


ALTER FUNCTION public."getSolutionSheet"(answer_sheet_id integer) OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 34133)
-- Name: students_list; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE students_list (
    student_id text NOT NULL,
    student_name text,
    branch_name text
);


ALTER TABLE students_list OWNER TO developer;

--
-- TOC entry 249 (class 1255 OID 42306)
-- Name: getStudent(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudent"() RETURNS SETOF students_list
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY (select * from public."students_list");
END
$$;


ALTER FUNCTION public."getStudent"() OWNER TO developer;

--
-- TOC entry 364 (class 1255 OID 42377)
-- Name: getStudentAttendanceList(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentAttendanceList"(course_code text) RETURNS TABLE(student_id text, student_name text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
 RETURN QUERY(SELECT student_alloted_sub.student_id,students_list.student_name from student_alloted_sub natural join students_list where student_alloted_sub.alloted_subject_code=course_code);
END
$$;


ALTER FUNCTION public."getStudentAttendanceList"(course_code text) OWNER TO developer;

--
-- TOC entry 352 (class 1255 OID 42363)
-- Name: getStudentAttendanceList(text, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentAttendanceList"(course text, branch text) RETURNS TABLE(student_id text, student_name text)
    LANGUAGE plpgsql
    AS $$
  BEGIN
RETURN QUERY(SELECT student_alloted_sub.student_id, students_list.student_name from student_alloted_sub natural join students_list where student_alloted_sub.alloted_subject_code=course and  students_list.branch_name=branch);

  END
  $$;


ALTER FUNCTION public."getStudentAttendanceList"(course text, branch text) OWNER TO developer;

--
-- TOC entry 403 (class 1255 OID 42293)
-- Name: getStudentList(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getStudentList"() RETURNS TABLE(name text, student_id text, semester integer, course text, branch text, batch text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(SELECT users.name,student.student_id,student.semester, student.course, student.branch, student.batch FROM student, users where users.id=student.erp_id);
END
$$;


ALTER FUNCTION public."getStudentList"() OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 42842)
-- Name: getStudentListById(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentListById"(std_id text) RETURNS TABLE(name text, first_name text, middle_name text, last_name text, category text, state text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text, guardian_name text, guardian_contact text, guardian_email text, guardian_address text, father_name text, mother_name text, father_contact text, mother_contact text, permanent_address text, local_address text, hosteller boolean, hostel_address json, entry_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY (
SELECT
  registration_student_list.name, 
  registration_student_list.first_name, 
  registration_student_list.middle_name, 
  registration_student_list.last_name, 
  registration_student_list.category, 
  registration_student_list.state, 
  registration_student_list.phone_number, 
  registration_student_list.email, 
  registration_student_list.date_of_birth, 
  registration_student_list.program_allocated, 
  registration_student_list.physically_disabled, 
  registration_student_list.gender, 
  registration_student_list.nationality, 
  registration_student_list.guardian_name, 
  registration_student_list.guardian_contact, 
  registration_student_list.guardian_email, 
  registration_student_list.guardian_address, 
  registration_student_list.father_name, 
  registration_student_list.mother_name, 
  registration_student_list.father_contact, 
  registration_student_list.mother_contact, 
  registration_student_list.permanent_address, 
  registration_student_list.local_address, 
  registration_student_list.hosteller, 
  registration_student_list.hostel_address, 
  registration_student_list.entry_time 
 
 FROM 
  public.registration_student_list
WHERE registration_student_list.id=(select reg_id  from student where student_id=std_id  ) );
END
$$;


ALTER FUNCTION public."getStudentListById"(std_id text) OWNER TO developer;

--
-- TOC entry 399 (class 1255 OID 42289)
-- Name: getStudentProfile(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentProfile"(param bigint) RETURNS TABLE(erp_id bigint, student_id text, semester integer, course text, branch text, batch text, registration_date timestamp without time zone, reg_id bigint, name text, first_name text, middle_name text, last_name text, category text, state text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text, guardian_name text, guardian_contact text, guardian_email text, guardian_address text, father_name text, mother_name text, father_contact text, mother_contact text, permanent_address text, local_address text, hosteller boolean, hostel_address json, entry_time timestamp without time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY (
SELECT 
  student.erp_id, 
  student.student_id, 
  student.semester, 
  student.course, 
  student.branch, 
  student.batch, 
  student.registration_date, 
  student.reg_id, 
  registration_student_list.name, 
  registration_student_list.first_name, 
  registration_student_list.middle_name, 
  registration_student_list.last_name, 
  registration_student_list.category, 
  registration_student_list.state, 
  registration_student_list.phone_number, 
  registration_student_list.email, 
  registration_student_list.date_of_birth, 
  registration_student_list.program_allocated, 
  registration_student_list.physically_disabled, 
  registration_student_list.gender, 
  registration_student_list.nationality, 
  registration_student_list.guardian_name, 
  registration_student_list.guardian_contact, 
  registration_student_list.guardian_email, 
  registration_student_list.guardian_address, 
  registration_student_list.father_name, 
  registration_student_list.mother_name, 
  registration_student_list.father_contact, 
  registration_student_list.mother_contact, 
  registration_student_list.permanent_address, 
  registration_student_list.local_address, 
  registration_student_list.hosteller, 
  registration_student_list.hostel_address, 
  registration_student_list.entry_time 
 FROM 
  public.student join 
  public.registration_student_list
ON
  student.reg_id = registration_student_list.id 
WHERE
student.erp_id=param ) ;
END
$$;


ALTER FUNCTION public."getStudentProfile"(param bigint) OWNER TO developer;

--
-- TOC entry 381 (class 1255 OID 34266)
-- Name: getStudentRegistrationCount(text, integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentRegistrationCount"(program text, sem integer) RETURNS bigint
    LANGUAGE sql
    AS $$SELECT count(*)  FROM student where semester=sem and branch=program;$$;


ALTER FUNCTION public."getStudentRegistrationCount"(program text, sem integer) OWNER TO developer;

--
-- TOC entry 190 (class 1259 OID 16836)
-- Name: update_registration_student_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE update_registration_student_list (
    first_name text,
    middle_name text,
    last_name text,
    phone_number text,
    email text,
    date_of_birth date,
    guardian_name text,
    guardian_contact text,
    guardian_email text,
    guardian_address text,
    father_name text,
    mother_name text,
    father_contact text,
    mother_contact text,
    permanent_address text,
    local_address text,
    hosteller boolean,
    hostel_address json,
    entry_time timestamp without time zone DEFAULT now(),
    registration_id bigint NOT NULL
);


ALTER TABLE update_registration_student_list OWNER TO postgres;

--
-- TOC entry 317 (class 1255 OID 42288)
-- Name: getStudentRegistrationDataUpdate(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) RETURNS SETOF update_registration_student_list
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select * from update_registration_student_list 
where registration_id=reg_id);
END
$$;


ALTER FUNCTION public."getStudentRegistrationDataUpdate"(reg_id bigint) OWNER TO postgres;

--
-- TOC entry 325 (class 1255 OID 42204)
-- Name: getStudentRegistrationList(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getStudentRegistrationList"() RETURNS SETOF registration_student_list
    LANGUAGE plpgsql
    AS $$ 
BEGIN
RETURN QUERY(select * from registration_student_list);
END;
$$;


ALTER FUNCTION public."getStudentRegistrationList"() OWNER TO developer;

--
-- TOC entry 395 (class 1255 OID 42470)
-- Name: getStudentsList(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getStudentsList"() RETURNS TABLE(student_id text, name text, semester integer, batch text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY (
SELECT 
  student.student_id, 
  registration_student_list.name,
  student.semester, 
  student.batch
 FROM 
  public.student join 
  public.registration_student_list
ON
  student.reg_id = registration_student_list.id 
 ) ;
END
$$;


ALTER FUNCTION public."getStudentsList"() OWNER TO postgres;

--
-- TOC entry 310 (class 1255 OID 42287)
-- Name: getSubdirectory(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSubdirectory"(subdir text) RETURNS SETOF files
    LANGUAGE plpgsql
    AS $$
  BEGIN
 RETURN QUERY(select * from files where subdirectory=subdir);
 END
 $$;


ALTER FUNCTION public."getSubdirectory"(subdir text) OWNER TO developer;

--
-- TOC entry 255 (class 1255 OID 42307)
-- Name: getSubjectAllocation(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSubjectAllocation"(faculty text) RETURNS TABLE(semester integer, faculty_id text, branch_name text, alloted_course_code text, date timestamp without time zone, batch_id text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(SELECT DISTINCT on (semester)  faculty_alloted_sub.semester, faculty_alloted_sub.faculty_id, faculty_alloted_sub.branch_name, faculty_alloted_sub.alloted_course_code,  faculty_alloted_sub.date , faculty_alloted_sub.batch_id
FROM faculty_alloted_sub where faculty_alloted_sub.faculty_id=faculty order by semester);
END
$$;


ALTER FUNCTION public."getSubjectAllocation"(faculty text) OWNER TO developer;

--
-- TOC entry 192 (class 1259 OID 16943)
-- Name: subject_list; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE subject_list (
    course_code text NOT NULL,
    course_name text,
    credit integer
);


ALTER TABLE subject_list OWNER TO developer;

--
-- TOC entry 363 (class 1255 OID 42640)
-- Name: getSubjectList(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSubjectList"() RETURNS subject_list
    LANGUAGE sql
    AS $$SELECT course_code, course_name, credit FROM subject_list;$$;


ALTER FUNCTION public."getSubjectList"() OWNER TO developer;

--
-- TOC entry 392 (class 1255 OID 42281)
-- Name: getSubjects(integer); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getSubjects"(sem integer) RETURNS SETOF subject_list
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(SELECT *
  FROM public."subject_list" where semester=sem);
  END
  $$;


ALTER FUNCTION public."getSubjects"(sem integer) OWNER TO developer;

--
-- TOC entry 342 (class 1255 OID 42755)
-- Name: getTags(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getTags"(tag_name text) RETURNS TABLE(users text, attributes text)
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY(SELECT "tags".users, "tags".attributes
  FROM public."tags"  where tag_name=tagname);
  END
$$;


ALTER FUNCTION public."getTags"(tag_name text) OWNER TO developer;

--
-- TOC entry 230 (class 1259 OID 42599)
-- Name: templates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE templates (
    title text,
    author bigint,
    "timestamp" timestamp without time zone DEFAULT now(),
    tags text[],
    id bigint NOT NULL,
    file bigint
);


ALTER TABLE templates OWNER TO postgres;

--
-- TOC entry 351 (class 1255 OID 42632)
-- Name: getTemplates(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getTemplates"(id integer) RETURNS SETOF templates
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN  QUERY(select title,author,tags,file from public."templates"  where templates.id=id 
);
END;
$$;


ALTER FUNCTION public."getTemplates"(id integer) OWNER TO postgres;

--
-- TOC entry 370 (class 1255 OID 42256)
-- Name: getTestPaper(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getTestPaper"() RETURNS TABLE(id integer, questions integer[], subject text, creation_date timestamp without time zone, status text, duration interval, author text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(select  "test_testPaper".id, "test_testPaper".questions, "test_testPaper".subject,
 "test_testPaper".creation_date, "test_testPaper".status, "test_testPaper".duration, 
 public."getUserName"("test_testPaper".author)as author

  from public."test_testPaper" order by id);
END
$$;


ALTER FUNCTION public."getTestPaper"() OWNER TO postgres;

--
-- TOC entry 361 (class 1255 OID 42639)
-- Name: getThreadName(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getThreadName"(t_id bigint) RETURNS text
    LANGUAGE sql
    AS $$select thread_name from threads where thread_id=t_id;$$;


ALTER FUNCTION public."getThreadName"(t_id bigint) OWNER TO developer;

--
-- TOC entry 272 (class 1255 OID 26286)
-- Name: getTotal(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getTotal"(answer_sheet_id integer) RETURNS bigint
    LANGUAGE sql
    AS $$
select sum(marks)  from public."test_answer" where id in
(select unnest (answers)
from public."test_answerSheet" where id=answer_sheet_id)  ;

$$;


ALTER FUNCTION public."getTotal"(answer_sheet_id integer) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 42716)
-- Name: getUnreadMessages(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUnreadMessages"(conversation_id bigint) RETURNS TABLE(message_timestamp timestamp without time zone, text text, id bigint, author bigint, username text)
    LANGUAGE plpgsql
    AS $$
begin
  RETURN QUERY(select messages.timestamp,messages.text,messages.id,messages.author,"getUserName"(messages.author) as username from messages where messages.id in 
 (SELECT * FROM (select unnest ( unread) as id from conversations where conversations.id=conversation_id order by id  desc ) as t1  ORDER BY id ASC));
end;
$$;


ALTER FUNCTION public."getUnreadMessages"(conversation_id bigint) OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16753)
-- Name: notifications; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE notifications (
    notif_id bigint NOT NULL,
    notif_type text,
    message text,
    link text,
    notif_timestamp timestamp without time zone,
    expiry timestamp without time zone,
    notif_author text
);


ALTER TABLE notifications OWNER TO developer;

--
-- TOC entry 387 (class 1255 OID 42219)
-- Name: getUnreadNotifications(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUnreadNotifications"(userid bigint) RETURNS SETOF notifications
    LANGUAGE plpgsql
    AS $$ 
BEGIN
RETURN QUERY(select * from notifications where notif_id=ANY(select unnest(unread_notifications) from users where id=userid) order by notif_timestamp DESC);
END;
$$;


ALTER FUNCTION public."getUnreadNotifications"(userid bigint) OWNER TO postgres;

--
-- TOC entry 367 (class 1255 OID 42383)
-- Name: getUserAnswerSheets(integer, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getUserAnswerSheets"(test_id integer, erp_id bigint) RETURNS TABLE(status text, id integer, submission_time timestamp without time zone, total_marks integer, author text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY(SELECT "test_answerSheet".status, "test_answerSheet".id,  "test_answerSheet".submission_time,"test_answerSheet".total_marks,public."getUserName" ("test_answerSheet".author) as author
  FROM "test_answerSheet"  where "test_answerSheet".test_paper_id=test_id and "test_answerSheet".author=erp_id);
END
$$;


ALTER FUNCTION public."getUserAnswerSheets"(test_id integer, erp_id bigint) OWNER TO developer;

--
-- TOC entry 346 (class 1255 OID 42638)
-- Name: getUserAutoSuggest(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getUserAutoSuggest"(suggest_word text) RETURNS TABLE(id bigint, user_type text, username text, name text, email text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY (select users.id,users.user_type,users.username,users.name,users.email
from users
where users.username like lower(suggest_word) || '%' or users.name like lower(suggest_word) || '%' or users.email like lower(suggest_word) || '%');
END;
$$;


ALTER FUNCTION public."getUserAutoSuggest"(suggest_word text) OWNER TO developer;

--
-- TOC entry 320 (class 1255 OID 42336)
-- Name: getUserEmail(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUserEmail"(user_name text) RETURNS text
    LANGUAGE sql
    AS $$SELECT  email
  FROM users where username=user_name ;
$$;


ALTER FUNCTION public."getUserEmail"(user_name text) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 16531)
-- Name: getUserId(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUserId"(user_name text) RETURNS bigint
    LANGUAGE sql
    AS $$SELECT  id
  FROM users where username=user_name ;
$$;


ALTER FUNCTION public."getUserId"(user_name text) OWNER TO postgres;

--
-- TOC entry 344 (class 1255 OID 33847)
-- Name: getUserName(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUserName"(user_id bigint) RETURNS text
    LANGUAGE sql
    AS $$Select name from users where id=user_id;$$;


ALTER FUNCTION public."getUserName"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 379 (class 1255 OID 34174)
-- Name: getUserNameGeneration(); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getUserNameGeneration"() RETURNS text
    LANGUAGE sql
    AS $$select  username
from user_name_generation
where used=false ;$$;


ALTER FUNCTION public."getUserNameGeneration"() OWNER TO developer;

--
-- TOC entry 343 (class 1255 OID 33835)
-- Name: getUserUserName(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUserUserName"(user_id bigint) RETURNS text
    LANGUAGE sql
    AS $$Select username from users where id=user_id;$$;


ALTER FUNCTION public."getUserUserName"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 398 (class 1255 OID 42203)
-- Name: getUserUsernameById(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUserUsernameById"(user_id bigint) RETURNS text
    LANGUAGE sql
    AS $$Select username from users where id=user_id;$$;


ALTER FUNCTION public."getUserUsernameById"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 388 (class 1255 OID 42271)
-- Name: getUsernameGenerationData(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getUsernameGenerationData"(rid bigint) RETURNS TABLE(first_name text, middle_name text, last_name text, program_allocated text, reg_year bigint, birth_year bigint)
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY( SELECT registration_student_list.first_name , registration_student_list.middle_name,registration_student_list.last_name,registration_student_list.program_allocated,cast(extract (year from registration_student_list.entry_time)as bigint) as reg_year ,cast(extract (year from registration_student_list.date_of_birth) as bigint) as birth_year 
  FROM registration_student_list
   where id=rid );

  END
  


$$;


ALTER FUNCTION public."getUsernameGenerationData"(rid bigint) OWNER TO developer;

--
-- TOC entry 264 (class 1255 OID 42713)
-- Name: getUsernameStatus(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "getUsernameStatus"(uname text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
declare
count bigint;
declare
count1 bigint;
declare 
count2 bigint;
BEGIN
select count(username)from user_name_generation where username=uname into count1;
select count(username) from users where username=uname into count2;
count=count1+count2;
if count=0
then
return false;
else
return true;
end if;
END

$$;


ALTER FUNCTION public."getUsernameStatus"(uname text) OWNER TO developer;

--
-- TOC entry 314 (class 1255 OID 33757)
-- Name: getUsers(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUsers"(type text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  users_id bigint[];
  user_id bigint;
 
BEGIN
  SELECT array_agg(id)  FROM users where user_type=type into users_id;
 perform "newConversation"(users_id);
   
  

END

$$;


ALTER FUNCTION public."getUsers"(type text) OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 16476)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    username text,
    id bigint NOT NULL,
    notifications bigint[],
    unread_notifications bigint[],
    name text,
    user_type text,
    conversations bigint[],
    unread_conversations bigint[],
    last_seen timestamp without time zone,
    email text,
    vercode text,
    "timestamp" timestamp without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 366 (class 1255 OID 42647)
-- Name: getUsersByType(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUsersByType"(input_type text) RETURNS SETOF users
    LANGUAGE plpgsql
    AS $$BEGIN
RETURN QUERY(Select * from users where user_type=input_type);
END;$$;


ALTER FUNCTION public."getUsersByType"(input_type text) OWNER TO postgres;

--
-- TOC entry 402 (class 1255 OID 42472)
-- Name: getUsersList(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "getUsersList"() RETURNS TABLE(username text, user_type text, last_seen timestamp without time zone, email text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY( SELECT  users.username ,users.user_type,users.last_seen,users.email FROM users  );
  END
  $$;


ALTER FUNCTION public."getUsersList"() OWNER TO postgres;

--
-- TOC entry 353 (class 1255 OID 42777)
-- Name: insertTemplates(text, text, bigint, text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "insertTemplates"(title text, f_name text, author bigint, tags text[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare 
file_id bigint;
BEGIN
select files.id from files where files.file_name=f_name into file_id;
insert into templates(title,file,author,tags)
values(title,file_id,author,tags);
END;
$$;


ALTER FUNCTION public."insertTemplates"(title text, f_name text, author bigint, tags text[]) OWNER TO postgres;

--
-- TOC entry 345 (class 1255 OID 33794)
-- Name: markAsReadMessages(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "markAsReadMessages"(conversation_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
   read bigint[];
BEGIN
 
select unread from conversations where conversations.id=conversation_id into read;
UPDATE conversations
   SET messages=array_cat(messages,read) , unread=NULL
 WHERE id=conversation_id; 

END

$$;


ALTER FUNCTION public."markAsReadMessages"(conversation_id bigint) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 25554)
-- Name: newAnswerSheet(integer, bigint, timestamp without time zone, text, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO public."test_answerSheet"(
             author, submission_time, status, answers, test_paper_id)
    VALUES (author, submission_time, status, answers, test_paper_id) returning id;
$$;


ALTER FUNCTION public."newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) OWNER TO postgres;

--
-- TOC entry 333 (class 1255 OID 33759)
-- Name: newConversation(bigint[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "newConversation"(members bigint[], name_of_chat_group text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
    
    member text;
    member_id bigint;
    id_list bigint[];
    convo_id bigint;
    convo_id_list bigint[];
    convo bigint;
    chat bigint;
    con bigint[];
BEGIN

   FOREACH member_id IN ARRAY members
    LOOP
id_list = array_append(id_list,member_id);    
END LOOP;
 
 FOREACH member_id IN ARRAY members
    LOOP
      
INSERT INTO conversations(
            messages, unread, members_id,owner,chat_name)
    VALUES ('{}', '{}', id_list,member_id,name_of_chat_group) returning id INTO convo_id;
    convo_id_list=array_append(convo_id_list,convo_id);
    UPDATE users
   SET  conversations=array_append(conversations,convo_id)
 WHERE id=member_id;    
END LOOP;     
 INSERT INTO chat(
           conversation)
    VALUES (convo_id_list) returning id into chat ;
    
con= "getConversationId"(chat);
FOREACH convo IN ARRAY con
    LOOP
       UPDATE conversations
   SET  chat_id=chat
 WHERE id=convo;
END LOOP;
  

END

$$;


ALTER FUNCTION public."newConversation"(members bigint[], name_of_chat_group text) OWNER TO postgres;

--
-- TOC entry 369 (class 1255 OID 25453)
-- Name: newMessage(text, bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
   conversation_array bigint[];
     message_id bigint;
     chat_index bigint;
     convo_id bigint;
     own bigint;
     nul_convo_id bigint;
     my_convos bigint[];
BEGIN
   INSERT INTO messages(
            author, "timestamp", text)
    VALUES (author, CURRENT_TIMESTAMP,message) returning id into message_id;
   
   chat_index= "getChatId"(conversation_id);
   conversation_array="getConversationId"(chat_index);
   FOREACH convo_id IN ARRAY conversation_array
    LOOP
    if convo_id=conversation_id then UPDATE conversations
   SET messages=array_append(messages,message_id) 
 WHERE id=convo_id ; 
    else
       UPDATE conversations
   SET unread=array_append(unread,message_id)
 WHERE id=convo_id returning owner into own;
 select unread_conversations from users where id=own into my_convos;
IF NOT(my_convos::bigint[] @> ARRAY[convo_id]) THEN  update users  SET unread_conversations = array_append(unread_conversations,convo_id) where id=own; 
END IF;
 end if;  
END LOOP;
END

$$;


ALTER FUNCTION public."newMessage"(message text, author bigint, conversation_id bigint) OWNER TO postgres;

--
-- TOC entry 298 (class 1255 OID 26292)
-- Name: newTestPaper(integer[], text, bigint, date, text, interval); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO public."test_testPaper"(
      questions, subject, author, creation_date, status,duration)
  VALUES (questions, subject, author, creation_date, status,duration) returning id as id;
$$;


ALTER FUNCTION public."newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 16811)
-- Name: readNotification(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "readNotification"(userid bigint, notif_id bigint) RETURNS void
    LANGUAGE sql
    AS $$update users
set unread_notifications=array_remove(unread_notifications,notif_id),
notifications=array_append(notifications,notif_id)
where id=userid
$$;


ALTER FUNCTION public."readNotification"(userid bigint, notif_id bigint) OWNER TO postgres;

--
-- TOC entry 331 (class 1255 OID 17289)
-- Name: report(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION report(csab_id bigint) RETURNS bigint
    LANGUAGE sql
    AS $$update csab_student_list
set reported=true
where id=csab_id;

INSERT INTO registration_student_list(
            name, first_name, middle_name, last_name, category, state, phone_number, 
            email, date_of_birth, program_allocated, status, physically_disabled, 
            gender, nationality,permanent_address)
    VALUES (
    (select name from csab_student_list where id=csab_id),
    (select first_name from csab_student_list where id=csab_id),
    (select middle_name from csab_student_list where id=csab_id),
    (select last_name from csab_student_list where id=csab_id),
    (select category from csab_student_list where id=csab_id),
    (select state from csab_student_list where id=csab_id),
    (select phone_number from csab_student_list where id=csab_id),
    (select email from csab_student_list where id=csab_id),
    (select date_of_birth from csab_student_list where id=csab_id),
    (select program_allocated from csab_student_list where id=csab_id),
    (select status from csab_student_list where id=csab_id),
    (select physically_disabled from csab_student_list where id=csab_id),
    (select gender from csab_student_list where id=csab_id),
    (select nationality from csab_student_list where id=csab_id),
    (select address from csab_student_list where id=csab_id)
    ) returning id;
$$;


ALTER FUNCTION public.report(csab_id bigint) OWNER TO postgres;

--
-- TOC entry 308 (class 1255 OID 42889)
-- Name: report_student(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION report_student(csab bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
declare
   reg_id bigint;
BEGIN
 
update csab_student_list
set reported=true
where csab_student_list.csab_id=csab;

INSERT INTO registration_student_list(
            name, first_name, middle_name, last_name, category, state, phone_number, 
            email, date_of_birth, program_allocated, status, physically_disabled, 
            gender, nationality,permanent_address)
    VALUES (
    (select name from csab_student_list where csab_student_list.csab_id=csab),
    (select first_name from csab_student_list where csab_student_list.csab_id=csab),
    (select middle_name from csab_student_list where csab_student_list.csab_id=csab),
    (select last_name from csab_student_list where csab_student_list.csab_id=csab),
    (select category from csab_student_list where csab_student_list.csab_id=csab),
    (select state from csab_student_list where csab_student_list.csab_id=csab),
    (select phone_number from csab_student_list where csab_student_list.csab_id=csab),
    (select email from csab_student_list where csab_student_list.csab_id=csab),
    (select date_of_birth from csab_student_list where csab_student_list.csab_id=csab),
    (select program_allocated from csab_student_list where csab_student_list.csab_id=csab),
    (select status from csab_student_list where csab_student_list.csab_id=csab),
    (select physically_disabled from csab_student_list where csab_student_list.csab_id=csab),
    (select gender from csab_student_list where csab_student_list.csab_id=csab),
    (select nationality from csab_student_list where csab_student_list.csab_id=csab),
    (select address from csab_student_list where csab_student_list.csab_id=csab)
    ) returning id into reg_id;
    UPDATE csab_student_list
   SET registration_id=reg_id
 WHERE "csab_student_list".csab_id=csab;
 return reg_id;

END

$$;


ALTER FUNCTION public.report_student(csab bigint) OWNER TO postgres;

--
-- TOC entry 327 (class 1255 OID 16776)
-- Name: sendNotification(bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) RETURNS void
    LANGUAGE sql
    AS $$
update users
set unread_notifications=array_append(unread_notifications,notif_id)
where id=ANY (userid);
$$;


ALTER FUNCTION public."sendNotification"(notif_id bigint, userid bigint[]) OWNER TO postgres;

--
-- TOC entry 297 (class 1255 OID 26260)
-- Name: setAnswerCorrection(boolean, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) RETURNS void
    LANGUAGE sql
    AS $$update public."test_answer" set correct=correction,marks=marks_obtained where id=answer_id$$;


ALTER FUNCTION public."setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) OWNER TO postgres;

--
-- TOC entry 377 (class 1255 OID 34230)
-- Name: setTimetable(integer, integer, integer, integer, text, text, text, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) RETURNS void
    LANGUAGE sql
    AS $$insert into tt_save_timetable
values(dept,sem,batch,time_slot,day,subject_name,
faculty_name,venue_name);$$;


ALTER FUNCTION public."setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) OWNER TO developer;

--
-- TOC entry 299 (class 1255 OID 33627)
-- Name: studentID(bigint, integer, text, text, text, text, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) RETURNS void
    LANGUAGE sql
    AS $$insert into student
values(erp_id,student_id,semester,course,branch,batch,registration_date)$$;


ALTER FUNCTION public."studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) OWNER TO developer;

--
-- TOC entry 371 (class 1255 OID 34056)
-- Name: updateFeeBreakup(integer, integer, text, json); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) RETURNS void
    LANGUAGE sql
    AS $$
update fee_breakup
set breakup=brkup
where year=bat and semester=sem and category=cat;$$;


ALTER FUNCTION public."updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) OWNER TO developer;

--
-- TOC entry 279 (class 1255 OID 17570)
-- Name: updateLastSeen(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "updateLastSeen"(user_id bigint) RETURNS void
    LANGUAGE sql
    AS $$update users set last_seen=now() where id=user_id;$$;


ALTER FUNCTION public."updateLastSeen"(user_id bigint) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 42334)
-- Name: updateVercode(bigint, text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "updateVercode"(user_id bigint, ver_code text) RETURNS void
    LANGUAGE sql
    AS $$
update users set timestamp=now(),vercode=ver_code where
 id=user_id;
$$;


ALTER FUNCTION public."updateVercode"(user_id bigint, ver_code text) OWNER TO developer;

--
-- TOC entry 341 (class 1255 OID 33813)
-- Name: updateVerificationStatus(integer, bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "updateVerificationStatus"(status_code integer, rid bigint) RETURNS void
    LANGUAGE sql
    AS $$UPDATE registration_student_list SET verification_status=status_code WHERE id=rid;
$$;


ALTER FUNCTION public."updateVerificationStatus"(status_code integer, rid bigint) OWNER TO developer;

--
-- TOC entry 372 (class 1255 OID 33966)
-- Name: updateVerified(bigint); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "updateVerified"(reg_id bigint) RETURNS void
    LANGUAGE sql
    AS $$
update registration_student_list set verified=true, verification_status=2 where
 id=reg_id;
$$;


ALTER FUNCTION public."updateVerified"(reg_id bigint) OWNER TO developer;

--
-- TOC entry 376 (class 1255 OID 34189)
-- Name: useUserNameGeneration(text); Type: FUNCTION; Schema: public; Owner: developer
--

CREATE FUNCTION "useUserNameGeneration"(uname text) RETURNS void
    LANGUAGE sql
    AS $$update user_name_generation
set used=true
where username=uname;$$;


ALTER FUNCTION public."useUserNameGeneration"(uname text) OWNER TO developer;

--
-- TOC entry 389 (class 1255 OID 42467)
-- Name: verifyForgotPassword(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "verifyForgotPassword"(user_name text, ver_code text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$ 
declare
erp_id bigint;
Begin
SELECT id
  FROM users where vercode=ver_code and username=user_name into erp_id;
  return erp_id;
  end
  $$;


ALTER FUNCTION public."verifyForgotPassword"(user_name text, ver_code text) OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17123)
-- Name: attendance_register; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE attendance_register (
    student_id text NOT NULL,
    attendance_status text NOT NULL,
    class_id text NOT NULL
);


ALTER TABLE attendance_register OWNER TO developer;

--
-- TOC entry 214 (class 1259 OID 33995)
-- Name: cat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cat (
    category text
);


ALTER TABLE cat OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16619)
-- Name: chat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE chat (
    conversation integer[],
    id bigint NOT NULL
);


ALTER TABLE chat OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16631)
-- Name: chat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE chat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chat_id_seq OWNER TO postgres;

--
-- TOC entry 2669 (class 0 OID 0)
-- Dependencies: 186
-- Name: chat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE chat_id_seq OWNED BY chat.id;


--
-- TOC entry 191 (class 1259 OID 16938)
-- Name: class_attendance_info; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE class_attendance_info (
    semester integer,
    subject_code character(10),
    class_type character(1),
    batch character(1) NOT NULL,
    class_start_time timestamp without time zone,
    class_ending_time timestamp without time zone,
    faculty_id text,
    class_id text NOT NULL
);


ALTER TABLE class_attendance_info OWNER TO developer;

--
-- TOC entry 227 (class 1259 OID 42509)
-- Name: comments_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE comments_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comments_comment_id_seq OWNER TO developer;

--
-- TOC entry 2672 (class 0 OID 0)
-- Dependencies: 227
-- Name: comments_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE comments_comment_id_seq OWNED BY comments.comment_id;


--
-- TOC entry 181 (class 1259 OID 16484)
-- Name: conversations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE conversations (
    id bigint NOT NULL,
    chat_id bigint,
    messages bigint[],
    unread bigint[],
    members_id bigint[],
    owner bigint,
    chat_name text,
    last_update timestamp without time zone
);


ALTER TABLE conversations OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16521)
-- Name: conversations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE conversations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE conversations_id_seq OWNER TO postgres;

--
-- TOC entry 2675 (class 0 OID 0)
-- Dependencies: 184
-- Name: conversations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE conversations_id_seq OWNED BY conversations.id;


--
-- TOC entry 232 (class 1259 OID 42620)
-- Name: course_allocation; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE course_allocation (
    course_code text NOT NULL,
    year integer NOT NULL,
    semester integer NOT NULL,
    credits integer
);


ALTER TABLE course_allocation OWNER TO developer;

--
-- TOC entry 207 (class 1259 OID 25454)
-- Name: course_registration; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE course_registration (
    student_id text NOT NULL,
    course_code text NOT NULL,
    registration_date timestamp without time zone NOT NULL,
    grade text
);


ALTER TABLE course_registration OWNER TO developer;

--
-- TOC entry 202 (class 1259 OID 17430)
-- Name: csab_student_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE csab_student_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE csab_student_list_id_seq OWNER TO postgres;

--
-- TOC entry 2679 (class 0 OID 0)
-- Dependencies: 202
-- Name: csab_student_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE csab_student_list_id_seq OWNED BY csab_student_list.csab_id;


--
-- TOC entry 208 (class 1259 OID 26251)
-- Name: department; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE department (
    department_id integer NOT NULL,
    department_name character(25)
);


ALTER TABLE department OWNER TO developer;

--
-- TOC entry 235 (class 1259 OID 42863)
-- Name: documents_document_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documents_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE documents_document_id_seq OWNER TO postgres;

--
-- TOC entry 2682 (class 0 OID 0)
-- Dependencies: 235
-- Name: documents_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documents_document_id_seq OWNED BY documents.document_id;


--
-- TOC entry 213 (class 1259 OID 33870)
-- Name: dt; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE dt (
    now timestamp with time zone
);


ALTER TABLE dt OWNER TO developer;

--
-- TOC entry 206 (class 1259 OID 17529)
-- Name: faculty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faculty (
    erp_id bigint NOT NULL,
    faculty_id text
);


ALTER TABLE faculty OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17116)
-- Name: faculty_alloted_sub; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE faculty_alloted_sub (
    faculty_id text NOT NULL,
    alloted_course_code text NOT NULL,
    semester integer,
    date timestamp without time zone,
    branch_name text NOT NULL,
    batch_id text NOT NULL
);


ALTER TABLE faculty_alloted_sub OWNER TO developer;

--
-- TOC entry 200 (class 1259 OID 17364)
-- Name: fee_payment_ref_no_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE fee_payment_ref_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fee_payment_ref_no_seq OWNER TO developer;

--
-- TOC entry 2686 (class 0 OID 0)
-- Dependencies: 200
-- Name: fee_payment_ref_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE fee_payment_ref_no_seq OWNED BY fee_payment.ref_no;


--
-- TOC entry 211 (class 1259 OID 33722)
-- Name: fee_transaction; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE fee_transaction (
    ref_no bigint[],
    reg_id bigint,
    semester integer,
    category text,
    date date
);


ALTER TABLE fee_transaction OWNER TO developer;

--
-- TOC entry 216 (class 1259 OID 34035)
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE files_id_seq OWNER TO developer;

--
-- TOC entry 2689 (class 0 OID 0)
-- Dependencies: 216
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- TOC entry 194 (class 1259 OID 17016)
-- Name: forms; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE forms (
    form_name text,
    fields text,
    format text
);


ALTER TABLE forms OWNER TO developer;

--
-- TOC entry 225 (class 1259 OID 42473)
-- Name: forum_category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE forum_category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forum_category_category_id_seq OWNER TO developer;

--
-- TOC entry 2692 (class 0 OID 0)
-- Dependencies: 225
-- Name: forum_category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE forum_category_category_id_seq OWNED BY forum_category.category_id;


--
-- TOC entry 209 (class 1259 OID 33634)
-- Name: groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE groups (
    name text,
    users_id bigint[],
    group_id bigint NOT NULL
);


ALTER TABLE groups OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 33654)
-- Name: groups_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groups_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_group_id_seq OWNER TO postgres;

--
-- TOC entry 2695 (class 0 OID 0)
-- Dependencies: 210
-- Name: groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groups_group_id_seq OWNED BY groups.group_id;


--
-- TOC entry 182 (class 1259 OID 16492)
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE messages (
    "timestamp" timestamp without time zone,
    text text,
    id bigint NOT NULL,
    author bigint
);


ALTER TABLE messages OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16668)
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE messages_id_seq OWNER TO postgres;

--
-- TOC entry 2698 (class 0 OID 0)
-- Dependencies: 187
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- TOC entry 188 (class 1259 OID 16751)
-- Name: notifications_notif_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE notifications_notif_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notifications_notif_id_seq OWNER TO developer;

--
-- TOC entry 2700 (class 0 OID 0)
-- Dependencies: 188
-- Name: notifications_notif_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE notifications_notif_id_seq OWNED BY notifications.notif_id;


--
-- TOC entry 205 (class 1259 OID 17524)
-- Name: office; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE office (
    erp_id bigint NOT NULL
);


ALTER TABLE office OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 42413)
-- Name: posts_post_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE posts_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posts_post_id_seq OWNER TO developer;

--
-- TOC entry 2703 (class 0 OID 0)
-- Dependencies: 222
-- Name: posts_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE posts_post_id_seq OWNED BY posts.post_id;


--
-- TOC entry 234 (class 1259 OID 42670)
-- Name: registration_fee_transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE registration_fee_transaction (
    ref_no bigint[],
    reg_id bigint,
    round integer,
    category text,
    date date DEFAULT now()
);


ALTER TABLE registration_fee_transaction OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17278)
-- Name: registration_student_list_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE registration_student_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registration_student_list_id_seq OWNER TO developer;

--
-- TOC entry 2706 (class 0 OID 0)
-- Dependencies: 198
-- Name: registration_student_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE registration_student_list_id_seq OWNED BY registration_student_list.id;


--
-- TOC entry 212 (class 1259 OID 33867)
-- Name: sem; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE sem (
    semester integer
);


ALTER TABLE sem OWNER TO developer;

--
-- TOC entry 204 (class 1259 OID 17509)
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE student (
    erp_id bigint NOT NULL,
    student_id text,
    semester integer,
    course text,
    branch text,
    batch text,
    registration_date timestamp without time zone DEFAULT now(),
    reg_id bigint
);


ALTER TABLE student OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 17112)
-- Name: student_alloted_sub; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE student_alloted_sub (
    student_id text NOT NULL,
    alloted_subject_code character(12) NOT NULL,
    registration_date timestamp without time zone,
    batch_id text
);


ALTER TABLE student_alloted_sub OWNER TO developer;

--
-- TOC entry 228 (class 1259 OID 42584)
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tags (
    tagname text,
    users text,
    attributes text,
    id bigint NOT NULL
);


ALTER TABLE tags OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 42590)
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tags_id_seq OWNER TO postgres;

--
-- TOC entry 2712 (class 0 OID 0)
-- Dependencies: 229
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- TOC entry 231 (class 1259 OID 42607)
-- Name: templates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE templates_id_seq OWNER TO postgres;

--
-- TOC entry 2714 (class 0 OID 0)
-- Dependencies: 231
-- Name: templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE templates_id_seq OWNED BY templates.id;


--
-- TOC entry 173 (class 1259 OID 16414)
-- Name: test_answerSheet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "test_answerSheet" (
    status text,
    answers integer[],
    test_paper_id integer,
    id integer NOT NULL,
    author bigint,
    submission_time timestamp without time zone,
    total_marks integer DEFAULT 0
);


ALTER TABLE "test_answerSheet" OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16420)
-- Name: test_answerSheet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "test_answerSheet_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "test_answerSheet_id_seq" OWNER TO postgres;

--
-- TOC entry 2717 (class 0 OID 0)
-- Dependencies: 174
-- Name: test_answerSheet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "test_answerSheet_id_seq" OWNED BY "test_answerSheet".id;


--
-- TOC entry 175 (class 1259 OID 16422)
-- Name: test_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_answer_id_seq OWNER TO postgres;

--
-- TOC entry 2719 (class 0 OID 0)
-- Dependencies: 175
-- Name: test_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_answer_id_seq OWNED BY test_answer.id;


--
-- TOC entry 177 (class 1259 OID 16430)
-- Name: test_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE test_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_question_id_seq OWNER TO postgres;

--
-- TOC entry 2721 (class 0 OID 0)
-- Dependencies: 177
-- Name: test_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE test_question_id_seq OWNED BY test_question.id;


--
-- TOC entry 178 (class 1259 OID 16432)
-- Name: test_testPaper; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "test_testPaper" (
    id integer NOT NULL,
    questions integer[],
    subject text,
    creation_date timestamp without time zone,
    status text,
    duration interval,
    author bigint,
    total_marks integer DEFAULT 0
);


ALTER TABLE "test_testPaper" OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16438)
-- Name: test_testPaper_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "test_testPaper_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "test_testPaper_id_seq" OWNER TO postgres;

--
-- TOC entry 2724 (class 0 OID 0)
-- Dependencies: 179
-- Name: test_testPaper_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "test_testPaper_id_seq" OWNED BY "test_testPaper".id;


--
-- TOC entry 220 (class 1259 OID 42402)
-- Name: threads_thread_id_seq; Type: SEQUENCE; Schema: public; Owner: developer
--

CREATE SEQUENCE threads_thread_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE threads_thread_id_seq OWNER TO developer;

--
-- TOC entry 2726 (class 0 OID 0)
-- Dependencies: 220
-- Name: threads_thread_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: developer
--

ALTER SEQUENCE threads_thread_id_seq OWNED BY threads.thread_id;


--
-- TOC entry 218 (class 1259 OID 34159)
-- Name: user_name_generation; Type: TABLE; Schema: public; Owner: developer
--

CREATE TABLE user_name_generation (
    username text,
    used boolean DEFAULT false
);


ALTER TABLE user_name_generation OWNER TO developer;

--
-- TOC entry 183 (class 1259 OID 16503)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- TOC entry 2729 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2283 (class 2604 OID 16633)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY chat ALTER COLUMN id SET DEFAULT nextval('chat_id_seq'::regclass);


--
-- TOC entry 2300 (class 2604 OID 42511)
-- Name: comment_id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY comments ALTER COLUMN comment_id SET DEFAULT nextval('comments_comment_id_seq'::regclass);


--
-- TOC entry 2281 (class 2604 OID 16523)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversations ALTER COLUMN id SET DEFAULT nextval('conversations_id_seq'::regclass);


--
-- TOC entry 2288 (class 2604 OID 17435)
-- Name: csab_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY csab_student_list ALTER COLUMN csab_id SET DEFAULT nextval('csab_student_list_id_seq'::regclass);


--
-- TOC entry 2307 (class 2604 OID 42869)
-- Name: document_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documents ALTER COLUMN document_id SET DEFAULT nextval('documents_document_id_seq'::regclass);


--
-- TOC entry 2286 (class 2604 OID 17366)
-- Name: ref_no; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY fee_payment ALTER COLUMN ref_no SET DEFAULT nextval('fee_payment_ref_no_seq'::regclass);


--
-- TOC entry 2293 (class 2604 OID 34037)
-- Name: id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- TOC entry 2301 (class 2604 OID 42478)
-- Name: category_id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY forum_category ALTER COLUMN category_id SET DEFAULT nextval('forum_category_category_id_seq'::regclass);


--
-- TOC entry 2292 (class 2604 OID 33656)
-- Name: group_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groups ALTER COLUMN group_id SET DEFAULT nextval('groups_group_id_seq'::regclass);


--
-- TOC entry 2282 (class 2604 OID 16670)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- TOC entry 2284 (class 2604 OID 16756)
-- Name: notif_id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY notifications ALTER COLUMN notif_id SET DEFAULT nextval('notifications_notif_id_seq'::regclass);


--
-- TOC entry 2297 (class 2604 OID 42418)
-- Name: post_id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY posts ALTER COLUMN post_id SET DEFAULT nextval('posts_post_id_seq'::regclass);


--
-- TOC entry 2269 (class 2604 OID 17280)
-- Name: id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY registration_student_list ALTER COLUMN id SET DEFAULT nextval('registration_student_list_id_seq'::regclass);


--
-- TOC entry 2302 (class 2604 OID 42592)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- TOC entry 2303 (class 2604 OID 42609)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY templates ALTER COLUMN id SET DEFAULT nextval('templates_id_seq'::regclass);


--
-- TOC entry 2273 (class 2604 OID 16441)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer ALTER COLUMN id SET DEFAULT nextval('test_answer_id_seq'::regclass);


--
-- TOC entry 2275 (class 2604 OID 16442)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "test_answerSheet" ALTER COLUMN id SET DEFAULT nextval('"test_answerSheet_id_seq"'::regclass);


--
-- TOC entry 2277 (class 2604 OID 16443)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_question ALTER COLUMN id SET DEFAULT nextval('test_question_id_seq'::regclass);


--
-- TOC entry 2278 (class 2604 OID 16444)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "test_testPaper" ALTER COLUMN id SET DEFAULT nextval('"test_testPaper_id_seq"'::regclass);


--
-- TOC entry 2295 (class 2604 OID 42407)
-- Name: thread_id; Type: DEFAULT; Schema: public; Owner: developer
--

ALTER TABLE ONLY threads ALTER COLUMN thread_id SET DEFAULT nextval('threads_thread_id_seq'::regclass);


--
-- TOC entry 2280 (class 2604 OID 16505)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2313 (class 2606 OID 16446)
-- Name: answer_sheet_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "test_answerSheet"
    ADD CONSTRAINT answer_sheet_id PRIMARY KEY (id);


--
-- TOC entry 2341 (class 2606 OID 25581)
-- Name: attendance_register_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY attendance_register
    ADD CONSTRAINT attendance_register_pk PRIMARY KEY (student_id, attendance_status, class_id);


--
-- TOC entry 2371 (class 2606 OID 42483)
-- Name: category_id; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY forum_category
    ADD CONSTRAINT category_id PRIMARY KEY (category_id);


--
-- TOC entry 2325 (class 2606 OID 16642)
-- Name: chat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY chat
    ADD CONSTRAINT chat_pk PRIMARY KEY (id);


--
-- TOC entry 2331 (class 2606 OID 25589)
-- Name: class_attendance_info_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY class_attendance_info
    ADD CONSTRAINT class_attendance_info_pk PRIMARY KEY (class_id);


--
-- TOC entry 2369 (class 2606 OID 42519)
-- Name: comment_id; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comment_id PRIMARY KEY (comment_id);


--
-- TOC entry 2321 (class 2606 OID 16594)
-- Name: conversations_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conversations
    ADD CONSTRAINT conversations_pk PRIMARY KEY (id);


--
-- TOC entry 2355 (class 2606 OID 25461)
-- Name: course_registration_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY course_registration
    ADD CONSTRAINT course_registration_pk PRIMARY KEY (course_code, student_id, registration_date);


--
-- TOC entry 2345 (class 2606 OID 26308)
-- Name: csab_student_list_jee_main_rollno_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY csab_student_list
    ADD CONSTRAINT csab_student_list_jee_main_rollno_key UNIQUE (jee_main_rollno);


--
-- TOC entry 2347 (class 2606 OID 17442)
-- Name: csab_student_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY csab_student_list
    ADD CONSTRAINT csab_student_list_pkey PRIMARY KEY (csab_id);


--
-- TOC entry 2357 (class 2606 OID 26255)
-- Name: department_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY department
    ADD CONSTRAINT department_pkey PRIMARY KEY (department_id);


--
-- TOC entry 2381 (class 2606 OID 42874)
-- Name: document_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT document_pk PRIMARY KEY (document_id);


--
-- TOC entry 2339 (class 2606 OID 25573)
-- Name: faculty_alloted_sub_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY faculty_alloted_sub
    ADD CONSTRAINT faculty_alloted_sub_pk PRIMARY KEY (faculty_id, alloted_course_code, branch_name, batch_id);


--
-- TOC entry 2353 (class 2606 OID 17533)
-- Name: faculty_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faculty
    ADD CONSTRAINT faculty_id PRIMARY KEY (erp_id);


--
-- TOC entry 2335 (class 2606 OID 34006)
-- Name: fee_break_up_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY fee_breakup
    ADD CONSTRAINT fee_break_up_pk PRIMARY KEY (year, category, semester);


--
-- TOC entry 2343 (class 2606 OID 17540)
-- Name: fee_payment_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY fee_payment
    ADD CONSTRAINT fee_payment_pk PRIMARY KEY (ref_no);


--
-- TOC entry 2359 (class 2606 OID 34045)
-- Name: files_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 16448)
-- Name: id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_answer
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- TOC entry 2323 (class 2606 OID 16678)
-- Name: message_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT message_pk PRIMARY KEY (id);


--
-- TOC entry 2327 (class 2606 OID 16761)
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notif_id);


--
-- TOC entry 2351 (class 2606 OID 17528)
-- Name: office_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY office
    ADD CONSTRAINT office_id PRIMARY KEY (erp_id);


--
-- TOC entry 2377 (class 2606 OID 42634)
-- Name: pk_course_allocation; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY course_allocation
    ADD CONSTRAINT pk_course_allocation PRIMARY KEY (course_code, year, semester);


--
-- TOC entry 2367 (class 2606 OID 42423)
-- Name: post_id; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT post_id PRIMARY KEY (post_id);


--
-- TOC entry 2309 (class 2606 OID 17358)
-- Name: registration_student_list_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY registration_student_list
    ADD CONSTRAINT registration_student_list_pkey PRIMARY KEY (id);


--
-- TOC entry 2379 (class 2606 OID 42669)
-- Name: round_fee_breakup_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY registration_fee_breakup
    ADD CONSTRAINT round_fee_breakup_pk PRIMARY KEY (category, round, year);


--
-- TOC entry 2337 (class 2606 OID 17194)
-- Name: student_alloted_sub_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY student_alloted_sub
    ADD CONSTRAINT student_alloted_sub_pkey PRIMARY KEY (student_id, alloted_subject_code);


--
-- TOC entry 2349 (class 2606 OID 17535)
-- Name: student_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_id PRIMARY KEY (erp_id);


--
-- TOC entry 2361 (class 2606 OID 34140)
-- Name: students_list_pk; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY students_list
    ADD CONSTRAINT students_list_pk PRIMARY KEY (student_id);


--
-- TOC entry 2333 (class 2606 OID 17132)
-- Name: subject_list_pkey; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY subject_list
    ADD CONSTRAINT subject_list_pkey PRIMARY KEY (course_code);


--
-- TOC entry 2373 (class 2606 OID 42764)
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- TOC entry 2375 (class 2606 OID 42762)
-- Name: templates_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_pk PRIMARY KEY (id);


--
-- TOC entry 2315 (class 2606 OID 16452)
-- Name: test_question_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test_question
    ADD CONSTRAINT test_question_id PRIMARY KEY (id);


--
-- TOC entry 2317 (class 2606 OID 16454)
-- Name: test_testpaper_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "test_testPaper"
    ADD CONSTRAINT test_testpaper_id PRIMARY KEY (id);


--
-- TOC entry 2365 (class 2606 OID 42412)
-- Name: thread_id; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY threads
    ADD CONSTRAINT thread_id PRIMARY KEY (thread_id);


--
-- TOC entry 2329 (class 2606 OID 34323)
-- Name: update_registration_student_list_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY update_registration_student_list
    ADD CONSTRAINT update_registration_student_list_pk PRIMARY KEY (registration_id);


--
-- TOC entry 2363 (class 2606 OID 34191)
-- Name: user_name_generation_username_unique; Type: CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY user_name_generation
    ADD CONSTRAINT user_name_generation_username_unique UNIQUE (username);


--
-- TOC entry 2319 (class 2606 OID 17275)
-- Name: users_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_id PRIMARY KEY (id);


--
-- TOC entry 2383 (class 2606 OID 42440)
-- Name: post_id; Type: FK CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT post_id FOREIGN KEY (post_id) REFERENCES posts(post_id);


--
-- TOC entry 2382 (class 2606 OID 42424)
-- Name: thread_id; Type: FK CONSTRAINT; Schema: public; Owner: developer
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT thread_id FOREIGN KEY (thread_id) REFERENCES threads(thread_id);


--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 278
-- Name: addAnswer(text[], integer, integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) FROM postgres;
GRANT ALL ON FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) TO postgres;
GRANT ALL ON FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "addAnswer"(answer text[], answer_sheet_id integer, question_id integer) TO developer;


--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 265
-- Name: addAttendance(text, json[]); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addAttendance"(class text, attendance_json json[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addAttendance"(class text, attendance_json json[]) FROM developer;
GRANT ALL ON FUNCTION "addAttendance"(class text, attendance_json json[]) TO developer;
GRANT ALL ON FUNCTION "addAttendance"(class text, attendance_json json[]) TO PUBLIC;


--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 287
-- Name: addCSABData(json[]); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addCSABData"(data json[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addCSABData"(data json[]) FROM developer;
GRANT ALL ON FUNCTION "addCSABData"(data json[]) TO developer;
GRANT ALL ON FUNCTION "addCSABData"(data json[]) TO PUBLIC;


--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 250
-- Name: addCategory(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addCategory"(cat_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addCategory"(cat_name text) FROM developer;
GRANT ALL ON FUNCTION "addCategory"(cat_name text) TO developer;
GRANT ALL ON FUNCTION "addCategory"(cat_name text) TO PUBLIC;


--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 261
-- Name: addComment(text, bigint, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addComment"(data text, post_id bigint, erp_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addComment"(data text, post_id bigint, erp_id bigint) FROM developer;
GRANT ALL ON FUNCTION "addComment"(data text, post_id bigint, erp_id bigint) TO developer;
GRANT ALL ON FUNCTION "addComment"(data text, post_id bigint, erp_id bigint) TO PUBLIC;


--
-- TOC entry 2504 (class 0 OID 0)
-- Dependencies: 286
-- Name: addDocument(bigint, text, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addDocument"(file_id bigint, document_name text, owner_reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addDocument"(file_id bigint, document_name text, owner_reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "addDocument"(file_id bigint, document_name text, owner_reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "addDocument"(file_id bigint, document_name text, owner_reg_id bigint) TO PUBLIC;


--
-- TOC entry 2505 (class 0 OID 0)
-- Dependencies: 254
-- Name: addFeeBreakup(integer, integer, text, json[], integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) FROM developer;
GRANT ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) TO developer;
GRANT ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json[], total integer) TO PUBLIC;


--
-- TOC entry 2506 (class 0 OID 0)
-- Dependencies: 397
-- Name: addFeeBreakup(integer, integer, text, json, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) FROM developer;
GRANT ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) TO developer;
GRANT ALL ON FUNCTION "addFeeBreakup"(batch integer, sem integer, category text, breakup json, total integer) TO PUBLIC;


--
-- TOC entry 2507 (class 0 OID 0)
-- Dependencies: 350
-- Name: addFeePayment(text, bigint, json, bigint, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) FROM developer;
GRANT ALL ON FUNCTION "addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) TO developer;
GRANT ALL ON FUNCTION "addFeePayment"(fcomment text, pay_method bigint, details json, amt bigint, regid bigint) TO PUBLIC;


--
-- TOC entry 2508 (class 0 OID 0)
-- Dependencies: 358
-- Name: addFeeTransaction(bigint[], integer, text, date, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "addFeeTransaction"(ref_no bigint[], semester integer, category text, date date, reg_id bigint) TO PUBLIC;


--
-- TOC entry 2509 (class 0 OID 0)
-- Dependencies: 390
-- Name: addFile(text, text, text, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addFile"(directory text, name text, extension text, author bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addFile"(directory text, name text, extension text, author bigint) FROM developer;
GRANT ALL ON FUNCTION "addFile"(directory text, name text, extension text, author bigint) TO developer;
GRANT ALL ON FUNCTION "addFile"(directory text, name text, extension text, author bigint) TO PUBLIC;


--
-- TOC entry 2510 (class 0 OID 0)
-- Dependencies: 283
-- Name: addForm(text, text, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addForm"(formname text, req_fields text, form_format text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addForm"(formname text, req_fields text, form_format text) FROM developer;
GRANT ALL ON FUNCTION "addForm"(formname text, req_fields text, form_format text) TO developer;
GRANT ALL ON FUNCTION "addForm"(formname text, req_fields text, form_format text) TO PUBLIC;


--
-- TOC entry 2511 (class 0 OID 0)
-- Dependencies: 269
-- Name: addGroup(bigint[], text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addGroup"(users_id bigint[], name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addGroup"(users_id bigint[], name text) FROM developer;
GRANT ALL ON FUNCTION "addGroup"(users_id bigint[], name text) TO developer;
GRANT ALL ON FUNCTION "addGroup"(users_id bigint[], name text) TO PUBLIC;


--
-- TOC entry 2512 (class 0 OID 0)
-- Dependencies: 300
-- Name: addGroupNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM postgres;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO postgres;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO PUBLIC;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO developer;


--
-- TOC entry 2513 (class 0 OID 0)
-- Dependencies: 355
-- Name: addGroupNotification(text, text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM postgres;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO postgres;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO PUBLIC;
GRANT ALL ON FUNCTION "addGroupNotification"(ntype text, msg text, link text, notif_author text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO developer;


--
-- TOC entry 2514 (class 0 OID 0)
-- Dependencies: 295
-- Name: addNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) FROM postgres;
GRANT ALL ON FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) TO postgres;
GRANT ALL ON FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "addNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint) TO developer;


--
-- TOC entry 2515 (class 0 OID 0)
-- Dependencies: 262
-- Name: addPost(bigint, bigint, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addPost"(thread_id bigint, erp_id bigint, post_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addPost"(thread_id bigint, erp_id bigint, post_name text) FROM developer;
GRANT ALL ON FUNCTION "addPost"(thread_id bigint, erp_id bigint, post_name text) TO developer;
GRANT ALL ON FUNCTION "addPost"(thread_id bigint, erp_id bigint, post_name text) TO PUBLIC;


--
-- TOC entry 2516 (class 0 OID 0)
-- Dependencies: 275
-- Name: addQuestion(text, text, text[], text[], integer, integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) FROM postgres;
GRANT ALL ON FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) TO postgres;
GRANT ALL ON FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) TO PUBLIC;
GRANT ALL ON FUNCTION "addQuestion"(question text, type text, options text[], answer text[], testpaper integer, marks integer) TO developer;


--
-- TOC entry 2517 (class 0 OID 0)
-- Dependencies: 386
-- Name: addRegistrationFeeBreakup(integer, integer, text, json[], integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) FROM developer;
GRANT ALL ON FUNCTION "addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) TO developer;
GRANT ALL ON FUNCTION "addRegistrationFeeBreakup"(year integer, rnd integer, category text, breakup json[], total integer) TO PUBLIC;


--
-- TOC entry 2518 (class 0 OID 0)
-- Dependencies: 391
-- Name: addRegistrationFeeTransaction(bigint[], bigint, integer, text, date); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) FROM developer;
GRANT ALL ON FUNCTION "addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) TO developer;
GRANT ALL ON FUNCTION "addRegistrationFeeTransaction"(reference_no bigint[], reference_id bigint, rnd integer, cat text, dt date) TO PUBLIC;


--
-- TOC entry 2519 (class 0 OID 0)
-- Dependencies: 277
-- Name: addRegistrationInformation(text, text, text, text, text, text, date, text, boolean, text, text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) FROM postgres;
GRANT ALL ON FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) TO postgres;
GRANT ALL ON FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) TO PUBLIC;
GRANT ALL ON FUNCTION "addRegistrationInformation"(name text, category text, state text, address text, phone_number text, email text, date_of_birth date, program_allocated text, physically_disabled boolean, gender text, nationality text) TO developer;


--
-- TOC entry 2520 (class 0 OID 0)
-- Dependencies: 385
-- Name: addStudent(bigint, text, integer, text, text, text, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "addStudent"(erp_id bigint, sid text, sem integer, course text, branch text, batch text, reg_id bigint) TO PUBLIC;


--
-- TOC entry 2521 (class 0 OID 0)
-- Dependencies: 347
-- Name: addTags(text, text, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addTags"(t_name text, users text, att text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addTags"(t_name text, users text, att text) FROM developer;
GRANT ALL ON FUNCTION "addTags"(t_name text, users text, att text) TO developer;
GRANT ALL ON FUNCTION "addTags"(t_name text, users text, att text) TO PUBLIC;


--
-- TOC entry 2522 (class 0 OID 0)
-- Dependencies: 365
-- Name: addTemplates(text, bigint, bigint, text[]); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addTemplates"(title text, file bigint, author bigint, tags text[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addTemplates"(title text, file bigint, author bigint, tags text[]) FROM developer;
GRANT ALL ON FUNCTION "addTemplates"(title text, file bigint, author bigint, tags text[]) TO developer;
GRANT ALL ON FUNCTION "addTemplates"(title text, file bigint, author bigint, tags text[]) TO PUBLIC;


--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 294
-- Name: addTemplates(text, text, bigint, text[]); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addTemplates"(title text, file text, author bigint, tags text[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addTemplates"(title text, file text, author bigint, tags text[]) FROM developer;
GRANT ALL ON FUNCTION "addTemplates"(title text, file text, author bigint, tags text[]) TO developer;
GRANT ALL ON FUNCTION "addTemplates"(title text, file text, author bigint, tags text[]) TO PUBLIC;


--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 306
-- Name: addThread(bigint, text, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addThread"(cat_id bigint, thread_name text, erp_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addThread"(cat_id bigint, thread_name text, erp_id bigint) FROM developer;
GRANT ALL ON FUNCTION "addThread"(cat_id bigint, thread_name text, erp_id bigint) TO developer;
GRANT ALL ON FUNCTION "addThread"(cat_id bigint, thread_name text, erp_id bigint) TO PUBLIC;


--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 359
-- Name: addUpdateRegistrationStudentDetails(text, text, text, text, text, bigint, text, text, text, text, text, text, text, text, boolean, json, text, text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) FROM postgres;
GRANT ALL ON FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) TO postgres;
GRANT ALL ON FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) TO PUBLIC;
GRANT ALL ON FUNCTION "addUpdateRegistrationStudentDetails"(param_first_name text, param_middle_name text, param_last_name text, param_phone_number text, param_email text, param_registration_id bigint, param_guardian_name text, param_guardian_contact text, param_guardian_email text, param_guardian_address text, param_father_name text, param_father_contact text, param_mother_contact text, param_mother_name text, param_hosteller boolean, param_hostel_address json, param_permanent_address text, param_local_address text) TO developer;


--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 289
-- Name: addUser(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addUser"(user_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addUser"(user_name text) FROM developer;
GRANT ALL ON FUNCTION "addUser"(user_name text) TO developer;
GRANT ALL ON FUNCTION "addUser"(user_name text) TO PUBLIC;


--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 288
-- Name: addUser(text, text, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addUser"(val_user_name text, val_name text, val_user_type text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addUser"(val_user_name text, val_name text, val_user_type text) FROM developer;
GRANT ALL ON FUNCTION "addUser"(val_user_name text, val_name text, val_user_type text) TO developer;
GRANT ALL ON FUNCTION "addUser"(val_user_name text, val_name text, val_user_type text) TO PUBLIC;


--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 380
-- Name: addUsernameGeneration(text[]); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "addUsernameGeneration"(usernames text[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "addUsernameGeneration"(usernames text[]) FROM developer;
GRANT ALL ON FUNCTION "addUsernameGeneration"(usernames text[]) TO developer;
GRANT ALL ON FUNCTION "addUsernameGeneration"(usernames text[]) TO PUBLIC;
GRANT ALL ON FUNCTION "addUsernameGeneration"(usernames text[]) TO postgres;


--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 375
-- Name: applyUpdate(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "applyUpdate"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "applyUpdate"(reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "applyUpdate"(reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "applyUpdate"(reg_id bigint) TO PUBLIC;


--
-- TOC entry 2530 (class 0 OID 0)
-- Dependencies: 284
-- Name: broadcastNotification(text, text, text, timestamp without time zone, timestamp without time zone, bigint[]); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) FROM postgres;
GRANT ALL ON FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO postgres;
GRANT ALL ON FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO PUBLIC;
GRANT ALL ON FUNCTION "broadcastNotification"(ntype text, msg text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone, userid bigint[]) TO developer;


--
-- TOC entry 2531 (class 0 OID 0)
-- Dependencies: 273
-- Name: calculateTotal(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "calculateTotal"(answer_sheet_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "calculateTotal"(answer_sheet_id integer) FROM postgres;
GRANT ALL ON FUNCTION "calculateTotal"(answer_sheet_id integer) TO postgres;
GRANT ALL ON FUNCTION "calculateTotal"(answer_sheet_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "calculateTotal"(answer_sheet_id integer) TO developer;


--
-- TOC entry 2532 (class 0 OID 0)
-- Dependencies: 293
-- Name: createNotification(text, text, text, timestamp without time zone, timestamp without time zone); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) FROM PUBLIC;
REVOKE ALL ON FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) FROM postgres;
GRANT ALL ON FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) TO postgres;
GRANT ALL ON FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) TO PUBLIC;
GRANT ALL ON FUNCTION "createNotification"(notif_type text, message text, link text, notif_timestamp timestamp without time zone, expiry timestamp without time zone) TO developer;


--
-- TOC entry 2533 (class 0 OID 0)
-- Dependencies: 393
-- Name: deleteFalseUsername(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "deleteFalseUsername"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "deleteFalseUsername"() FROM developer;
GRANT ALL ON FUNCTION "deleteFalseUsername"() TO developer;
GRANT ALL ON FUNCTION "deleteFalseUsername"() TO PUBLIC;


--
-- TOC entry 2534 (class 0 OID 0)
-- Dependencies: 374
-- Name: deleteFile(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "deleteFile"(dir text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "deleteFile"(dir text) FROM developer;
GRANT ALL ON FUNCTION "deleteFile"(dir text) TO developer;
GRANT ALL ON FUNCTION "deleteFile"(dir text) TO PUBLIC;


--
-- TOC entry 2535 (class 0 OID 0)
-- Dependencies: 203
-- Name: csab_student_list; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE csab_student_list FROM PUBLIC;
REVOKE ALL ON TABLE csab_student_list FROM postgres;
GRANT ALL ON TABLE csab_student_list TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE csab_student_list TO developer;


--
-- TOC entry 2536 (class 0 OID 0)
-- Dependencies: 360
-- Name: displayCsabList(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "displayCsabList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "displayCsabList"() FROM postgres;
GRANT ALL ON FUNCTION "displayCsabList"() TO postgres;
GRANT ALL ON FUNCTION "displayCsabList"() TO PUBLIC;
GRANT ALL ON FUNCTION "displayCsabList"() TO developer;


--
-- TOC entry 2537 (class 0 OID 0)
-- Dependencies: 373
-- Name: displayCsabProfile(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "displayCsabProfile"(id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "displayCsabProfile"(id bigint) FROM postgres;
GRANT ALL ON FUNCTION "displayCsabProfile"(id bigint) TO postgres;
GRANT ALL ON FUNCTION "displayCsabProfile"(id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "displayCsabProfile"(id bigint) TO developer;


--
-- TOC entry 2538 (class 0 OID 0)
-- Dependencies: 332
-- Name: displayFaculty(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "displayFaculty"(faculty text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "displayFaculty"(faculty text) FROM developer;
GRANT ALL ON FUNCTION "displayFaculty"(faculty text) TO developer;
GRANT ALL ON FUNCTION "displayFaculty"(faculty text) TO PUBLIC;


--
-- TOC entry 2539 (class 0 OID 0)
-- Dependencies: 252
-- Name: existsRegId(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "existsRegId"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "existsRegId"(reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "existsRegId"(reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "existsRegId"(reg_id bigint) TO PUBLIC;


--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 383
-- Name: facultyAvail(text, integer, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "facultyAvail"(day2 text, time1 integer, faculty text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "facultyAvail"(day2 text, time1 integer, faculty text) FROM developer;
GRANT ALL ON FUNCTION "facultyAvail"(day2 text, time1 integer, faculty text) TO developer;
GRANT ALL ON FUNCTION "facultyAvail"(day2 text, time1 integer, faculty text) TO PUBLIC;


--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 382
-- Name: facultyAvailability(text, integer, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "facultyAvailability"(day text, time1 integer, faculty text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "facultyAvailability"(day text, time1 integer, faculty text) FROM developer;
GRANT ALL ON FUNCTION "facultyAvailability"(day text, time1 integer, faculty text) TO developer;
GRANT ALL ON FUNCTION "facultyAvailability"(day text, time1 integer, faculty text) TO PUBLIC;


--
-- TOC entry 2542 (class 0 OID 0)
-- Dependencies: 285
-- Name: feeVerify(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "feeVerify"(ref bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "feeVerify"(ref bigint) FROM developer;
GRANT ALL ON FUNCTION "feeVerify"(ref bigint) TO developer;
GRANT ALL ON FUNCTION "feeVerify"(ref bigint) TO PUBLIC;


--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 226
-- Name: forum_category; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE forum_category FROM PUBLIC;
REVOKE ALL ON TABLE forum_category FROM developer;
GRANT ALL ON TABLE forum_category TO developer;


--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 335
-- Name: getAllCategories(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getAllCategories"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllCategories"() FROM developer;
GRANT ALL ON FUNCTION "getAllCategories"() TO developer;
GRANT ALL ON FUNCTION "getAllCategories"() TO PUBLIC;


--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 313
-- Name: getAllConversationMessages(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAllConversationMessages"(convo_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllConversationMessages"(convo_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getAllConversationMessages"(convo_id integer) TO postgres;
GRANT ALL ON FUNCTION "getAllConversationMessages"(convo_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getAllConversationMessages"(convo_id integer) TO developer;


--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 312
-- Name: getAllFileData(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAllFileData"(fid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllFileData"(fid bigint) FROM postgres;
GRANT ALL ON FUNCTION "getAllFileData"(fid bigint) TO postgres;
GRANT ALL ON FUNCTION "getAllFileData"(fid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getAllFileData"(fid bigint) TO developer;


--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 319
-- Name: getAllFoo(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAllFoo"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllFoo"() FROM postgres;
GRANT ALL ON FUNCTION "getAllFoo"() TO postgres;
GRANT ALL ON FUNCTION "getAllFoo"() TO PUBLIC;
GRANT ALL ON FUNCTION "getAllFoo"() TO developer;


--
-- TOC entry 2548 (class 0 OID 0)
-- Dependencies: 263
-- Name: getAllTemplates(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAllTemplates"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllTemplates"() FROM postgres;
GRANT ALL ON FUNCTION "getAllTemplates"() TO postgres;
GRANT ALL ON FUNCTION "getAllTemplates"() TO PUBLIC;
GRANT ALL ON FUNCTION "getAllTemplates"() TO developer;


--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 368
-- Name: getAllUnreadMessages(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAllUnreadMessages"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAllUnreadMessages"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getAllUnreadMessages"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getAllUnreadMessages"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getAllUnreadMessages"(user_id bigint) TO developer;


--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 172
-- Name: test_answer; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE test_answer FROM PUBLIC;
REVOKE ALL ON TABLE test_answer FROM postgres;
GRANT ALL ON TABLE test_answer TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE test_answer TO developer;


--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 401
-- Name: getAnswer(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getAnswer"(answer_sheet_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAnswer"(answer_sheet_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getAnswer"(answer_sheet_id integer) TO postgres;
GRANT ALL ON FUNCTION "getAnswer"(answer_sheet_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getAnswer"(answer_sheet_id integer) TO developer;


--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 400
-- Name: getAnswerSheet(integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getAnswerSheet"(test_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getAnswerSheet"(test_id integer) FROM developer;
GRANT ALL ON FUNCTION "getAnswerSheet"(test_id integer) TO developer;
GRANT ALL ON FUNCTION "getAnswerSheet"(test_id integer) TO PUBLIC;


--
-- TOC entry 2553 (class 0 OID 0)
-- Dependencies: 270
-- Name: getBranchName(text, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getBranchName"(faculty text, sem_no integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getBranchName"(faculty text, sem_no integer) FROM developer;
GRANT ALL ON FUNCTION "getBranchName"(faculty text, sem_no integer) TO developer;
GRANT ALL ON FUNCTION "getBranchName"(faculty text, sem_no integer) TO PUBLIC;


--
-- TOC entry 2554 (class 0 OID 0)
-- Dependencies: 257
-- Name: getCategory(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCategory"(cat_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCategory"(cat_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getCategory"(cat_id bigint) TO developer;
GRANT ALL ON FUNCTION "getCategory"(cat_id bigint) TO PUBLIC;


--
-- TOC entry 2555 (class 0 OID 0)
-- Dependencies: 326
-- Name: getCategoryName(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCategoryName"(cat_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCategoryName"(cat_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getCategoryName"(cat_id bigint) TO developer;
GRANT ALL ON FUNCTION "getCategoryName"(cat_id bigint) TO PUBLIC;


--
-- TOC entry 2556 (class 0 OID 0)
-- Dependencies: 328
-- Name: getChatId(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getChatId"(conversation_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getChatId"(conversation_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getChatId"(conversation_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getChatId"(conversation_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getChatId"(conversation_id bigint) TO developer;


--
-- TOC entry 2557 (class 0 OID 0)
-- Dependencies: 304
-- Name: getChatMessages(bigint, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) FROM postgres;
GRANT ALL ON FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) TO postgres;
GRANT ALL ON FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getChatMessages"(conversation_id bigint, set integer, limit_value integer) TO developer;


--
-- TOC entry 2558 (class 0 OID 0)
-- Dependencies: 329
-- Name: getChatName(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getChatName"(chat bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getChatName"(chat bigint) FROM postgres;
GRANT ALL ON FUNCTION "getChatName"(chat bigint) TO postgres;
GRANT ALL ON FUNCTION "getChatName"(chat bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getChatName"(chat bigint) TO developer;


--
-- TOC entry 2559 (class 0 OID 0)
-- Dependencies: 291
-- Name: getConversationId(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getConversationId"(chat_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getConversationId"(chat_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getConversationId"(chat_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getConversationId"(chat_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getConversationId"(chat_id bigint) TO developer;


--
-- TOC entry 2560 (class 0 OID 0)
-- Dependencies: 330
-- Name: getConversationsInfo(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getConversationsInfo"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getConversationsInfo"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getConversationsInfo"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getConversationsInfo"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getConversationsInfo"(user_id bigint) TO developer;


--
-- TOC entry 2561 (class 0 OID 0)
-- Dependencies: 362
-- Name: getCourseBySemYear(integer, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) FROM developer;
GRANT ALL ON FUNCTION "getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) TO developer;
GRANT ALL ON FUNCTION "getCourseBySemYear"(input_year integer, input_semester integer, OUT course_code text, OUT credits integer) TO PUBLIC;


--
-- TOC entry 2562 (class 0 OID 0)
-- Dependencies: 253
-- Name: getCourseGradeList(text, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCourseGradeList"(input_course_code text, registration_year integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCourseGradeList"(input_course_code text, registration_year integer) FROM developer;
GRANT ALL ON FUNCTION "getCourseGradeList"(input_course_code text, registration_year integer) TO developer;
GRANT ALL ON FUNCTION "getCourseGradeList"(input_course_code text, registration_year integer) TO PUBLIC;


--
-- TOC entry 2563 (class 0 OID 0)
-- Dependencies: 349
-- Name: getCourseList(text, integer, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCourseList"(faculty text, sem_no integer, branch text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCourseList"(faculty text, sem_no integer, branch text) FROM developer;
GRANT ALL ON FUNCTION "getCourseList"(faculty text, sem_no integer, branch text) TO developer;
GRANT ALL ON FUNCTION "getCourseList"(faculty text, sem_no integer, branch text) TO PUBLIC;


--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 356
-- Name: getCourse_code(integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getCourse_code"(sem integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCourse_code"(sem integer) FROM developer;
GRANT ALL ON FUNCTION "getCourse_code"(sem integer) TO developer;
GRANT ALL ON FUNCTION "getCourse_code"(sem integer) TO PUBLIC;


--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 303
-- Name: getCoversationsInfo(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getCoversationsInfo"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCoversationsInfo"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getCoversationsInfo"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getCoversationsInfo"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getCoversationsInfo"(user_id bigint) TO developer;


--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 271
-- Name: getCredit(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getCredit"(course_code text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getCredit"(course_code text) FROM postgres;
GRANT ALL ON FUNCTION "getCredit"(course_code text) TO postgres;
GRANT ALL ON FUNCTION "getCredit"(course_code text) TO PUBLIC;
GRANT ALL ON FUNCTION "getCredit"(course_code text) TO developer;


--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 215
-- Name: files; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE files FROM PUBLIC;
REVOKE ALL ON TABLE files FROM developer;
GRANT ALL ON TABLE files TO developer;


--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 348
-- Name: getDirectory(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getDirectory"(dir text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getDirectory"(dir text) FROM developer;
GRANT ALL ON FUNCTION "getDirectory"(dir text) TO developer;
GRANT ALL ON FUNCTION "getDirectory"(dir text) TO PUBLIC;


--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 296
-- Name: getDocuments(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getDocuments"(owner_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getDocuments"(owner_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getDocuments"(owner_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getDocuments"(owner_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getDocuments"(owner_id bigint) TO developer;


--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 282
-- Name: getFacultyCourseCode(text, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFacultyCourseCode"(faculty text, semester_no integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFacultyCourseCode"(faculty text, semester_no integer) FROM developer;
GRANT ALL ON FUNCTION "getFacultyCourseCode"(faculty text, semester_no integer) TO developer;
GRANT ALL ON FUNCTION "getFacultyCourseCode"(faculty text, semester_no integer) TO PUBLIC;


--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 396
-- Name: getFeeAmount(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getFeeAmount"(userid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeAmount"(userid bigint) FROM postgres;
GRANT ALL ON FUNCTION "getFeeAmount"(userid bigint) TO postgres;
GRANT ALL ON FUNCTION "getFeeAmount"(userid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getFeeAmount"(userid bigint) TO developer;


--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 323
-- Name: getFeeAmount(integer, text, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeeAmount"(sem integer, cat text, bat integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeAmount"(sem integer, cat text, bat integer) FROM developer;
GRANT ALL ON FUNCTION "getFeeAmount"(sem integer, cat text, bat integer) TO developer;
GRANT ALL ON FUNCTION "getFeeAmount"(sem integer, cat text, bat integer) TO PUBLIC;


--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 193
-- Name: fee_breakup; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE fee_breakup FROM PUBLIC;
REVOKE ALL ON TABLE fee_breakup FROM developer;
GRANT ALL ON TABLE fee_breakup TO developer;


--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 311
-- Name: getFeeBreakup(integer, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeeBreakup"(sem integer, acad_year integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeBreakup"(sem integer, acad_year integer) FROM developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(sem integer, acad_year integer) TO developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(sem integer, acad_year integer) TO PUBLIC;


--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 309
-- Name: getFeeBreakup(integer, integer, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeeBreakup"(year integer, sem integer, user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeBreakup"(year integer, sem integer, user_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(year integer, sem integer, user_id bigint) TO developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(year integer, sem integer, user_id bigint) TO PUBLIC;


--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 307
-- Name: getFeeBreakup(integer, integer, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) FROM developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) TO developer;
GRANT ALL ON FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) TO PUBLIC;
GRANT ALL ON FUNCTION "getFeeBreakup"(bat integer, sem integer, cat text) TO postgres;


--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 394
-- Name: getFeeJson(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getFeeJson"(userid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeJson"(userid bigint) FROM postgres;
GRANT ALL ON FUNCTION "getFeeJson"(userid bigint) TO postgres;
GRANT ALL ON FUNCTION "getFeeJson"(userid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getFeeJson"(userid bigint) TO developer;


--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 324
-- Name: getFeeJson(integer, integer, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeeJson"(fee_year integer, sem integer, cat text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeeJson"(fee_year integer, sem integer, cat text) FROM developer;
GRANT ALL ON FUNCTION "getFeeJson"(fee_year integer, sem integer, cat text) TO developer;
GRANT ALL ON FUNCTION "getFeeJson"(fee_year integer, sem integer, cat text) TO PUBLIC;


--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 318
-- Name: getFeePaymentDetails(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeePaymentDetails"(rid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeePaymentDetails"(rid bigint) FROM developer;
GRANT ALL ON FUNCTION "getFeePaymentDetails"(rid bigint) TO developer;
GRANT ALL ON FUNCTION "getFeePaymentDetails"(rid bigint) TO PUBLIC;


--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 199
-- Name: fee_payment; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE fee_payment FROM PUBLIC;
REVOKE ALL ON TABLE fee_payment FROM developer;
GRANT ALL ON TABLE fee_payment TO developer;


--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 316
-- Name: getFeePaymentHistory(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeePaymentHistory"(userid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeePaymentHistory"(userid bigint) FROM developer;
GRANT ALL ON FUNCTION "getFeePaymentHistory"(userid bigint) TO developer;
GRANT ALL ON FUNCTION "getFeePaymentHistory"(userid bigint) TO PUBLIC;


--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 322
-- Name: getFeePaymentList(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFeePaymentList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFeePaymentList"() FROM developer;
GRANT ALL ON FUNCTION "getFeePaymentList"() TO developer;
GRANT ALL ON FUNCTION "getFeePaymentList"() TO PUBLIC;


--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 334
-- Name: getFormFormat(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFormFormat"(formname text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFormFormat"(formname text) FROM developer;
GRANT ALL ON FUNCTION "getFormFormat"(formname text) TO developer;
GRANT ALL ON FUNCTION "getFormFormat"(formname text) TO PUBLIC;


--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 340
-- Name: getFormName(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getFormName"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getFormName"() FROM developer;
GRANT ALL ON FUNCTION "getFormName"() TO developer;
GRANT ALL ON FUNCTION "getFormName"() TO PUBLIC;


--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 315
-- Name: getLDAPStudentStatus(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getLDAPStudentStatus"(rid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getLDAPStudentStatus"(rid bigint) FROM developer;
GRANT ALL ON FUNCTION "getLDAPStudentStatus"(rid bigint) TO developer;
GRANT ALL ON FUNCTION "getLDAPStudentStatus"(rid bigint) TO PUBLIC;


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 292
-- Name: getMessagesId(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getMessagesId"(convo_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getMessagesId"(convo_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getMessagesId"(convo_id integer) TO postgres;
GRANT ALL ON FUNCTION "getMessagesId"(convo_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getMessagesId"(convo_id integer) TO developer;


--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 224
-- Name: comments; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE comments FROM PUBLIC;
REVOKE ALL ON TABLE comments FROM developer;
GRANT ALL ON TABLE comments TO developer;


--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 321
-- Name: getMultipleComments(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getMultipleComments"(p_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getMultipleComments"(p_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getMultipleComments"(p_id bigint) TO developer;
GRANT ALL ON FUNCTION "getMultipleComments"(p_id bigint) TO PUBLIC;


--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 223
-- Name: posts; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE posts FROM PUBLIC;
REVOKE ALL ON TABLE posts FROM developer;
GRANT ALL ON TABLE posts TO developer;


--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 305
-- Name: getMultiplePosts(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getMultiplePosts"(t_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getMultiplePosts"(t_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getMultiplePosts"(t_id bigint) TO developer;
GRANT ALL ON FUNCTION "getMultiplePosts"(t_id bigint) TO PUBLIC;


--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 221
-- Name: threads; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE threads FROM PUBLIC;
REVOKE ALL ON TABLE threads FROM developer;
GRANT ALL ON TABLE threads TO developer;


--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 301
-- Name: getMultipleThreads(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getMultipleThreads"(cat_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getMultipleThreads"(cat_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getMultipleThreads"(cat_id bigint) TO developer;
GRANT ALL ON FUNCTION "getMultipleThreads"(cat_id bigint) TO PUBLIC;


--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 251
-- Name: getPaymentDetails(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getPaymentDetails"(rid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getPaymentDetails"(rid bigint) FROM developer;
GRANT ALL ON FUNCTION "getPaymentDetails"(rid bigint) TO developer;
GRANT ALL ON FUNCTION "getPaymentDetails"(rid bigint) TO PUBLIC;


--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 302
-- Name: getProgram(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getProgram"(sid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getProgram"(sid bigint) FROM developer;
GRANT ALL ON FUNCTION "getProgram"(sid bigint) TO developer;
GRANT ALL ON FUNCTION "getProgram"(sid bigint) TO PUBLIC;


--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 176
-- Name: test_question; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE test_question FROM PUBLIC;
REVOKE ALL ON TABLE test_question FROM postgres;
GRANT ALL ON TABLE test_question TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE test_question TO developer;


--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 357
-- Name: getQuestions(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getQuestions"(testpaper_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getQuestions"(testpaper_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getQuestions"(testpaper_id integer) TO postgres;
GRANT ALL ON FUNCTION "getQuestions"(testpaper_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getQuestions"(testpaper_id integer) TO developer;


--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 233
-- Name: registration_fee_breakup; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE registration_fee_breakup FROM PUBLIC;
REVOKE ALL ON TABLE registration_fee_breakup FROM postgres;
GRANT ALL ON TABLE registration_fee_breakup TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE registration_fee_breakup TO developer;


--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 384
-- Name: getRegistrationFeeBreakup(text, integer, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) FROM developer;
GRANT ALL ON FUNCTION "getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) TO developer;
GRANT ALL ON FUNCTION "getRegistrationFeeBreakup"(cat text, rnd integer, reg_year integer) TO PUBLIC;


--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 378
-- Name: getRegistrationStatus(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getRegistrationStatus"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getRegistrationStatus"(reg_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getRegistrationStatus"(reg_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getRegistrationStatus"(reg_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getRegistrationStatus"(reg_id bigint) TO developer;


--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 171
-- Name: registration_student_list; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE registration_student_list FROM PUBLIC;
REVOKE ALL ON TABLE registration_student_list FROM developer;
GRANT ALL ON TABLE registration_student_list TO developer;


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 267
-- Name: getRegistrationStudentData(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getRegistrationStudentData"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getRegistrationStudentData"(reg_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getRegistrationStudentData"(reg_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getRegistrationStudentData"(reg_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getRegistrationStudentData"(reg_id bigint) TO developer;


--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 266
-- Name: getRequiredFields(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getRequiredFields"(formname text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getRequiredFields"(formname text) FROM developer;
GRANT ALL ON FUNCTION "getRequiredFields"(formname text) TO developer;
GRANT ALL ON FUNCTION "getRequiredFields"(formname text) TO PUBLIC;


--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 274
-- Name: getSingleComment(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSingleComment"(c_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSingleComment"(c_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getSingleComment"(c_id bigint) TO developer;
GRANT ALL ON FUNCTION "getSingleComment"(c_id bigint) TO PUBLIC;


--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 256
-- Name: getSinglePost(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSinglePost"(p_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSinglePost"(p_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getSinglePost"(p_id bigint) TO developer;
GRANT ALL ON FUNCTION "getSinglePost"(p_id bigint) TO PUBLIC;


--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 259
-- Name: getSingleThread(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSingleThread"(t_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSingleThread"(t_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getSingleThread"(t_id bigint) TO developer;
GRANT ALL ON FUNCTION "getSingleThread"(t_id bigint) TO PUBLIC;


--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 258
-- Name: getSolutionSheet(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getSolutionSheet"(answer_sheet_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSolutionSheet"(answer_sheet_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getSolutionSheet"(answer_sheet_id integer) TO postgres;
GRANT ALL ON FUNCTION "getSolutionSheet"(answer_sheet_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getSolutionSheet"(answer_sheet_id integer) TO developer;


--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 217
-- Name: students_list; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE students_list FROM PUBLIC;
REVOKE ALL ON TABLE students_list FROM developer;
GRANT ALL ON TABLE students_list TO developer;


--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 249
-- Name: getStudent(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudent"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudent"() FROM developer;
GRANT ALL ON FUNCTION "getStudent"() TO developer;
GRANT ALL ON FUNCTION "getStudent"() TO PUBLIC;


--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 364
-- Name: getStudentAttendanceList(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentAttendanceList"(course_code text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentAttendanceList"(course_code text) FROM developer;
GRANT ALL ON FUNCTION "getStudentAttendanceList"(course_code text) TO developer;
GRANT ALL ON FUNCTION "getStudentAttendanceList"(course_code text) TO PUBLIC;


--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 352
-- Name: getStudentAttendanceList(text, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentAttendanceList"(course text, branch text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentAttendanceList"(course text, branch text) FROM developer;
GRANT ALL ON FUNCTION "getStudentAttendanceList"(course text, branch text) TO developer;
GRANT ALL ON FUNCTION "getStudentAttendanceList"(course text, branch text) TO PUBLIC;


--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 403
-- Name: getStudentList(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getStudentList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentList"() FROM postgres;
GRANT ALL ON FUNCTION "getStudentList"() TO postgres;
GRANT ALL ON FUNCTION "getStudentList"() TO PUBLIC;
GRANT ALL ON FUNCTION "getStudentList"() TO developer;


--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 260
-- Name: getStudentListById(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentListById"(std_id text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentListById"(std_id text) FROM developer;
GRANT ALL ON FUNCTION "getStudentListById"(std_id text) TO developer;
GRANT ALL ON FUNCTION "getStudentListById"(std_id text) TO PUBLIC;


--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 399
-- Name: getStudentProfile(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentProfile"(param bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentProfile"(param bigint) FROM developer;
GRANT ALL ON FUNCTION "getStudentProfile"(param bigint) TO developer;
GRANT ALL ON FUNCTION "getStudentProfile"(param bigint) TO PUBLIC;


--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 381
-- Name: getStudentRegistrationCount(text, integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentRegistrationCount"(program text, sem integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentRegistrationCount"(program text, sem integer) FROM developer;
GRANT ALL ON FUNCTION "getStudentRegistrationCount"(program text, sem integer) TO developer;
GRANT ALL ON FUNCTION "getStudentRegistrationCount"(program text, sem integer) TO PUBLIC;


--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 190
-- Name: update_registration_student_list; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE update_registration_student_list FROM PUBLIC;
REVOKE ALL ON TABLE update_registration_student_list FROM postgres;
GRANT ALL ON TABLE update_registration_student_list TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE update_registration_student_list TO developer;


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 317
-- Name: getStudentRegistrationDataUpdate(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getStudentRegistrationDataUpdate"(reg_id bigint) TO developer;


--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 325
-- Name: getStudentRegistrationList(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getStudentRegistrationList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentRegistrationList"() FROM developer;
GRANT ALL ON FUNCTION "getStudentRegistrationList"() TO developer;
GRANT ALL ON FUNCTION "getStudentRegistrationList"() TO PUBLIC;


--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 395
-- Name: getStudentsList(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getStudentsList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getStudentsList"() FROM postgres;
GRANT ALL ON FUNCTION "getStudentsList"() TO postgres;
GRANT ALL ON FUNCTION "getStudentsList"() TO PUBLIC;
GRANT ALL ON FUNCTION "getStudentsList"() TO developer;


--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 310
-- Name: getSubdirectory(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSubdirectory"(subdir text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSubdirectory"(subdir text) FROM developer;
GRANT ALL ON FUNCTION "getSubdirectory"(subdir text) TO developer;
GRANT ALL ON FUNCTION "getSubdirectory"(subdir text) TO PUBLIC;


--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 255
-- Name: getSubjectAllocation(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSubjectAllocation"(faculty text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSubjectAllocation"(faculty text) FROM developer;
GRANT ALL ON FUNCTION "getSubjectAllocation"(faculty text) TO developer;
GRANT ALL ON FUNCTION "getSubjectAllocation"(faculty text) TO PUBLIC;


--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 192
-- Name: subject_list; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE subject_list FROM PUBLIC;
REVOKE ALL ON TABLE subject_list FROM developer;
GRANT ALL ON TABLE subject_list TO developer;


--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 363
-- Name: getSubjectList(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSubjectList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSubjectList"() FROM developer;
GRANT ALL ON FUNCTION "getSubjectList"() TO developer;
GRANT ALL ON FUNCTION "getSubjectList"() TO PUBLIC;


--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 392
-- Name: getSubjects(integer); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getSubjects"(sem integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getSubjects"(sem integer) FROM developer;
GRANT ALL ON FUNCTION "getSubjects"(sem integer) TO developer;
GRANT ALL ON FUNCTION "getSubjects"(sem integer) TO PUBLIC;


--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 342
-- Name: getTags(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getTags"(tag_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getTags"(tag_name text) FROM developer;
GRANT ALL ON FUNCTION "getTags"(tag_name text) TO developer;
GRANT ALL ON FUNCTION "getTags"(tag_name text) TO PUBLIC;


--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 230
-- Name: templates; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE templates FROM PUBLIC;
REVOKE ALL ON TABLE templates FROM postgres;
GRANT ALL ON TABLE templates TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE templates TO developer;


--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 351
-- Name: getTemplates(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getTemplates"(id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getTemplates"(id integer) FROM postgres;
GRANT ALL ON FUNCTION "getTemplates"(id integer) TO postgres;
GRANT ALL ON FUNCTION "getTemplates"(id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getTemplates"(id integer) TO developer;


--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 370
-- Name: getTestPaper(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getTestPaper"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getTestPaper"() FROM postgres;
GRANT ALL ON FUNCTION "getTestPaper"() TO postgres;
GRANT ALL ON FUNCTION "getTestPaper"() TO PUBLIC;
GRANT ALL ON FUNCTION "getTestPaper"() TO developer;


--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 361
-- Name: getThreadName(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getThreadName"(t_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getThreadName"(t_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getThreadName"(t_id bigint) TO developer;
GRANT ALL ON FUNCTION "getThreadName"(t_id bigint) TO PUBLIC;


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 272
-- Name: getTotal(integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getTotal"(answer_sheet_id integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getTotal"(answer_sheet_id integer) FROM postgres;
GRANT ALL ON FUNCTION "getTotal"(answer_sheet_id integer) TO postgres;
GRANT ALL ON FUNCTION "getTotal"(answer_sheet_id integer) TO PUBLIC;
GRANT ALL ON FUNCTION "getTotal"(answer_sheet_id integer) TO developer;


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 268
-- Name: getUnreadMessages(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUnreadMessages"(conversation_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUnreadMessages"(conversation_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getUnreadMessages"(conversation_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getUnreadMessages"(conversation_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUnreadMessages"(conversation_id bigint) TO developer;


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 189
-- Name: notifications; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE notifications FROM PUBLIC;
REVOKE ALL ON TABLE notifications FROM developer;
GRANT ALL ON TABLE notifications TO developer;


--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 387
-- Name: getUnreadNotifications(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUnreadNotifications"(userid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUnreadNotifications"(userid bigint) FROM postgres;
GRANT ALL ON FUNCTION "getUnreadNotifications"(userid bigint) TO postgres;
GRANT ALL ON FUNCTION "getUnreadNotifications"(userid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUnreadNotifications"(userid bigint) TO developer;


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 367
-- Name: getUserAnswerSheets(integer, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getUserAnswerSheets"(test_id integer, erp_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserAnswerSheets"(test_id integer, erp_id bigint) FROM developer;
GRANT ALL ON FUNCTION "getUserAnswerSheets"(test_id integer, erp_id bigint) TO developer;
GRANT ALL ON FUNCTION "getUserAnswerSheets"(test_id integer, erp_id bigint) TO PUBLIC;


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 346
-- Name: getUserAutoSuggest(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getUserAutoSuggest"(suggest_word text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserAutoSuggest"(suggest_word text) FROM developer;
GRANT ALL ON FUNCTION "getUserAutoSuggest"(suggest_word text) TO developer;
GRANT ALL ON FUNCTION "getUserAutoSuggest"(suggest_word text) TO PUBLIC;


--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 320
-- Name: getUserEmail(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUserEmail"(user_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserEmail"(user_name text) FROM postgres;
GRANT ALL ON FUNCTION "getUserEmail"(user_name text) TO postgres;
GRANT ALL ON FUNCTION "getUserEmail"(user_name text) TO PUBLIC;
GRANT ALL ON FUNCTION "getUserEmail"(user_name text) TO developer;


--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 290
-- Name: getUserId(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUserId"(user_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserId"(user_name text) FROM postgres;
GRANT ALL ON FUNCTION "getUserId"(user_name text) TO postgres;
GRANT ALL ON FUNCTION "getUserId"(user_name text) TO PUBLIC;
GRANT ALL ON FUNCTION "getUserId"(user_name text) TO developer;


--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 344
-- Name: getUserName(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUserName"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserName"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getUserName"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getUserName"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUserName"(user_id bigint) TO developer;


--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 379
-- Name: getUserNameGeneration(); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getUserNameGeneration"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserNameGeneration"() FROM developer;
GRANT ALL ON FUNCTION "getUserNameGeneration"() TO developer;
GRANT ALL ON FUNCTION "getUserNameGeneration"() TO PUBLIC;


--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 343
-- Name: getUserUserName(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUserUserName"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserUserName"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getUserUserName"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getUserUserName"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUserUserName"(user_id bigint) TO developer;


--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 398
-- Name: getUserUsernameById(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUserUsernameById"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUserUsernameById"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "getUserUsernameById"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "getUserUsernameById"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUserUsernameById"(user_id bigint) TO developer;


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 388
-- Name: getUsernameGenerationData(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getUsernameGenerationData"(rid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUsernameGenerationData"(rid bigint) FROM developer;
GRANT ALL ON FUNCTION "getUsernameGenerationData"(rid bigint) TO developer;
GRANT ALL ON FUNCTION "getUsernameGenerationData"(rid bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "getUsernameGenerationData"(rid bigint) TO postgres;


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 264
-- Name: getUsernameStatus(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "getUsernameStatus"(uname text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUsernameStatus"(uname text) FROM developer;
GRANT ALL ON FUNCTION "getUsernameStatus"(uname text) TO developer;
GRANT ALL ON FUNCTION "getUsernameStatus"(uname text) TO PUBLIC;


--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 314
-- Name: getUsers(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUsers"(type text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUsers"(type text) FROM postgres;
GRANT ALL ON FUNCTION "getUsers"(type text) TO postgres;
GRANT ALL ON FUNCTION "getUsers"(type text) TO PUBLIC;
GRANT ALL ON FUNCTION "getUsers"(type text) TO developer;


--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 180
-- Name: users; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM postgres;
GRANT ALL ON TABLE users TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users TO developer;


--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 366
-- Name: getUsersByType(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUsersByType"(input_type text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUsersByType"(input_type text) FROM postgres;
GRANT ALL ON FUNCTION "getUsersByType"(input_type text) TO postgres;
GRANT ALL ON FUNCTION "getUsersByType"(input_type text) TO PUBLIC;
GRANT ALL ON FUNCTION "getUsersByType"(input_type text) TO developer;


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 402
-- Name: getUsersList(); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "getUsersList"() FROM PUBLIC;
REVOKE ALL ON FUNCTION "getUsersList"() FROM postgres;
GRANT ALL ON FUNCTION "getUsersList"() TO postgres;
GRANT ALL ON FUNCTION "getUsersList"() TO PUBLIC;
GRANT ALL ON FUNCTION "getUsersList"() TO developer;


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 345
-- Name: markAsReadMessages(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "markAsReadMessages"(conversation_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "markAsReadMessages"(conversation_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "markAsReadMessages"(conversation_id bigint) TO postgres;
GRANT ALL ON FUNCTION "markAsReadMessages"(conversation_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "markAsReadMessages"(conversation_id bigint) TO developer;


--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 281
-- Name: newAnswerSheet(integer, bigint, timestamp without time zone, text, integer[]); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) FROM postgres;
GRANT ALL ON FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) TO postgres;
GRANT ALL ON FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) TO PUBLIC;
GRANT ALL ON FUNCTION "newAnswerSheet"(test_paper_id integer, author bigint, submission_time timestamp without time zone, status text, answers integer[]) TO developer;


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 333
-- Name: newConversation(bigint[], text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "newConversation"(members bigint[], name_of_chat_group text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "newConversation"(members bigint[], name_of_chat_group text) FROM postgres;
GRANT ALL ON FUNCTION "newConversation"(members bigint[], name_of_chat_group text) TO postgres;
GRANT ALL ON FUNCTION "newConversation"(members bigint[], name_of_chat_group text) TO PUBLIC;
GRANT ALL ON FUNCTION "newConversation"(members bigint[], name_of_chat_group text) TO developer;


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 369
-- Name: newMessage(text, bigint, bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) TO postgres;
GRANT ALL ON FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "newMessage"(message text, author bigint, conversation_id bigint) TO developer;


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 298
-- Name: newTestPaper(integer[], text, bigint, date, text, interval); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) FROM PUBLIC;
REVOKE ALL ON FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) FROM postgres;
GRANT ALL ON FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) TO postgres;
GRANT ALL ON FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) TO PUBLIC;
GRANT ALL ON FUNCTION "newTestPaper"(questions integer[], subject text, author bigint, creation_date date, status text, duration interval) TO developer;


--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 280
-- Name: readNotification(bigint, bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "readNotification"(userid bigint, notif_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "readNotification"(userid bigint, notif_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "readNotification"(userid bigint, notif_id bigint) TO postgres;
GRANT ALL ON FUNCTION "readNotification"(userid bigint, notif_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "readNotification"(userid bigint, notif_id bigint) TO developer;


--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 331
-- Name: report(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION report(csab_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION report(csab_id bigint) FROM postgres;
GRANT ALL ON FUNCTION report(csab_id bigint) TO postgres;
GRANT ALL ON FUNCTION report(csab_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION report(csab_id bigint) TO developer;


--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 308
-- Name: report_student(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION report_student(csab bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION report_student(csab bigint) FROM postgres;
GRANT ALL ON FUNCTION report_student(csab bigint) TO postgres;
GRANT ALL ON FUNCTION report_student(csab bigint) TO PUBLIC;
GRANT ALL ON FUNCTION report_student(csab bigint) TO developer;


--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 327
-- Name: sendNotification(bigint, bigint[]); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) FROM PUBLIC;
REVOKE ALL ON FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) FROM postgres;
GRANT ALL ON FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) TO postgres;
GRANT ALL ON FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) TO PUBLIC;
GRANT ALL ON FUNCTION "sendNotification"(notif_id bigint, userid bigint[]) TO developer;


--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 297
-- Name: setAnswerCorrection(boolean, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) FROM PUBLIC;
REVOKE ALL ON FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) FROM postgres;
GRANT ALL ON FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) TO postgres;
GRANT ALL ON FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) TO PUBLIC;
GRANT ALL ON FUNCTION "setAnswerCorrection"(correction boolean, answer_id integer, marks_obtained integer) TO developer;


--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 377
-- Name: setTimetable(integer, integer, integer, integer, text, text, text, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) FROM developer;
GRANT ALL ON FUNCTION "setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) TO developer;
GRANT ALL ON FUNCTION "setTimetable"(dept integer, sem integer, batch integer, time_slot integer, day text, subject_name text, faculty_name text, venue_name text) TO PUBLIC;


--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 299
-- Name: studentID(bigint, integer, text, text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) FROM PUBLIC;
REVOKE ALL ON FUNCTION "studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) FROM developer;
GRANT ALL ON FUNCTION "studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) TO developer;
GRANT ALL ON FUNCTION "studentID"(erp_id bigint, semester integer, student_id text, course text, branch text, batch text, registration_date timestamp without time zone) TO PUBLIC;


--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 371
-- Name: updateFeeBreakup(integer, integer, text, json); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) FROM PUBLIC;
REVOKE ALL ON FUNCTION "updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) FROM developer;
GRANT ALL ON FUNCTION "updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) TO developer;
GRANT ALL ON FUNCTION "updateFeeBreakup"(bat integer, sem integer, cat text, brkup json) TO PUBLIC;


--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 279
-- Name: updateLastSeen(bigint); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "updateLastSeen"(user_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "updateLastSeen"(user_id bigint) FROM postgres;
GRANT ALL ON FUNCTION "updateLastSeen"(user_id bigint) TO postgres;
GRANT ALL ON FUNCTION "updateLastSeen"(user_id bigint) TO PUBLIC;
GRANT ALL ON FUNCTION "updateLastSeen"(user_id bigint) TO developer;


--
-- TOC entry 2661 (class 0 OID 0)
-- Dependencies: 276
-- Name: updateVercode(bigint, text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "updateVercode"(user_id bigint, ver_code text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "updateVercode"(user_id bigint, ver_code text) FROM developer;
GRANT ALL ON FUNCTION "updateVercode"(user_id bigint, ver_code text) TO developer;
GRANT ALL ON FUNCTION "updateVercode"(user_id bigint, ver_code text) TO PUBLIC;


--
-- TOC entry 2662 (class 0 OID 0)
-- Dependencies: 341
-- Name: updateVerificationStatus(integer, bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "updateVerificationStatus"(status_code integer, rid bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "updateVerificationStatus"(status_code integer, rid bigint) FROM developer;
GRANT ALL ON FUNCTION "updateVerificationStatus"(status_code integer, rid bigint) TO developer;
GRANT ALL ON FUNCTION "updateVerificationStatus"(status_code integer, rid bigint) TO PUBLIC;


--
-- TOC entry 2663 (class 0 OID 0)
-- Dependencies: 372
-- Name: updateVerified(bigint); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "updateVerified"(reg_id bigint) FROM PUBLIC;
REVOKE ALL ON FUNCTION "updateVerified"(reg_id bigint) FROM developer;
GRANT ALL ON FUNCTION "updateVerified"(reg_id bigint) TO developer;
GRANT ALL ON FUNCTION "updateVerified"(reg_id bigint) TO PUBLIC;


--
-- TOC entry 2664 (class 0 OID 0)
-- Dependencies: 376
-- Name: useUserNameGeneration(text); Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON FUNCTION "useUserNameGeneration"(uname text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "useUserNameGeneration"(uname text) FROM developer;
GRANT ALL ON FUNCTION "useUserNameGeneration"(uname text) TO developer;
GRANT ALL ON FUNCTION "useUserNameGeneration"(uname text) TO PUBLIC;


--
-- TOC entry 2665 (class 0 OID 0)
-- Dependencies: 389
-- Name: verifyForgotPassword(text, text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION "verifyForgotPassword"(user_name text, ver_code text) FROM PUBLIC;
REVOKE ALL ON FUNCTION "verifyForgotPassword"(user_name text, ver_code text) FROM postgres;
GRANT ALL ON FUNCTION "verifyForgotPassword"(user_name text, ver_code text) TO postgres;
GRANT ALL ON FUNCTION "verifyForgotPassword"(user_name text, ver_code text) TO PUBLIC;
GRANT ALL ON FUNCTION "verifyForgotPassword"(user_name text, ver_code text) TO developer;


--
-- TOC entry 2666 (class 0 OID 0)
-- Dependencies: 197
-- Name: attendance_register; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE attendance_register FROM PUBLIC;
REVOKE ALL ON TABLE attendance_register FROM developer;
GRANT ALL ON TABLE attendance_register TO developer;


--
-- TOC entry 2667 (class 0 OID 0)
-- Dependencies: 214
-- Name: cat; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE cat FROM PUBLIC;
REVOKE ALL ON TABLE cat FROM postgres;
GRANT ALL ON TABLE cat TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE cat TO developer;


--
-- TOC entry 2668 (class 0 OID 0)
-- Dependencies: 185
-- Name: chat; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE chat FROM PUBLIC;
REVOKE ALL ON TABLE chat FROM postgres;
GRANT ALL ON TABLE chat TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE chat TO developer;


--
-- TOC entry 2670 (class 0 OID 0)
-- Dependencies: 186
-- Name: chat_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE chat_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE chat_id_seq FROM postgres;
GRANT ALL ON SEQUENCE chat_id_seq TO postgres;
GRANT ALL ON SEQUENCE chat_id_seq TO developer;


--
-- TOC entry 2671 (class 0 OID 0)
-- Dependencies: 191
-- Name: class_attendance_info; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE class_attendance_info FROM PUBLIC;
REVOKE ALL ON TABLE class_attendance_info FROM developer;
GRANT ALL ON TABLE class_attendance_info TO developer;


--
-- TOC entry 2673 (class 0 OID 0)
-- Dependencies: 227
-- Name: comments_comment_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE comments_comment_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE comments_comment_id_seq FROM developer;
GRANT ALL ON SEQUENCE comments_comment_id_seq TO developer;


--
-- TOC entry 2674 (class 0 OID 0)
-- Dependencies: 181
-- Name: conversations; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE conversations FROM PUBLIC;
REVOKE ALL ON TABLE conversations FROM postgres;
GRANT ALL ON TABLE conversations TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE conversations TO developer;


--
-- TOC entry 2676 (class 0 OID 0)
-- Dependencies: 184
-- Name: conversations_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE conversations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE conversations_id_seq FROM postgres;
GRANT ALL ON SEQUENCE conversations_id_seq TO postgres;
GRANT ALL ON SEQUENCE conversations_id_seq TO developer;


--
-- TOC entry 2677 (class 0 OID 0)
-- Dependencies: 232
-- Name: course_allocation; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE course_allocation FROM PUBLIC;
REVOKE ALL ON TABLE course_allocation FROM developer;
GRANT ALL ON TABLE course_allocation TO developer;


--
-- TOC entry 2678 (class 0 OID 0)
-- Dependencies: 207
-- Name: course_registration; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE course_registration FROM PUBLIC;
REVOKE ALL ON TABLE course_registration FROM developer;
GRANT ALL ON TABLE course_registration TO developer;


--
-- TOC entry 2680 (class 0 OID 0)
-- Dependencies: 202
-- Name: csab_student_list_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE csab_student_list_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE csab_student_list_id_seq FROM postgres;
GRANT ALL ON SEQUENCE csab_student_list_id_seq TO postgres;
GRANT ALL ON SEQUENCE csab_student_list_id_seq TO developer;


--
-- TOC entry 2681 (class 0 OID 0)
-- Dependencies: 208
-- Name: department; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE department FROM PUBLIC;
REVOKE ALL ON TABLE department FROM developer;
GRANT ALL ON TABLE department TO developer;


--
-- TOC entry 2683 (class 0 OID 0)
-- Dependencies: 213
-- Name: dt; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE dt FROM PUBLIC;
REVOKE ALL ON TABLE dt FROM developer;
GRANT ALL ON TABLE dt TO developer;


--
-- TOC entry 2684 (class 0 OID 0)
-- Dependencies: 206
-- Name: faculty; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE faculty FROM PUBLIC;
REVOKE ALL ON TABLE faculty FROM postgres;
GRANT ALL ON TABLE faculty TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE faculty TO developer;


--
-- TOC entry 2685 (class 0 OID 0)
-- Dependencies: 196
-- Name: faculty_alloted_sub; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE faculty_alloted_sub FROM PUBLIC;
REVOKE ALL ON TABLE faculty_alloted_sub FROM developer;
GRANT ALL ON TABLE faculty_alloted_sub TO developer;


--
-- TOC entry 2687 (class 0 OID 0)
-- Dependencies: 200
-- Name: fee_payment_ref_no_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE fee_payment_ref_no_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fee_payment_ref_no_seq FROM developer;
GRANT ALL ON SEQUENCE fee_payment_ref_no_seq TO developer;


--
-- TOC entry 2688 (class 0 OID 0)
-- Dependencies: 211
-- Name: fee_transaction; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE fee_transaction FROM PUBLIC;
REVOKE ALL ON TABLE fee_transaction FROM developer;
GRANT ALL ON TABLE fee_transaction TO developer;


--
-- TOC entry 2690 (class 0 OID 0)
-- Dependencies: 216
-- Name: files_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE files_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE files_id_seq FROM developer;
GRANT ALL ON SEQUENCE files_id_seq TO developer;


--
-- TOC entry 2691 (class 0 OID 0)
-- Dependencies: 194
-- Name: forms; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE forms FROM PUBLIC;
REVOKE ALL ON TABLE forms FROM developer;
GRANT ALL ON TABLE forms TO developer;


--
-- TOC entry 2693 (class 0 OID 0)
-- Dependencies: 225
-- Name: forum_category_category_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE forum_category_category_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE forum_category_category_id_seq FROM developer;
GRANT ALL ON SEQUENCE forum_category_category_id_seq TO developer;


--
-- TOC entry 2694 (class 0 OID 0)
-- Dependencies: 209
-- Name: groups; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE groups FROM PUBLIC;
REVOKE ALL ON TABLE groups FROM postgres;
GRANT ALL ON TABLE groups TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE groups TO developer;


--
-- TOC entry 2696 (class 0 OID 0)
-- Dependencies: 210
-- Name: groups_group_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE groups_group_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE groups_group_id_seq FROM postgres;
GRANT ALL ON SEQUENCE groups_group_id_seq TO postgres;
GRANT ALL ON SEQUENCE groups_group_id_seq TO developer;


--
-- TOC entry 2697 (class 0 OID 0)
-- Dependencies: 182
-- Name: messages; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE messages FROM PUBLIC;
REVOKE ALL ON TABLE messages FROM postgres;
GRANT ALL ON TABLE messages TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE messages TO developer;


--
-- TOC entry 2699 (class 0 OID 0)
-- Dependencies: 187
-- Name: messages_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE messages_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE messages_id_seq FROM postgres;
GRANT ALL ON SEQUENCE messages_id_seq TO postgres;
GRANT ALL ON SEQUENCE messages_id_seq TO developer;


--
-- TOC entry 2701 (class 0 OID 0)
-- Dependencies: 188
-- Name: notifications_notif_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE notifications_notif_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE notifications_notif_id_seq FROM developer;
GRANT ALL ON SEQUENCE notifications_notif_id_seq TO developer;


--
-- TOC entry 2702 (class 0 OID 0)
-- Dependencies: 205
-- Name: office; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE office FROM PUBLIC;
REVOKE ALL ON TABLE office FROM postgres;
GRANT ALL ON TABLE office TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE office TO developer;


--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 222
-- Name: posts_post_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE posts_post_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE posts_post_id_seq FROM developer;
GRANT ALL ON SEQUENCE posts_post_id_seq TO developer;


--
-- TOC entry 2705 (class 0 OID 0)
-- Dependencies: 234
-- Name: registration_fee_transaction; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE registration_fee_transaction FROM PUBLIC;
REVOKE ALL ON TABLE registration_fee_transaction FROM postgres;
GRANT ALL ON TABLE registration_fee_transaction TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE registration_fee_transaction TO developer;


--
-- TOC entry 2707 (class 0 OID 0)
-- Dependencies: 198
-- Name: registration_student_list_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE registration_student_list_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE registration_student_list_id_seq FROM developer;
GRANT ALL ON SEQUENCE registration_student_list_id_seq TO developer;
GRANT ALL ON SEQUENCE registration_student_list_id_seq TO PUBLIC;


--
-- TOC entry 2708 (class 0 OID 0)
-- Dependencies: 212
-- Name: sem; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE sem FROM PUBLIC;
REVOKE ALL ON TABLE sem FROM developer;
GRANT ALL ON TABLE sem TO developer;


--
-- TOC entry 2709 (class 0 OID 0)
-- Dependencies: 204
-- Name: student; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE student FROM PUBLIC;
REVOKE ALL ON TABLE student FROM postgres;
GRANT ALL ON TABLE student TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE student TO developer;


--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 195
-- Name: student_alloted_sub; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE student_alloted_sub FROM PUBLIC;
REVOKE ALL ON TABLE student_alloted_sub FROM developer;
GRANT ALL ON TABLE student_alloted_sub TO developer;


--
-- TOC entry 2711 (class 0 OID 0)
-- Dependencies: 228
-- Name: tags; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE tags FROM PUBLIC;
REVOKE ALL ON TABLE tags FROM postgres;
GRANT ALL ON TABLE tags TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tags TO developer;


--
-- TOC entry 2713 (class 0 OID 0)
-- Dependencies: 229
-- Name: tags_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE tags_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tags_id_seq FROM postgres;
GRANT ALL ON SEQUENCE tags_id_seq TO postgres;
GRANT ALL ON SEQUENCE tags_id_seq TO developer;


--
-- TOC entry 2715 (class 0 OID 0)
-- Dependencies: 231
-- Name: templates_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE templates_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE templates_id_seq FROM postgres;
GRANT ALL ON SEQUENCE templates_id_seq TO postgres;
GRANT ALL ON SEQUENCE templates_id_seq TO developer;


--
-- TOC entry 2716 (class 0 OID 0)
-- Dependencies: 173
-- Name: test_answerSheet; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "test_answerSheet" FROM PUBLIC;
REVOKE ALL ON TABLE "test_answerSheet" FROM postgres;
GRANT ALL ON TABLE "test_answerSheet" TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "test_answerSheet" TO developer;


--
-- TOC entry 2718 (class 0 OID 0)
-- Dependencies: 174
-- Name: test_answerSheet_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE "test_answerSheet_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "test_answerSheet_id_seq" FROM postgres;
GRANT ALL ON SEQUENCE "test_answerSheet_id_seq" TO postgres;
GRANT ALL ON SEQUENCE "test_answerSheet_id_seq" TO developer;


--
-- TOC entry 2720 (class 0 OID 0)
-- Dependencies: 175
-- Name: test_answer_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE test_answer_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE test_answer_id_seq FROM postgres;
GRANT ALL ON SEQUENCE test_answer_id_seq TO postgres;
GRANT ALL ON SEQUENCE test_answer_id_seq TO developer;


--
-- TOC entry 2722 (class 0 OID 0)
-- Dependencies: 177
-- Name: test_question_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE test_question_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE test_question_id_seq FROM postgres;
GRANT ALL ON SEQUENCE test_question_id_seq TO postgres;
GRANT ALL ON SEQUENCE test_question_id_seq TO developer;


--
-- TOC entry 2723 (class 0 OID 0)
-- Dependencies: 178
-- Name: test_testPaper; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "test_testPaper" FROM PUBLIC;
REVOKE ALL ON TABLE "test_testPaper" FROM postgres;
GRANT ALL ON TABLE "test_testPaper" TO postgres;
GRANT ALL ON TABLE "test_testPaper" TO developer;


--
-- TOC entry 2725 (class 0 OID 0)
-- Dependencies: 179
-- Name: test_testPaper_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE "test_testPaper_id_seq" FROM PUBLIC;
REVOKE ALL ON SEQUENCE "test_testPaper_id_seq" FROM postgres;
GRANT ALL ON SEQUENCE "test_testPaper_id_seq" TO postgres;
GRANT ALL ON SEQUENCE "test_testPaper_id_seq" TO developer;


--
-- TOC entry 2727 (class 0 OID 0)
-- Dependencies: 220
-- Name: threads_thread_id_seq; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON SEQUENCE threads_thread_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE threads_thread_id_seq FROM developer;
GRANT ALL ON SEQUENCE threads_thread_id_seq TO developer;


--
-- TOC entry 2728 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_name_generation; Type: ACL; Schema: public; Owner: developer
--

REVOKE ALL ON TABLE user_name_generation FROM PUBLIC;
REVOKE ALL ON TABLE user_name_generation FROM developer;
GRANT ALL ON TABLE user_name_generation TO developer;


--
-- TOC entry 2730 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE users_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE users_id_seq FROM postgres;
GRANT ALL ON SEQUENCE users_id_seq TO postgres;
GRANT ALL ON SEQUENCE users_id_seq TO developer;


-- Completed on 2016-06-05 12:58:40

--
-- PostgreSQL database dump complete
--

