/**
 * 
 */
package postgreSQLDatabase.feePayment;

import org.json.JSONObject;

/**
 * @author Shubhi
 *
 */
public class Payment {
	private Long ref_no;
	private String comment;
	private JSONObject details;
	private Long amount;
	private boolean verified;
	private String payment_method;
	private long transaction_id;
	/**
	 * @return the transaction_id
	 */
	public long getTransaction_id() {
		return transaction_id;
	}

	/**
	 * @param transaction_id the transaction_id to set
	 */
	public void setTransaction_id(long transaction_id) {
		this.transaction_id = transaction_id;
	}

	public Long getRef_no() {
		return ref_no;
	}

	public void setRef_no(Long ref_no) {
		this.ref_no = ref_no;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public JSONObject getDetails() {
		return details;
	}

	public void setDetails(JSONObject details) {
		this.details = details;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(int payment_method_code) {
		switch (payment_method_code) {
		
		case 1:
			this.payment_method = "DD";
			break;
		case 2:
			this.payment_method = "NET BANKING";
			break;
		case 3:
			this.payment_method = "NEFT";
			break;
		case 4:
			this.payment_method="CHEQUE";
			break;
		case 5:
			this.payment_method="CHALLAN";
			break;
		case 6:
			this.payment_method = "CASH";
			break;
		default:
			this.payment_method="";
			break;
		}
		
	}

}
