/**
 * 
 */
package postgreSQLDatabase.templates;

import java.sql.Date;
import java.util.ArrayList;

/**
 * @author manisha pc
 *
 */
public class Template {
	private long id;
	private Long file;
	private ArrayList<String> tags;
	private String title;
	private long author;
	private Date timestamp;
	private postgreSQLDatabase.file.File file_wrapper;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the tags
	 */
	public ArrayList<String> getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the author
	 */
	public long getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(long author) {
		this.author = author;
	}



	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	public Long getFile() {
		return file;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(Long file) {
		this.file = file;
	}
	/**
	 * 
	 */
	public Template() {
		// TODO Auto-generated constructor stub
	}
	public postgreSQLDatabase.file.File getFile_wrapper() {
		return file_wrapper;
	}
	
	public String getFilePath() {
		return file_wrapper.getDirectory()+"/"+file_wrapper.getFile_name();
	}
	/**
	 * @param file the file to set
	 */
	public void setFile_wrapper(postgreSQLDatabase.file.File file_wrapper) {
		this.file_wrapper = file_wrapper;
	}
}
