/**
 * 
 */
package utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

/**
 * @author Arushi
 *
 */
public class AddFileHandler {

	/**
	 * @param args
	 */
	SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
	public static FileHandler filehandler = null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public  AddFileHandler(){
		try {  

	        // This block configure the logger with handler and formatter  
			filehandler = new FileHandler("C:/Temp/MyLogFile_"+ format.format(Calendar.getInstance().getTime()) + ".txt"); 
	        //logger.setUseParentHandlers(false);
	        
	        SimpleFormatter formatter = new SimpleFormatter();  
	        filehandler.setFormatter(formatter);  

	        // the following statement is used to log any messages  
	       // logger.info("My first log");  

	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
	}
	
}
