/**
 * 
 */
package utilities;

import org.apache.commons.lang.RandomStringUtils;

/**
 * @author Shubhi
 *
 */
public class Random {
public static void main(String[] args) {
	generateRandomString(10);
}
public static String generateRandomString(int length){
	String random=RandomStringUtils.randomAlphanumeric(length);
	//System.out.println(random);
	return random;
}
}
