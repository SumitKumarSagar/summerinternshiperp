/**
 * 
 */
package quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author Joey
 *
 */
public class HelloJob implements Job{
	

	/* (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
	String data= 	(String) context.getMergedJobDataMap().get("data");
		System.out.println(data);
	}
}
