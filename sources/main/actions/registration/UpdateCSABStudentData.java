package actions.registration;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import exceptions.IncorrectFormatException;
import users.Student;

/**
 * Servlet implementation class UpdateCSABStudentData
 */
@WebServlet("/UpdateCSABStudentData")
@MultipartConfig
public class UpdateCSABStudentData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateCSABStudentData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//		System.out.println("X"+request.getParameter("action").equals("update")+"X");
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> formItems=null;
		PrintWriter writer=response.getWriter();
		try {
			formItems = upload.parseRequest(request);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(request.getParameter("action").equals("update")){
			System.out.println("reached");


			Student student=new Student();

		

				System.out.println("size"+formItems.size());
				Iterator<FileItem> iter = formItems.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (item.isFormField()) {
						String field;
						try {
						switch(item.getFieldName()){

						case "csab_id": 
							System.out.println((Long.parseLong(item.getString())));
							student.setCsab_id(Long.parseLong(item.getString()));
							break;
						case "name":   student.setName(item.getString());break;
						case "first_name":student.setFirst_name(item.getString());break;
						case "middle_name":student.setMiddle_name(item.getString());break;
						case "last_name":student.setLast_name(item.getString());break;
						case "category":student.setCategory(item.getString());break;
						case "jee_main_rollno":student.setJee_main_rollno((item.getString()));break;
						case "jee_advance_rollno":student.setJee_adv_rollno((item.getString()));break;
						case "state":student.setState_eligibility(item.getString());break;
						case "phone_number":student.setMobile(item.getString());break;
						case "email":student.setEmail(item.getString());break;
						case "date_of_birth" : student.setDate_of_birth(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));					break;
						case "program_allocated":student.setProgram_allocated(item.getString());break;
						case "allocated_category":student.setAllocated_category(item.getString());break;
						case "allocated_rank": student.setAllocated_rank(item.getString());break;
						case "status":student.setStatus(item.getString());break;
						case "choice_number":student.setChoice_no(Integer.parseInt(item.getString()));break;
						case "pwd":student.setPwd(item.getString());break;
						case "gender":student.setGender(item.getString());break;
						case "quota":student.setQuota(item.getString());break;
						case "round":student.setRound(Integer.parseInt(item.getString()));break;
						case "willingness":student.setWillingness(item.getString());break;
						case "address":student.setPermanent_address(item.getString());break;
						case "rc_name":student.setRc_name(item.getString());break;
						case "nationality":student.setNationality(item.getString());break;
						case "entry_date" :student.setEntry_date(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));break;
						case "reported":student.setReported(Boolean.parseBoolean(item.getString()));break;
						}
						
					}
					 catch (IncorrectFormatException | ParseException |  NumberFormatException ex) {
						response.getWriter().write( "There was an error: " + ex.getMessage());
						ex.printStackTrace();
					}
				}
				}
			
			postgreSQLDatabase.registration.Query.updateCSABStudentData(student);

		}

		// To Add A Student 

		if(request.getParameter("action").equals("add")){			
			Student student=new Student();
			Iterator<FileItem> iter = formItems.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					String field;
					try {
					switch(item.getFieldName()){

					case "name":  System.out.println("name is"+item.getString()); student.setName(item.getString());break;
					case "first_name":student.setFirst_name(item.getString());break;
					case "middle_name":student.setMiddle_name(item.getString());break;
					case "last_name":student.setLast_name(item.getString());break;
					case "category":student.setCategory(item.getString());break;
					case "jee_main_rollno":student.setJee_main_rollno((item.getString()));break;
					case "jee_advance_rollno":student.setJee_adv_rollno((item.getString()));break;
					case "state":student.setState_eligibility(item.getString());break;
					case "phone_number":student.setMobile(item.getString());break;
					case "email":student.setEmail(item.getString());break;
					case "date_of_birth" : student.setDate_of_birth(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));					break;
					case "program_allocated":student.setProgram_allocated(item.getString());break;
					case "allocated_category":student.setAllocated_category(item.getString());break;
					case "allocated_rank": student.setAllocated_rank(item.getString());break;
					case "status":student.setStatus(item.getString());break;
					case "choice_number":student.setChoice_no(Integer.parseInt(item.getString()));break;
					case "pwd":student.setPwd(item.getString());break;
					case "gender":student.setGender(item.getString());break;
					case "quota":student.setQuota(item.getString());break;
					case "round":student.setRound(Integer.parseInt(item.getString()));break;
					case "willingness":student.setWillingness(item.getString());break;
					case "address":student.setPermanent_address(item.getString());break;
					case "rc_name":student.setRc_name(item.getString());break;
					case "nationality":student.setNationality(item.getString());break;
					case "entry_date" :student.setEntry_date(new SimpleDateFormat("yyyy-MM-dd").parse(item.getString()));break;
					case "reported":student.setReported(Boolean.parseBoolean(item.getString()));break;
					}
					} catch (IncorrectFormatException | ParseException  | NumberFormatException ex) {
						//	request.setAttribute("message", "There was an error: " + ex.getMessage());
							//ex.printStackTrace();
						writer.append(ex.getMessage());
						}
				}
			}
			
			postgreSQLDatabase.registration.Query.addCSABStudentData(student);

		}

	}
}
