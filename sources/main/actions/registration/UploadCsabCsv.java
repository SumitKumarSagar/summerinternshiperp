package actions.registration;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import users.Student;
import csv.Parser;
import exceptions.IncorrectFormatException;

/**
 * Servlet implementation class UploadCsabCsv
 */
@WebServlet("/UploadCsabCsv")
public class UploadCsabCsv extends HttpServlet {
	private static final long serialsionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadCsabCsv() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer=response.getWriter();
		int skip_start=Integer.parseInt(request.getParameter("skip_start"));
		String header[]=request.getParameter("array").split(",");
		String file_name=request.getServletContext().getRealPath("")+File.separator+"upload"+File.separator+request.getParameter("filename");
		
		
		
		Boolean consistent =true;
		Boolean validate=true;
		Boolean success=false;
		JSONObject message=new JSONObject();
		JSONArray errors=new JSONArray();
		
		Parser csv=new Parser(file_name);
		ArrayList<ArrayList<String>> list = csv.getArray();
		ArrayList<Student> student_list=new ArrayList<Student>();
		for(int j=skip_start;j<csv.getNumRows();j++){
			Student current =new Student();
			for(int i=0;i<csv.getNumCols();i++){
				try {
					switch(header[i]){
					case "name":current.setName(csv.get(j, i));break;
					case "first_name":current.setFirst_name(csv.get(j, i));break;
					case "middle_name":current.setMiddle_name(csv.get(j, i));break;
					case "last_name":current.setLast_name(csv.get(j, i));break;
					case "category":current.setCategory(csv.get(j, i));break;
					case "jee_main_rollno":current.setJee_main_rollno(csv.get(j, i));break;
					case "jee_adv_rollno":current.setJee_adv_rollno((csv.get(j, i)));break;
					case "state":current.setState_eligibility(csv.get(j, i));break;
					case "phone_number":current.setMobile(csv.get(j, i));break;
					case "email":current.setEmail(csv.get(j, i));break;
					case "date_of_birth":current.setDate_of_birth(csv.get(j, i));break;
					case "program_allocated":current.setProgram_allocated(csv.get(j, i));break;
					case "allocated_category":current.setAllocated_category(csv.get(j, i));break;
					case "allocated_rank":current.setAllocated_rank((csv.get(j, i)));break;
					case "status":current.setStatus(csv.get(j, i));break;
					case "choice_no":current.setChoice_no(csv.get(j, i));break;
					case "physically_disabled":current.setPwd(csv.get(j, i));break;
					case "gender":current.setGender(csv.get(j, i));break;
					case "quota":current.setQuota(csv.get(j, i));break;
					case "round":current.setRound(csv.get(j, i));break;
					case "willingness":current.setWillingness(csv.get(j, i));break;
					case "address":current.setPermanent_address(csv.get(j, i));break;
					case "rc_name":current.setRc_name(csv.get(j, i));break;
					case "nationality":current.setNationality(csv.get(j, i));break;
					}
				} catch (IncorrectFormatException e) {
					//e.printStackTrace();
					validate=false;
					JSONObject bug=new JSONObject();
					bug.put("row",j);
					bug.put("column", i);
					bug.put("message", e.getMessage());
					errors.put(bug);
				}
				catch (IndexOutOfBoundsException e) {
					//e.printStackTrace();
					consistent=false;
					JSONObject bug=new JSONObject();
					bug.put("row",j);
					bug.put("column", i);
					bug.put("message", "inconsistent_row");
					errors.put(bug);
				}
				if(!consistent)break;
			}
			if(!consistent)break;
			student_list.add(current);
			
		}
		System.out.println("size"+student_list.size());
		if(consistent && validate)
		{
		PreparedStatement proc;
		
		try {
			proc = settings.database.PostgreSQLConnection.getConnection().prepareCall("SELECT \"addCSABData\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
			Iterator<Student> iterator=student_list.iterator();
			while(iterator.hasNext()){
				Student current = iterator.next();
				proc.setString(1, current.getName()); //1 is the first ? (1 based counting)
				proc.setString(2, current.getFirst_name());
				proc.setString(3, current.getMiddle_name());
				proc.setString(4, current.getLast_name());
				proc.setString(5,current.getCategory());
				proc.setInt(6,current.getJee_main_rollno());
				proc.setInt(7,current.getJee_adv_rollno());
				proc.setString(8,current.getState_eligibility());
				proc.setString(9,current.getMobile());
				proc.setString(10,current.getEmail());
				proc.setDate(11,utilities.StringFormatter.convert(new java.util.Date()));
				proc.setString(12,current.getProgram_allocated()); 
				proc.setString(13,current.getAllocated_category());
				proc.setString(14,current.getAllocated_rank());
				proc.setString(15,current.getStatus());
				proc.setInt(16,current.getChoice_no());
				proc.setBoolean(17,current.isPwd());
				proc.setString(18,current.getGender());
				proc.setString(19,current.getQuota());
				proc.setInt(20,current.getRound());
				proc.setString(21,current.getWillingness());
				proc.setString(22,current.getPermanent_address());
				proc.setString(23,current.getRc_name());
				proc.setString(24,current.getNationality());
				proc.addBatch();
			}

			System.out.println(proc.toString());
			
			  proc.executeBatch();
			  message.put("success", true);
			  writer.write("Uploaded Successfully");


		

		} catch (SQLException e) {
		
			// TODO Auto-generated catch block
			System.out.println(e.getNextException().getSQLState());
			if(e.getNextException().getSQLState().equals(String.valueOf(23505))){
				
				errors.put("Duplicate enteries for column JEE Mains roll number");
				
			}
			else if(e.getNextException().getSQLState().equals("0100E")){
				success=true;
				
			}
			else{
				message.put("success",false);
				errors.put("unknown");
			}
		}
		
		}

		message.put("success", success);
		
		message.put("message", errors);
		
              writer.write(message.toString());
	}

}


