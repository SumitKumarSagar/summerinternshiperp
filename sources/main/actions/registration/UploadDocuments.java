package actions.registration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;


import postgreSQLDatabase.registration.Query;

@WebServlet("/UploadDocuments")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,

		maxFileSize = 1024 * 1024 * 50, maxRequestSize = 1024 * 1024 * 100)
public class UploadDocuments extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String UPLOAD_DIRECTORY = "/uploads";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().write("hello");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		List<FileItem> formItems = null;
		Long csab_id = null;
		JSONArray details = null;
		Long reg_id;
		try {

			formItems = upload.parseRequest(request);

			Iterator<FileItem> iter = formItems.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();

				if (item.isFormField()) {

					try {
						switch (item.getFieldName()) {
						case "csab_id":
							csab_id = Long.parseLong(item.getString());
							break;
						case "details":
							details = new JSONArray(item.getString());
							// System.out.println("XX"+item.getString());
							break;
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				} else
					System.out.println(item.getFieldName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Report Student
	 reg_id = Query.reportStudent(csab_id);
		String filePath = "uploads" + "/" + "student_" + reg_id;
		ftp.FTPUpload.createDirectory("uploads");
		ftp.FTPUpload.createDirectory(filePath);

		// Document Upload

		File fileSaveDir = new File(filePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}

		for (int i = 0; i < details.length(); i++) {
			JSONObject current = details.getJSONObject(i);
			String type = current.getString("type");
			String file = current.getString("file");
			Iterator<FileItem> iterator = formItems.iterator();
			while (iterator.hasNext()) {
				FileItem file_item = iterator.next();
				if (type.equals(file_item.getFieldName())) {
					InputStream inputStream = file_item.getInputStream();
					postgreSQLDatabase.file.File file_wrapper = new postgreSQLDatabase.file.File();
					file_wrapper.setDirectory("uploads/student_"+reg_id);
					file_wrapper.setAuthor(Long.parseLong((request.getSession().getAttribute("erpId")).toString()));
					String name[] = file.split("\\.");

					file_wrapper.setExtension(name[1]);

					file_wrapper.setFile_name(file);
					
				
					try {
						// add reference to database
						long file_id = postgreSQLDatabase.file.Query.addNewFile(file_wrapper);
						
						postgreSQLDatabase.documents.Documents document = new postgreSQLDatabase.documents.Documents();
						document.setOwner_id(reg_id);
						document.setFile_id(file_id);
						document.setDocument_name(type);
						postgreSQLDatabase.documents.Query.addDocument(document);

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//Upoad to FTP
					ftp.FTPUpload.uploadFile(inputStream, filePath + "/" + file);
					break;
				}
			}

		}
		
JSONObject j_object=new JSONObject();
j_object.put("reg_id",reg_id );
response.getWriter().write(j_object.toString());
}
}
