package actions.templates;
import postgreSQLDatabase.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import postgreSQLDatabase.templates.Tags;

/**
 * Servlet implementation class GetAllTags
 */
@WebServlet("/GetAllTags")
public class GetAllTags extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAllTags() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
		ArrayList<Tags> tags=postgreSQLDatabase.templates.Query.getAllTags();
		Iterator<Tags> iterator=tags.iterator();
		JSONArray all_tags=new JSONArray();
		while(iterator.hasNext()){
		Tags current = iterator.next();
			JSONObject j_object=new JSONObject();
			j_object.put("tagname",current.getTagname());
			j_object.put("attributes",current.getAttributes());
			j_object.put("users",current.getUsers());
			all_tags.put(j_object);
		}
		writer.write(all_tags.toString());
	}

}
