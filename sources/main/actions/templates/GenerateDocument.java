package actions.templates;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.xhtmlrenderer.pdf.ITextRenderer;

import exceptions.IncorrectFormatException;
import users.Student;

/**
 * Servlet implementation class NextServlet
 */
@WebServlet("/GenerateDocument")
public class GenerateDocument extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenerateDocument() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	//	PrintWriter writer = response.getWriter();
		// TODO Auto-generated method stub
		String student_id = request.getParameter("student_id");

		String[] split_id = student_id.split(",");
		ArrayList<Student> list = new ArrayList<Student>();
		for (int i = 0; i < split_id.length; i++) {
			Student current = new Student();
			current = postgreSQLDatabase.templates.Query.getStudentByStudentId(split_id[i]);
			list.add(current);
		}

		String id = request.getParameter("file_id");
		long file_id = Long.parseLong(id);

		postgreSQLDatabase.file.File file = postgreSQLDatabase.file.Query.getAllFileData(file_id);
		
		String file_name=file.getFile_name();
		String uploadFilePath = "templates";

		

		 String filePath = uploadFilePath + "/" +file_name;

		Iterator<Student> iterator = list.iterator();
		String new_string = "";
		while (iterator.hasNext()) {
			Student student = iterator.next();
			HashMap<String, String> map = student.getTagHashMap();
			
			map.put("{(date)}", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
			System.out.println(map.toString());
			
			String string = ftp.FTPUpload.readTextFile(filePath);

			TreeSet<String> tags = new TreeSet<String>(email.Mapping.findTags(string));
			System.out.println(tags);
			try{
			 new_string += "<p style=\"page-break-after:always;\" ></p>"+email.Mapping.mapStudentDetails(string, map)+"";
			}
			catch(Exception e){
				response.sendRedirect("Error.jsp?error=Data inconsistent");
				return;
			}
			
			
			
		}
		response.setContentType("application/pdf");
		
			
			
			try{

			ITextRenderer renderer = new ITextRenderer();
			System.out.println(new_string);
			String HTML="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
					+ "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>IIITK ERP</title><style>"
+"@page { size: A4;margin: 1cm 1cm 2cm 1cm;}"
//+"@media print { footer {page-break-after: always;}}"
+"</style>"
+"</head>"
+"<body>";
			renderer.setDocumentFromString(HTML+new_string+"</body></html>");
			ServletOutputStream os = response.getOutputStream();
			renderer.layout();
		
				renderer.createPDF(os);
			} catch (Exception e) {
				e.printStackTrace();
			//	response.getWriter().write("Sorry there was an error");
			}

	}

	

}
