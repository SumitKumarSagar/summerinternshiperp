package actions.templates;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import exceptions.TagNotFoundException;
import postgreSQLDatabase.templates.Tags;
import postgreSQLDatabase.templates.Template;

/**
 * Servlet implementation class UpdateTemplates
 */
@WebServlet("/UpdateTemplates")
public class UpdateTemplates extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateTemplates() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				HttpSession session = request.getSession();
				DiskFileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List<FileItem> formItems = null;
					try {
						formItems = upload.parseRequest(request);
					} catch (FileUploadException e4) {
						// TODO Auto-generated catch block
						e4.printStackTrace();
					}
				
				System.out.println(formItems.size());
				String file = null;
				Long template_id = null;
				Iterator<FileItem> iter = formItems.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (item.isFormField()) {

						try{
							switch(item.getFieldName()){


							case "template_id" :
							template_id=Long.parseLong(item.getString());
								
								break;
							case "file":
								file=item.getString();
							}
						}
						catch(Exception e){
							System.out.println(e.getMessage());
							
						}	}
				
					}
				
				System.out.println(template_id);
				System.out.println(file);
				
				
				
				
				
				
				if (file != null) {
					Template template = postgreSQLDatabase.templates.Query.getTemplates(template_id);

					String uploadFilePath = template.getFile_wrapper().getDirectory();
					String file_name= template.getFile_wrapper().getFile_name();
					// System.out.println(uploadFilePath);
					ftp.FTPUpload.createDirectory(uploadFilePath);
					String filePath = uploadFilePath +"/" + file_name;
					ByteArrayInputStream upload_stream = new ByteArrayInputStream(file.getBytes(StandardCharsets.UTF_8));
					ftp.FTPUpload.uploadFile(upload_stream,filePath);
					upload_stream.close();
						

		}
}
}