package actions.ftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.common.io.ByteStreams;

/**
 * Servlet implementation class FtpDownload
 */
@WebServlet("/FtpDownload")
public class FtpDownload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FtpDownload() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String file=request.getParameter("file");
		InputStream stream =  ftp.FTPUpload.downloadFile(request.getParameter("file"));

		try {
			String filename=file.substring(file.lastIndexOf("/")+1);

			response.setContentType("text/html");  
			 ServletOutputStream os = response.getOutputStream(); 
			response.setContentType("APPLICATION/OCTET-STREAM");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
			  
			byte[] buffer=new byte[20*1024];
			while(true) {
			  int readSize=stream.read(buffer);
			  if(readSize==-1)
			    break;
			os.write(buffer,0,readSize);
			}
			stream.close();  
			os.flush();
			os.close(); 
			
		}
		catch(Exception e){
			e.printStackTrace();
		}


	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub




	}


}
