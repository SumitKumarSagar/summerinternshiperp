package actions.chats;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import postgreSQLDatabase.chats.Conversation;
import settings.database.PostgreSQLConnection;

/**
 * Servlet implementation class RetrieveConversationsInfo
 */

@WebServlet(
		name="Retrieve Conversation info Servlet",
		urlPatterns={"/RetrieveConversationsInfo"}
	)
public class RetrieveConversationsInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveConversationsInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer=response.getWriter();
	
			Long user_id=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			
		try {
			ArrayList<Conversation> convos = postgreSQLDatabase.chats.Query.getConversationInfo(user_id);
			JSONArray convo_array=new JSONArray();
			Iterator<Conversation>convo_iterator=convos.iterator();
			while(convo_iterator.hasNext()){
				Conversation current=convo_iterator.next();
				JSONObject convo_object=new JSONObject();
				convo_object.put("chat_name",current.getChat_name());
				convo_object.put("conversation_id",current.getConversation_id());
				//System.out.println(current.getConversation_id());
				ArrayList<String>members=current.getMembers();
				JSONArray members_array=new JSONArray();
				Iterator<String> members_iterator = members.iterator();
				while(members_iterator.hasNext()){
					members_array.put(members_iterator.next());
				}
				convo_object.put("members", members_array);
				convo_array.put(convo_object);
			}
			writer.write(convo_array.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
}

