package actions.chats;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateNewConversation
 */
@WebServlet("/CreateNewConversation")
public class CreateNewConversation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateNewConversation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String users[]=request.getParameter("users").split(",");
		String chat_name="friends";
		Long users_id[]=new Long[users.length];
		int i=0;
		for(String name:users){
			users_id[i]=postgreSQLDatabase.authentication.Query.getUserId(name);
			System.out.println(users_id[i]);
			i++;
		}
		postgreSQLDatabase.chats.Query.createNewConversation(users_id, chat_name);
	}

}
