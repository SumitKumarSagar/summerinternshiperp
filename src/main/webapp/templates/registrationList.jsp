<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="users.Student"%>
    <%@page import="java.util.*"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
@page{
size:A4;
}
table, tr, td, th {border:1px solid black; border-collapse:collapse;}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Registration ID</th>
											<th>Name</th>
											<th>First Name</th>
											<th>Middle Name</th>
											<th>Last Name</th>
											<th>Category</th>
											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Date Of Birth</th>
											<th>Program Allocated</th>
											<th>Status</th>
											<th>Physically Disabled</th>
											<th>Gender</th>
											<th>Nationality</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Student> registration_list = Query.getStudentRegistrationList();
											Iterator<Student> iterator = registration_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
										%>
										<tr>
											<td><center><%=current.getRegistration_id()%></center></td>
											<td><center><%=current.getName()%></center></td>
											<td><center><%=current.getFirst_name()%></center></td>
											<td><center><%=current.getMiddle_name()%></center></td>
											<td><center><%=current.getLast_name()%></center></td>
											<td><center><%=current.getCategory()%></center></td>
                                            <td><center><%=current.getState_eligibility()%></center></td>
								            <td><center><%=current.getMobile()%></center></td>
											<td><center><%=current.getEmail()%></center></td>
											<td><center><%=current.getDate_of_birth()%></center></td>
										    <td><center><%=current.getProgram_allocated()%></center></td>
											<td><center><%=current.getStatus()%></center></td>
											<td><center><%=current.isPwd()%></center></td>
											<td><center><%=current.getGender()%></center></td>
											<td><center><%=current.getNationality()%></center></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						
</body>
<script type="text/javascript">
window.print();
</script>
</html>