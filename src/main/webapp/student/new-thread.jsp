<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="forum.ForumThread"%>
<%@page import="forum.Category"%>
<%@page import="postgreSQLDatabase.forum.Query"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>InTime</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="../plugins/select2/select2.min.css">		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			.example-modal .modal{
									position: relative;
									top: auto;
									bottom: auto;
									right: auto;
									left: auto;
									display: block;
									z-index: 1;
								 }

			.example-modal .modal {
									background: transparent !important;
								  }
		</style>
		
	</head>
	
	<body class="hold-transition skin-blue sidebar-mini">
	
		<div class="wrapper">
			<!--Header-->
			<%@ include file="header.jsp" %>
			
			<!-- Left side column. contains the logo and sidebar -->
			<%@ include file="main-sidebar.jsp" %>
  
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Main content -->
				<section class="content">
					<!--Post on timeline-->
					<div class="row">
						<div class="col-md-12">
						
						<ul class="timeline">
						<li>
							<img class="profile-user-img img-responsive img-circle pull-left" style="width:64px !important;height:64px !important;" src="../dist/img/user4-128x128.jpg" alt="User profile picture"> 

							<div class="example-modal">
								<div class="modal pull-left">
									<div class="modal-dialog">
									<form action="../AddNewThread" method="post">
										<div class="modal-content">
											<div class="modal-header">
												<div class="timeline-footer pull-right">
												</div>
												
												<h4 class="modal-title">
												<input type="text" class="form-control input-sm" 
                  placeholder="Enter new thread title" name="new_thread">
												</h4>
											</div>
											
											<div class="modal-body">
												<div class="form-group">
													<textarea type="text" class="form-control" style="border-top:none !important;border-right:none !important;border-left:none !important;height:100px !important;resize:none !important;"id="exampleInputEmail1" placeholder="Your Post..." name="post_name"></textarea>								
													<br />
													<select name="category_id">
													<%
													ArrayList<Category> category_list=postgreSQLDatabase.forum.Query.getAllCategories();
					                                Iterator<Category> iterator=category_list.iterator();
					                                while(iterator.hasNext()){
					                                	Category current=iterator.next();
					                                	
													    if((Long.parseLong(request.getParameter("category")))==current.getCategory_id()){
													    
													    %>
													    <option value="<%=current.getCategory_id()%>" selected="selected"><%=current.getCategory_name() %></option>	
													   <% }
													    else{
													%>
													
													<option value="<%=current.getCategory_id()%>"><%=current.getCategory_name() %></option>
													
													<%}} %>
													</select>
													<div class="btn-group pull-right">
														<button class="btn btn-primary btn-xs btn-info">Send Post</button>
													</div>
                 								</div>
											</div>
											
											<div class="modal-body pull-right">
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>
						</li>
					    </ul>		  
				        </div> 
			        </div>		
	  	        </section>
			</div>
		</div>	
		<!-- ./wrapper -->

		<%@ include file="footer.jsp" %>
				<!-- Control Sidebar -->
				<%@ include file="control-sidebar.jsp" %>
				<!-- /.control-sidebar -->

		<!-- jQuery 2.1.4 -->
		<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="../bootstrap/js/bootstrap.min.js"></script>
		<!-- FastClick -->
		<script src="../plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="../dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="../dist/js/demo.js"></script>
		<script src="../plugins/select2/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();});
</script>
	</body>
</html>