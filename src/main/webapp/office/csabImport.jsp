<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="csv.Parser"%>
<%@page import="java.io.File"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/cookie/jquery.cookie.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>

<!-- This is the Javascript file of jqGrid -->
<style>
invisible {
    display: none  !important;
    visibility:hidden  !important;
   

}
visible {
    display: inline;
}
</style>

<script>
	function skip(){

	    var num_top=parseInt(document.getElementById("skip_rows_top").value);
	    var num_bottom=parseInt(document.getElementById("skip_rows_bottom").value);
		table_body = document.getElementById("csv").getElementsByClassName("table_body")[0];
		rows = table_body.getElementsByTagName("tr").length;
		
		if(num_top>=rows-num_bottom || num_top<0||num_bottom>rows-num_top||num_bottom<0){
			alert("Incorrect number of rows");
			return false;
		}
		else{
			for(i=0;i<rows;i++){
				
				if(num_top!=0){
					console.log(num_top);
					table_body.getElementsByTagName("tr")[i].setAttribute('style', 'display:none !important');
					num_top--;
				}
				else if(num_bottom!=0 &&i>rows-num_bottom-1){
					//alert();
					table_body.getElementsByTagName("tr")[i].setAttribute('style', 'display:none !important');
					num_bottom--;
				}
				else{
					table_body.getElementsByTagName("tr")[i].setAttribute('style', 'display:table-row !important');
				}
			}
			
		}
	}
	
	
	
</script>


<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					CSAB <small>Student Registration List</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>



			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<%
									String filename=request.getServletContext().getRealPath("")+File.separator+"upload"+File.separator+request.getParameter("filename");
														                // out.println(filename);
														                Parser csv=new Parser(filename);
														                
								%>
								
								<div class="pull-right">
								<label>Skip rows from top</label>
									<input id="skip_rows_top" type="number" class="form-control" min="0" onchange=" return skip()" value="0"
										placeholder="Skip rows" />
								</div>
								
								<div >
								<label for="upload" >Upload another file</label>
								<form action="../CSABFileUpload" method="POST" enctype="multipart/form-data">
									<input type="file" name="file" class="btn btn-primary" onchange="this.parentNode.submit()" value="Upload file"/> 
								</form>
								<button type="button" class="btn btn-success " onclick="getColumns()">Save</button>
								</div>
								
								
								<table id="csv" class="table table-bordered table-striped">
									<thead>
										<tr>
											<%
											out.println("<th>SN</th>");
												for(int i=1;i<=csv.getNumCols();i++)
														{
																				%>
											<th><select class="field">
													<option value="ignore">Ignore</option>
													<option value="name">Name</option>
													<option value="first_name">First Name</option>
													<option value="middle_name">Middle Name</option>
													<option value="last_name">Last Name</option>
													<option value="category">Category</option>
													<option value="jee_main_rollno">JEE Main Roll No</option>
													<option value="jee_adv_rollno">JEE Advance Roll No</option>
													<option value="state">State</option>
													<option value="phone_number">Phone Number</option>
													<option value="email">Email</option>
													<option value="date_of_birth">Date Of Birth</option>
													<option value="program_allocated">Program
														Allocated</option>
													<option value="allocated_category">Allocated
														Category</option>
													<option value="allocated_rank">Allocated Rank</option>
													<option value="status">Status</option>
													<option value="choice_no">Choice Number</option>
													<option value="physically_disabled">Physically
														Disabled</option>
													<option value="gender">Gender</option>
													<option value="quota">Quota</option>
													<option value="round">Round</option>
													<option value="willingness">Willingness</option>
													<option value="address">Address</option>
													<option value="rc_name">RC Name</option>
													<option value="nationality">Nationality</option>
											</select></th>
											<%	}
											%>
										</tr>
									</thead>
									<tbody class="table_body" id="table_body">
									<%
											ArrayList<ArrayList<String>> array=csv.getArray();
																		   Iterator<ArrayList<String>> iterator=array.iterator();
																		   int count=0;
																		   while(iterator.hasNext()){
																			   
																			   out.println("<tr id='row_"+(count)+"'>");
																			   out.println("<td>"+ ++count+"</td>");
																			   ArrayList<String> current=iterator.next();
																				 Iterator<String> iterator1=current.iterator();
																			int col_count=1;
																				 while(iterator1.hasNext()){
																				col_count++;
																				String cell=iterator1.next();
																				if(cell.length()<=20)
																				out.println("<td id='cell_"+(count-1)+"_"+(col_count-2)+"' ><input type='text' style='border:none' value='"+cell+"' readonly></td>");
																				else
																				out.println("<td id='cell_"+(count-1)+"_"+(col_count-2)+"'><textarea style='border:none;overflow:hidden;' readonly>"+cell+"</textarea></td>");
																					
																			}
																				 while(col_count<=csv.getNumCols()){
																						col_count++;
																						out.println("<td></td>");
																					}
																				
																				out.println("</tr>");
																		   }
																	
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<div class="pull-right">
								    <label>Skip rows from bottom</label>
									<input id="skip_rows_bottom" type="number" class="form-control" min="0" onchange=" return skip()" value="0"
										placeholder="Skip from Rows Bottom" />
								</div>
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->

	<!-- Bootstrap 3.3.5 -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<!-- AdminLTE for demo purposes -->
	<!-- page script -->
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
	 $(function () {
		  var table=  $("#csv").DataTable({
			  "paging": false,
		      "lengthChange": true,
		      "searching": true,
		      "ordering": false,
		      "info": true,
		      "autoWidth": true,
		      buttons: [ 'copy', 'excel', 'pdf'  ]
			});
		  
		  });
	</script>
	<script type="text/javascript">
	//if( $.cookie("csv_upload_array")) 
		if( $.cookie("csv_upload_array")){
			var array=$.cookie("csv_upload_array").split(",");
			var fields=document.getElementById("csv").getElementsByClassName("field");
			for(var j=0;j<fields.length;j++){
				fields[j].value=array[j];
			}
		}
		function getColumns(){
			var array=[];
			
			
		var mandatory_fields=["name","date_of_birth","state","round","category","program_allocated","gender","jee_main_rollno"];
	//var mandatory_fields=["name"];	
		var skip_rows_start=document.getElementById("skip_rows_top").value;
		var skip_rows_end=document.getElementById("skip_rows_bottom").value;
			//alert("hello"+skip_rows);
			var columns=document.getElementById("csv").getElementsByClassName("field");
			for(var i=0;i<columns.length;i++){
				array.push(columns[i].value);
			}
			
			for(var j=0;j<mandatory_fields.length;j++){
				
				if(array.indexOf(mandatory_fields[j])==-1){alert("Mandatory field {"+mandatory_fields[j]+"} is missing");
				$.cookie("csv_upload_array", array);
				
				return;}
			} 
			
			
			for(i=0;i<array.length;i++){
				for(j=i+1;j<array.length;j++){
					if(array[i]=="ignore")break;
					if(array[i]==array[j]){
						alert("Duplicate Entry of '"+array[j]+"' at the "+(j+1)+" column number");
						$.cookie("csv_upload_array", array);
						return;
					   }
				}
			}
			$.cookie("csv_upload_array", array);
			var xmlhttp;
			try{
				xmlhttp = new XMLHttpRequest();
			} catch (e){
				// Internet Explorer Browsers
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
					
				} catch (e) {
					try{
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e){
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}	

			if(xmlhttp){	
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						var data=JSON.parse(xmlhttp.responseText);
						if(data.success){
							alert("File uploaded successfully");
						}
						else{
							var errors=data.message;
						
							for(var i=0;i<errors.length;i++){
								var error=errors[i];
								if(error.message=="inconsistent_row"){
									document.getElementById("row_"+error.row).setAttribute("style","background-color:hsla(0, 100%, 50%, 0.68)  !important");
								}
								else {
									document.getElementById("cell_"+error.row+"_"+error.column).setAttribute("style","background-color:hsla(0, 100%, 50%, 0.68)  !important");
									document.getElementById("cell_"+error.row+"_"+error.column).setAttribute("title",error.message);
								}
							}
							
						}
						//alert(xmlhttp.responseText);
					}
					if(xmlhttp.status == 404)
						alert("Could not connect to server");
				}
				xmlhttp.open("POST","../UploadCsabCsv",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("array="+array+"&filename=<%=request.getParameter("filename")%>"+"&skip_start="+skip_rows_start+'&skip_end='+skip_rows_end);
			}
			return false;

			
		}
		document.getElementById('skip_rows_top').setAttribute('max', document.getElementsByTagName('tr').length-1);
		document.getElementById('skip_rows_bottom').setAttribute('max', document.getElementsByTagName('tr').length-1);
		
		function upload(){
			
		}
		 
		
		
	</script>
</body>
</html>