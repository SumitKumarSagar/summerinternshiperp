<!DOCTYPE html>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="postgreSQLDatabase.feePayment.*"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Registration Status</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<%
								ArrayList<FeeBreakup> breakup_list = Query.getFeePaymentList();
								Iterator<FeeBreakup> iterator = breakup_list.iterator();
								while (iterator.hasNext()) {
									FeeBreakup current = iterator.next();
							%>
							<%
								JSONArray details = current.getBreakup();
							%>
							<h4 class="modal-title" id="myModalLabel">Fee Breakup</h4>
							<div class="modal-body" id="breakup_modal">
								<table class="table table-bordered table-striped"><tbody>
									<%
										//System.out.println(details);
											for (int i = 0; i < details.length() - 1; i++) {
												JSONObject sub_category = details.getJSONObject(i);
												Iterator<String> sub_category_iterator = sub_category.keys();
												out.print(
														"<tr><td><strong>" + sub_category.getString("name").toUpperCase() + "</strong></td></tr>");
												while (sub_category_iterator.hasNext()) {
													String key = sub_category_iterator.next();

													if (!key.equalsIgnoreCase("subtotal") && !key.equalsIgnoreCase("name"))
														out.print("<tr><td>" + key.toUpperCase() + "</td><td>" + sub_category.getString(key)
																+ "</td></tr>");
												}
												out.print("<tr><td>" + "SUBTOTAL" + "</td><td>" + sub_category.getString("subtotal").toUpperCase()
														+ "</td></tr>");

											}
									%>
									</tbody>
								</table>
								<div class="form-group">
									<label for="exampleInputPassword1">Total<%=details.getJSONObject(details.length() - 1).getInt("total")%></label>
								</div>
								<%
									}
								%>
							</div>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>



			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										    <th></th>
											<th>Year</th>
											<th>Semester</th>
											<th>Category</th>
										</tr>
									</thead>
									<tbody>
										  <%
                ArrayList<FeeBreakup> breakup_list1=Query.getFeePaymentList();
                Iterator<FeeBreakup> it=breakup_list1.iterator();
                while(it.hasNext()){
    				FeeBreakup current1=it.next();
    				
                %> 
										<tr>
											<td>
												<button type="button" class="btn btn-success"
													data-toggle="modal" data-target="#myModal"
													onclick="display(this)">
													<i class="glyphicon glyphicon-eye-open">View </i>
												</button>

											</td>
											<td><%=current1.getYear() %></td>
                  <td><%=current1.getSemester() %></td>
                  <td><%=current1.getCategory()%> 
												<%-- <% JSONArray details=  current.getBreakup();
								                
												%></span>
												<div class="form-group">
													<label for="exampleInputPassword1">Fee Breakup : </label>
													<table><%
													
															  		//System.out.println(details);
					       	                            for(int i=0;i<details.length()-1;i++){
							                			JSONObject sub_category= details.getJSONObject(i);
							                			Iterator<String> sub_category_iterator =sub_category.keys(); 
							                			out.print("<tr><td><strong>"+sub_category.getString("name").toUpperCase()+"</strong></td></tr>");
							                			while(sub_category_iterator.hasNext()){
							                			String key=sub_category_iterator.next();
							                			
							                		if(!key.equalsIgnoreCase("subtotal")&&!key.equalsIgnoreCase("name"))	 out.print("<tr><td>"+key.toUpperCase()+"</td><td>"+sub_category.getString(key)+"</td></tr>");
							                			}
							                			out.print("<tr><td>"+"SUBTOTAL"+"</td><td>"+sub_category.getString("subtotal").toUpperCase()+"</td></tr>");
							                	  		
							                		 }
                		
							                		  %>
													</table>
												</div>
												<div class="form-group">
													<label for="exampleInputPassword1">Total<%=details.getJSONObject(details.length()-1).getInt("total")%></label>
												</div> --%>

												</td>
										</tr>
<%
                }
                %>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script src="../dist/js/payment.js"></script>
	<script>
		$(function() {
			$("#example1").DataTable({
				"paging" : true,
				"lengthChange" : true,
				"searching" : true,
				"ordering" : true,
				"info" : true,
				"autoWidth" : true
			});
		});

		function display(row) {
			// alert(row.parentNode.parentNode.innerHTML);
			document.getElementById("breakup_modal").innerHTML = row.parentNode.parentNode
					.getElementsByClassName("breakup")[0].innerHTML;
		}
	</script>

</body>
</html>