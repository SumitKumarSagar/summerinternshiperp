<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Registration Status</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Registration Id</th>
											<th>Name</th>
											<th>Student ID</th>
											<th>Semester</th>
											<th>Promote</th>
										</tr>
									</thead>

									<tbody>
										<input type="button" onclick="temp()" class="btn btn-primary"
											value="Promote" />
										<%
											ArrayList<Student> registration_list = Query.getStudentRegistrationList();
											Iterator<Student> iterator = registration_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
										%>
										<tr class="student">
											<td><span class="reg_id"><%=current.getRegistration_id()%></span></td>
											<td><%=current.getName()%></td>
											<td><%=current.getStudent_id()%></td>
											<td><%=current.getSemester()%></td>
											<td><input type="checkbox" class="promote"
												name="promote" /></td>

										</tr>

										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script src="../dist/js/payment.js"></script>
	<script>
		$(function() {
			$("#example1").DataTable({
				"paging" : true,
				"lengthChange" : true,
				"searching" : true,
				"ordering" : true,
				"info" : true,
				"autoWidth" : true
			});
		});
		function temp() {

			var students = document.getElementsByClassName("student");
			var json_array = [];

			for (var i = 0; i < students.length; i++) {
				var json_object = {};
				var student = students[i];
				json_object["reg_id"] = student
						.getElementsByClassName("reg_id")[0].innerHTML;
				console
						.log(student.getElementsByClassName("promote")[0].checked);
				json_object["promote"] = student
						.getElementsByClassName("promote")[0].checked;
			 json_array.push(json_object);
				
			}
			
			
			var details=JSON.stringify(json_array);
			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			if(xmlhttp){	
				xmlhttp.onreadystatechange=function() {
					if (xmlhttp.readyState==4 && xmlhttp.status==200) {
						window.location.reload();
					}
					if(xmlhttp.status == 404)
						alert("Could not connect to server");
				}

				xmlhttp.open("POST", "../PromoteStudent", true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				xmlhttp.send("details=" + details);
			}
		}
	</script>

</body>
</html>