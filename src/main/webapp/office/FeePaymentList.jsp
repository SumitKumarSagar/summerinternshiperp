<!DOCTYPE html>
<%@page import="org.json.JSONObject"%>
<%@page import="postgreSQLDatabase.feePayment.*"%>
<!-- author Anita Gadhwal-->
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>




		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Fee Payment Details <small></small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Payment List</h3>
								<a
									href="newPayment.jsp?transaction_id=<%=request.getParameter("transaction_id")%>"><button
										class="btn btn-primary" onclick="">Add Payment</button></a>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Verify</th>
											<th>Delete</th>
											<th>Print</th>
											<th>Reference no.</th>
											<th>Comment</th>
											<th>Payment Method</th>
											<th>Amount</th>
											<th>Details</th>
										</tr>
									</thead>
									<tbody>

										<%
											long transaction_id = Long.parseLong(request.getParameter("transaction_id").toString());

											ArrayList<Payment> payment_list = Query.getFeePaymentByTransaction(transaction_id);
											Iterator<Payment> iterator = payment_list.iterator();
											while (iterator.hasNext()) {
												Payment current = iterator.next();
										%>
										<td><div class="btn-group">
												<%
													if (current.isVerified() != true) {
												%>
												<button type="button" class="btn btn-block btn-primary"
													onclick="verify(<%=current.getRef_no()%>)">Verify</button>


												<%
													} else {
												%>
												<button type="button" class="btn btn-block btn-success"
													disabled>Verified</button>
												<%
													}
												%>
											</div></td>

										<td><button type="button"
												class="btn btn-block btn-danger"
												onclick="deletePayment(<%=current.getRef_no()%>)">
												<i class="glyphicon glyphicon-trash"></i>
											</button></td>

										<td>
											<button class="btn btn-sm btn-primary"
												onclick="window.open('../templates/feeReceipt2.jsp?ref_no=<%=current.getRef_no()%>','_blank')">
												<i class="fa fa-print"></i>
											</button>
										</td>
										<td><%=current.getRef_no()%></td>
										<td>
											<%
												if (current.getAmount() < 0)
														out.print("<strong>Office Payment</strong>:<br/>");
											%>
											<%=current.getComment()%>
										</td>

										<td><%=current.getPayment_method()%></td>
										<td><%=current.getAmount()%></td>
										<td><table>
												<%
													JSONObject details = current.getDetails();
														Iterator<String> iterator2 = details.keys();
														while (iterator2.hasNext()) {

															String key = iterator2.next();
															out.print("<tr><td><strong>" + key.toUpperCase() + ":</strong></td><td>" + details.get(key)
																	+ "</td></tr>");
														}
												%>
											</table></td>




										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<script>
	
	
	
function verify(ref_no){
	
		
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
				//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		
		if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				
						alert("Payment has been verified");
					
						var message=JSON.parse(xmlhttp.responseText);
						if(message.transaction_complete)
						{	alert("All Payments of this transaction are complete!");
						if(confirm("Do you want to mark this transaction as complete?")) 
						{
							verify_transaction(message.transaction_id);
						}
						}
						window.location.reload();
						
						
		        //	window.location.reload();
		        	
					
				   
				}
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    
		    xmlhttp.open("POST","../VerifyFeePayment",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("ref_no="+ref_no);
		}
		}
	   
  
  
function verify_transaction(transaction_id){
	
	var xmlhttp;
	try{
		xmlhttp = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
			//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}	
	
	if(xmlhttp){
	    xmlhttp.onreadystatechange=function() {
	    	
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				alert(xmlhttp.responseText);
				var message=JSON.parse(xmlhttp.responseText);
				if(message.transaction_complete)
					window.location.reload();
				else 
					alert("Some payments of this transaction have not been completed");
	
	        	
				
			   
			}
	        if(xmlhttp.status == 404)
				alert("Could not connect to server");
			}
	    
	    xmlhttp.open("POST","../VerifyFeeTransaction",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("transaction_id="+transaction_id);
	    
	
   }
}

  
  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
function generateTemplate(file_id){
	window.location.href="generateTemplate.jsp?file_id="+file_id;
}

</script>
	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script src="../dist/js/payment.js"></script>
	<script>
		$(function() {
			$("#example1").DataTable({
				"paging" : true,
				"lengthChange" : true,
				"searching" : true,
				"ordering" : true,
				"info" : true,
				"autoWidth" : true
			});
		});
	</script>
</body>
</html>