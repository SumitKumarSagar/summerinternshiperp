<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Data Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <script src="../dist/js/feeBreakup.js"></script>
	<style>
		.hideBorder{
			border:none;
		}
	</style>
	<script>
	
		function addsection(){
			document.getElementById("feeStructure").insertAdjacentHTML('beforeend',document.getElementById("addSection").innerHTML);
		}
		
		function addField(add){
			add.parentNode.parentNode.getElementsByClassName("allFields")[0].insertAdjacentHTML('beforeend',document.getElementById("addField").innerHTML);
		}
		function rowHide(hid){
			a = hid.parentNode.parentNode.parentNode;
			a.parentNode.removeChild(a);			
		}
		
		function createFeeStructure(){
			var xmlhttp;
			feePage1 = [];
			feePage2 = [];
			total1=0;
			total2=0;
			
			//semester = document.getElementById("sem").value;
			round=document.getElementById("round").value;
			year = 	document.getElementById("year").value;
			
			feeStruct = document.getElementById("feeStructure").getElementsByClassName("box box-solid");
		    for(i=0;i<feeStruct.length;i++){
		    	section1 = {};
		    	section2 = {};
		    	
		    	fieldsAdd1 = [];
		    	fieldsAdd2 = [];
		    	
		    	subtotal1 = 0;
				subtotal2 = 0;
				
				subtt1 = {};
				subtt2 = {};
		    	
				a = feeStruct[i].getElementsByClassName("box-title col-xs-4")[0].getElementsByClassName("form-control")[0].value;
		    	fields = feeStruct[i].getElementsByClassName("box-header")[0].getElementsByClassName("allFields")[0].getElementsByClassName("form-group col-md-12");
		    	
		    	for(j=0;j<fields.length;j++){
		    		field_values1 = {};
		    		field_values2 = {};
		    		y = fields[j].getElementsByClassName("col-md-3 hideBorder");
		    		field_values1[y[0].value]=y[1].value;total1+=parseInt(y[1].value);subtotal1+=parseInt(y[1].value);
		    	    field_values2[y[0].value]=y[2].value;total2+=parseInt(y[2].value);subtotal2+=parseInt(y[2].value);
		    	    fieldsAdd1.push(field_values1);
		    	    fieldsAdd2.push(field_values2);
		    	}
		    	subtt1["subtotal"] = subtotal1;fieldsAdd1.push(subtt1);
		    	subtt2["subtotal"] = subtotal2;fieldsAdd2.push(subtt2);
		    	section1[a] = fieldsAdd1;
		    	section2[a] = fieldsAdd2;
		    	feePage1.push(section1);
		    	feePage2.push(section2);
		    }
		    tt1 = {};
		    tt2 = {};
		    tt1["total"]=total1;
		    tt2["total"]=total2;
		    feePage1.push(tt1);
		    feePage2.push(tt2);
		    alert(JSON.stringify(feePage1));
	    	alert(JSON.stringify(feePage2));
			data1 =JSON.stringify(feePage1);
			data2 =JSON.stringify(feePage2);
		   
		    try{
				xmlhttp = new XMLHttpRequest();
			} catch (e){
				// Internet Explorer Browsers
				try{
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try{
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e){
					//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}	
			
			if(xmlhttp){
			    xmlhttp.onreadystatechange=function() {
			    	
			        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			        	alert("Fee structure is inserted.");
			        		
					}
			        
			        if(xmlhttp.status == 404)
						alert("Could not connect to server");
					}
			    xmlhttp.open("POST","../RegistrationFeeBreakup",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("round="+round+"&year="+year+"&fee_breakup_general="+data1+"&fee_breakup_sc="+data2);
			}
		}
		
	</script>	
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div style="display:none;">
     <span id="addSection">
           <div class="box box-solid">
				<div class="box-header">
				   	<h3 class="box-title col-xs-4">
						<input type="text" class="form-control" style="font-weight:bold;"
							placeholder="Type Header Name..." />
					</h3>

					<div class="box-tools pull-right">
					    <button type="button" class="btn btn-warning btn-sm"
							onclick="addField(this)">
							<i class="fa fa-plus"> Add fields</i>
						</button>
						<button type="button" class="btn bg-teal btn-sm"
							data-widget="remove">
							<i class="fa fa-times"></i>
						</button>
					</div><br><br><br>
					<span class="allFields"></span>
				</div>

			</div>
     </span>

     <span id="addField">
        <div class="form-group col-md-12">
                <div class="fieldHide">
                   <div class="col-md-2">
					<button type="button" class="btn bg-teal btn-sm" onclick="rowHide(this)">
						<i class="fa fa-times"></i>
					</button>
					</div>
					   <input type="text" class="col-md-3 hideBorder" placeholder="Type Field Name..." />
					   <input type="text" class="col-md-3 hideBorder" placeholder="Type Number..." />
					   <input type="text" class="col-md-3 hideBorder" placeholder="Type Number..." /> 
					
				</div>
		</div>
     </span>
</div>


<div class="wrapper">
<%@ include file="header.jsp" %>
<!-- Left side column. contains the logo and sidebar -->
<%@ include file="main-sidebar.jsp" %>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
             <div class="col-md-6">
              <div class="form-group">
                  <label>Round</label>
                  <select class="form-control" id="round">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
          </div></div>
          <div class="col-md-6">
          <div class="form-group">
                  <label>Year</label>
                  <select class="form-control" id="year">
                    <option>2016</option>
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                    <option>2020</option>
                    <option>2021</option>
                    <option>2022</option>
                    <option>2023</option>
                  </select>
          </div></div>
          </div></div></div>
          <div class="col-md-12">
          <div class="box">
          <div class="box-body">
              <div class="box box-success">
                <div class="box -header with-border col-md-12">
                  <label class="col-md-2"></label>
                  <label class="col-md-3"><h4 class="box-title">Head of Fees</h4></label>
                  <label class="col-md-3"><h4 class="box-title">General/OBC Category Student</h4></label>
                  <label class="col-md-3"><h4 class="box-title">SC/ST Category Student</h4></label>
                </div>
			  </div>
			  <br><br>
			  <div id="feeStructure"></div>
			  
              <form method="post" action="../RegistrationFeeBreakup">
              	<input type="hidden" name="year" >
              	<input type="hidden" name="category">
              	<input type="hidden" name="round">
              	<input type="hidden" name="breakup">
              	<input type="hidden" name="amount">
              </form>
            </div>
            <div class="box-footer no-padding">
                 <input type="button" class="btn btn-success pull-left" onclick="createFeeStructure()" value="Submit" />
                 <div class="pull-right">
                      <input type="button" class="btn btn-primary pll-right" onclick="addsection()" value="Add Section"/>
                 </div>                 
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  </div>
  <!-- /.content-wrapper -->
   <%@ include file="footer.jsp" %>

  <!-- Control Sidebar -->
  <%@ include file="control-sidebar.jsp" %>
  <!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- page script -->

</body>
</html>
